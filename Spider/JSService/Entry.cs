﻿/*
 * ==============================================================================
 *
 * Filename: Entry
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/19 11:57:45
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;
using System;

namespace JSService
{
    public class Entry : IEntryable
    {
        public object Create()
        {
            ServiceInstance instance = new ServiceInstance();
            return instance;
        }

        public Action<object, MessageType, int, int, object[]> Init(ICommandable cmd, bool isHotter, object ud, string param)
        {
            ServiceInstance jsService = ud as ServiceInstance;
            if (isHotter)
            {
                cmd.Log("hotter init");
            }
            else
            {
            }

            return Callback;
        }

        void Callback(object ud, MessageType type, int session, int source, params object[] dataArray)
        {
            ServiceInstance instance = ud as ServiceInstance;
            instance.Callback(ud, type, session, source, dataArray);
        }

        public void Release(bool isHotfix, object ud)
        {
            ServiceInstance instance = ud as ServiceInstance;
            if (!isHotfix)
            {
            }
        }

        public void Signal(object ud)
        {
            ServiceInstance instance = ud as ServiceInstance;
        }
    }
}
