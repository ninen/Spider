local spider = require "spider"
local socketmanager = require "socketmanager"

spider.start(function()
    socketmanager.listen()
    local handle = spider.localname(".handle")
    spider.log("thirdeye server start")
    spider.dispatch("socket", function(session, address, type, fd, buffer)
        if type == spider.STYPE_CONNECT then
            spider.log("connect fd=" .. fd)
            spider.send(handle, "lua", "connect", fd)
        elseif type == spider.STYPE_DATA then
            spider.send(handle, "lua", "message", fd, buffer)
        elseif type == spider.STYPE_CLOSE then
            spider.send(handle, "lua", "close",  fd)
        end
    end)
   

end)



