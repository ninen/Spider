local spider = require "spider"

local utils = {}

-- iv
local iv = "ABCDEF1234123412"

-- 加密
function utils.encrypt(key, data)
    local str, len = crypt.aesencrypt(data, key, iv)
    return str, len
end

-- 解密
function utils.decrypt(key, data)
    local str, len = crypt.aesdecrypt(data, key, iv)
    return str, len
end

-- 写数据
function utils.writeFileData(filename, offset, data)
    local file = io.open(filename, "ab")
    if file then
        io.output(file)
        io.write(data)
        io.flush()
        io.close(file)   
    end                     
end

return utils
