local spider = require "spider"
local json = require "json"

spider.start(function()
    spider.log("bootloader start")
    
    -- 创建handle
    spider.newservice("handle")

    -- 创建thirdeye service
    spider.newservice("thirdeye")
    
    spider.exit()
end)



