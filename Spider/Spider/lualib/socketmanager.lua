local spider = require "spider"

local socketmanager = {}

do
	local REG = spider.register_protocol
	REG {
        name = "socket",
        id = spider.PTYPE_SOCKET
    }
end

function socketmanager.listen()
    socket.listen()
end

function socketmanager.unlisten()
    socket.unlisten()
end

function socketmanager.send(saddr, buffer)
    socket.send(saddr, buffer)
    return 0
end

return socketmanager