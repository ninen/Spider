﻿local c = core
local tostring = tostring
local tonumber = tonumber
local coroutine = coroutine
local assert = assert
local pairs = pairs
local pcall = pcall

local coroutine_resume = coroutine.resume
local coroutine_yield = coroutine.yield

local proto = {}
local spider = {
	PTYPE_TEXT = 0,
	PTYPE_RESPONSE = 1,
	PTYPE_MULTICAST = 2,
	PTYPE_CLIENT = 3,
	PTYPE_SYSTEM = 4,
	PTYPE_HARBOR = 5,
	PTYPE_SOCKET = 6,
	PTYPE_ERROR = 7,
	PTYPE_QUEUE = 8,
	PTYPE_DEBUG = 9,
	PTYPE_LUA = 10,
	PTYPE_SNAX = 11,

	STYPE_DATA = 101, -- socket type
	STYPE_CONNECT = 102,
	STYPE_CLOSE = 103,
	STYPE_ACCEPT = 104,
	STYPE_ERROR = 105,
	STYPE_WARNING = 106,
	STYPE_HTTP = 107,
}

local session_id_coroutine = {}
local session_coroutine_id = {}
local session_coroutine_address = {}
local session_response = {}
local unresponse = {}

local wakeup_session = {}
local sleep_session = {}

local dead_service = {}
local error_queue = {}
local fork_queue = {}


function spider.register_protocol(class)
	local name = class.name
	local id = class.id
	assert(proto[name] == nil)
	assert(type(name) == "string" and type(id) == "number" and id >=0 and id <=255)
	proto[name] = class
	proto[id] = class
end

-- suspend is function
local suspend

-- coroutine reuse
local coroutine_pool = setmetatable({}, { __mode = "kv" })
local function co_create(f)
	local co = table.remove(coroutine_pool)
	if co == nil then
		co = coroutine.create(function(...)
			f(...)
			while true do
				f = nil
				coroutine_pool[#coroutine_pool+1] = co
				f = coroutine_yield "EXIT"
				f(coroutine_yield())
			end
		end)
	else
		coroutine_resume(co, f)
	end
	return co
end


local function string_to_handle(str)
	return tonumber("0x" .. string.sub(str , 2))
end

local self_handle
function spider.self()
	if self_handle then
		return self_handle
	end
	self_handle = string_to_handle(c.command("REG"))
	return self_handle
end

------------------------------ api ---------------------------------------
function spider.error(msg)
	local currentlineInfo = debug.getinfo(2, "l")
	local sourceInfo = debug.getinfo(2, "S")
	c.error(sourceInfo.short_src, ":".. currentlineInfo.currentline.. " ".. tostring(msg))
end

function spider.log(msg)
	local currentlineInfo = debug.getinfo(2, "l")
	local sourceInfo = debug.getinfo(2, "S")
	c.log(sourceInfo.short_src, ":" .. currentlineInfo.currentline.. " ".. tostring(msg))
end

function spider.wait(co)
	local session = c.genid()
	local ret, msg = coroutine_yield("SLEEP", session)
	co = co or coroutine.running()
	sleep_session[co] = nil
	session_id_coroutine[session] = nil
end

function spider.sleep(ti)
	local session = c.intcommand("TIMEOUT",ti)
	assert(session)
	-- 处导致正在执行的协程挂起,这会让corutine.resume返回去执行suspend.
	local succ, ret = coroutine_yield("SLEEP", session)
	sleep_session[coroutine.running()] = nil
	if succ then
		return
	end
	if ret == "BREAK" then
		return "BREAK"
	else
		error(ret)
	end
end

function spider.yield()
	return spider.sleep(0)
end

spider.redirect = function(dest, typename, session, ...)
	return c.redirect(dest, proto[typename].id, session, ...)
end

function spider.localname(name)
	local addr = c.command("QUERY", name)
	if addr then
		return string_to_handle(addr)
	end
end

function spider.exit()
	c.command("EXIT")
end

function spider.getstring(key)
	return c.command("GETSTRING",key)
end

function spider.setstring(key, value)
	c.command("SETSTRING", key, value)
end

local function yield_call(service, session)
	local succ, msg = coroutine_yield("CALL", session)
	if not succ then
		error "call failed"
	end
	return msg
end

function spider.call(addr, typename, ...)
	local p = proto[typename]
	local session = c.send(addr, p.id , ...)
	if session == nil then
		error("call to invalid address " .. spider.address(addr))
	end
	return yield_call(addr, session)
end

function spider.ret(...)
	return coroutine_yield("RETURN", ...)
end

function spider.address(addr)
	if type(addr) == "number" then
		return string.format(":%08x",addr)
	else
		return tostring(addr)
	end
end

function spider.send(addr, typename, ...)
	local p = proto[typename]
	return c.send(addr, p.id, ...)
end

function spider.timeout(ti, func, ...)
 	local session = c.intcommand("TIMEOUT", ti, ...)
 	assert(session)
 	local co = co_create(func)
 	assert(session_id_coroutine[session] == nil)
 	session_id_coroutine[session] = co
end

function spider.name(aliasname)
	c.command("REG", aliasname)
end

function spider.newservice(name)
	return spider.launch(name)
end

-- 启用服务
function spider.launch(name)
	local addr = c.command("LAUNCH", "LUA" .. name, "./LuaService.dll", name .. ".lua")
	if addr then
		return tonumber("0x" .. string.sub(addr , 2))
	end
end

function spider.wakeup(co)
	if sleep_session[co] and wakeup_session[co] == nil then
		wakeup_session[co] = true
		return true
	end
end

function spider.fork(func,...)
	local args = table.pack(...)
	local co = co_create(function()
		func(table.unpack(args,1,args.n))
	end)
	table.insert(fork_queue, co)
	return co
end

------------------------------------api end-------------------------------------------------


local function _error_dispatch(error_session, error_source)
	if error_session == 0 then
		
	else
		table.insert(error_queue, error_session)
	end
end


spider.genid = assert(c.genid)


----- register protocol
do
	local REG = spider.register_protocol

	REG {
		name = "lua",
		id = spider.PTYPE_LUA
	}

	REG {
		name = "response",
		id = spider.PTYPE_RESPONSE
	}

	REG {
		name = "error",
		id = spider.PTYPE_ERROR,
		dispatch = _error_dispatch,
	}
end

local function dispatch_error_queue()
	local session = table.remove(error_queue,1)
	if session then
		local co = session_id_coroutine[session]
		session_id_coroutine[session] = nil
		return suspend(co, coroutine_resume(co, false))
	end
end

local function dispatch_wakeup()
	local co = next(wakeup_session)
	if co then
		wakeup_session[co] = nil
		local session = sleep_session[co]
		if session then
			session_id_coroutine[session] = "BREAK"
			return suspend(co, coroutine_resume(co, false, "BREAK"))
		end
	end
end

-- suspend is local function
function suspend(co, result, command, param)
	if not result then
		local session = session_coroutine_id[co]
		if session then -- coroutine may fork by others (session is nil)
			local addr = session_coroutine_address[co]
			if session ~= 0 then
				-- only call response error
				c.send(addr, spider.PTYPE_ERROR, session)
			end
			session_coroutine_id[co] = nil
			session_coroutine_address[co] = nil
		end
		error(debug.traceback(co,tostring(command)))
	end
	if command == "CALL" then
		session_id_coroutine[param] = co
	elseif command == "SLEEP" then
		session_id_coroutine[param] = co
		sleep_session[co] = param
	elseif command == "RETURN" then
		local co_session = session_coroutine_id[co]
		local co_address = session_coroutine_address[co]
		if param == nil or session_response[co] then
			error(debug.traceback(co))
		end
		session_response[co] = true
		local ret
		if not dead_service[co_address] then
			ret = c.send(co_address, spider.PTYPE_RESPONSE, co_session, param) ~= nil
			if not ret then
				-- If the package is too large, returns nil. so we should report error back
				c.send(co_address, spider.PTYPE_ERROR, co_session, "")
			end
		end
		return suspend(co, coroutine_resume(co, ret))
	elseif command == "RESPONSE" then
		local co_session = session_coroutine_id[co]
		local co_address = session_coroutine_address[co]
		if session_response[co] then
			error(debug.traceback(co))
		end
		local f = param
		local function response(ok, ...)
			if ok == "TEST" then
				if dead_service[co_address] then
					unresponse[response] = nil
					f = false
					return false
				else
					return true
				end
			end
			if not f then
				if f == false then
					f = nil
					return false
				end
				error "Can't response more than once"
			end

			local ret
			if not dead_service[co_address] then
				if ok then
					ret = c.send(co_address, spider.PTYPE_RESPONSE, co_session, f(...)) ~= nil
					if not ret then
						-- If the package is too large, returns false. so we should report error back
						c.send(co_address, spider.PTYPE_ERROR, co_session, "")
					end
				else
					ret = c.send(co_address, spider.PTYPE_ERROR, co_session, "") ~= nil
				end
			else
				ret = false
			end
			unresponse[response] = nil
			f = nil
			return ret
		end

		session_response[co] = true
		unresponse[response] = true
		return suspend(co, coroutine_resume(co, response))
	elseif command == "EXIT" then
		-- coroutine exit
		local address = session_coroutine_address[co]
		session_coroutine_id[co] = nil
		session_coroutine_address[co] = nil
		session_response[co] = nil
	elseif command == "QUIT" then
		-- service exit
		return
	elseif command == "USER" then
		-- See skynet.coutine for detail
		error("Call spider.coroutine.yield out of spider.coroutine.resume\n" .. debug.traceback(co))
	elseif command == nil then
		-- debug trace
		return
	else
		error("Unknown command : " .. command .. "\n" .. debug.traceback(co))
	end
	dispatch_wakeup()
	dispatch_error_queue()
end

function spider.dispatch(typename, func)
	local p = proto[typename]
	if func then
		local ret = p.dispatch
		p.dispatch = func
		return ret
	else
		return p and p.dispatch
	end
end

local function unknown_response(session, address, msg)
	spider.error(string.format("Response message : %s" , msg))
	error(string.format("Unknown session : %d from %x", session, address))
end

local function unknown_request(session, address, msg, prototype)
	spider.error(string.format("Unknown request (%s): %s", prototype, msg))
	error(string.format("Unknown session : %d from %x", session, address))
end

function spider.dispatch_unknown_request(unknown)
	local prev = unknown_request
	unknown_request = unknown
	return prev
end


local function raw_dispatch_message(prototype, session, source, ...)
	local msg = table.concat({...}, ",")	
	if prototype == 1 then
		-- 如果是回应消息
		local co = session_id_coroutine[session]
		if co == "BREAK" then
			session_id_coroutine[session] = nil
		elseif co == nil then
			unknown_response(session, source, msg)
		else
			session_id_coroutine[session] = nil
			-- coroutine_resume调用传过来的方法
			-- coroutine_resume: true 是否退出协程
			suspend(co, coroutine_resume(co, true, ...))
		end
	else
		-- 其它消息
		local p = proto[prototype]
		if p == nil then
			-- 异常情况
			if session ~= 0 then
				c.send(source, spider.PTYPE_ERROR, session, "")
			else
				unknown_request(session, source, msg, prototype)
			end
			return
		end
		local f = p.dispatch
		if f then
			local co = co_create(f)
			session_coroutine_id[co] = session
			session_coroutine_address[co] = source
			suspend(co, coroutine_resume(co, session,source, ...))
		else
			unknown_request(session, source, msg, proto[prototype].name)
		end
	end
end

function spider.dispatch_message(...)
	local succ, err = pcall(raw_dispatch_message,...)
	while true do
		--表示获取表的第一个元素和key,t={12,a=34,5,c}都可以访问到
		-- fork函数也是在这个被调用
		local key,co = next(fork_queue)
		if co == nil then
			break
		end
		fork_queue[key] = nil
		local fork_succ, fork_err = pcall(suspend,co,coroutine_resume(co))
		if not fork_succ then
			if succ then
				succ = false
				err = tostring(fork_err)
			else
				err = tostring(err) .. "\n" .. tostring(fork_err)
			end
		end
	end
	assert(succ, tostring(err))
end

local init_func = {}

function spider.init(f, name)
	assert(type(f) == "function")
	if init_func == nil then
		f()
	else
		table.insert(init_func, f)
		if name then
			assert(type(name) == "string")
			assert(init_func[name] == nil)
			init_func[name] = f
		end
	end
end

local function init_all()
	local funcs = init_func
	init_func = nil
	if funcs then
		for _,f in ipairs(funcs) do
			f()
		end
	end
end

local function ret(f, ...)
	f()
	return ...
end

local function init_template(start, ...)
	init_all()
	init_func = {}
	return ret(init_all, start(...))
end

function spider.pcall(start, ...)
	return xpcall(init_template, debug.traceback, start, ...)
end

function spider.init_service(start)
	local ok, err = spider.pcall(start)
	if not ok then
		spider.error("init service failed: " .. tostring(err))
		spider.exit()
	end
end

function spider.start(start_func, ...)
	c.callback(spider.dispatch_message)
	spider.timeout(0, function()
		spider.init_service(start_func)
	end)
end

return spider

