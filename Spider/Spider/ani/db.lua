local spider = require "spider"
local json = require "json"

local db = {}
local info = {}
info.host = "47.92.213.250"
info.user = "liwei"
info.passwd = "8g199696QQ"
info.database = "lazymonitor"
info.port = 3306


function db.login(username, passwd)
    local count = 0
    local context = mysql.create(info.host, info.port, info.user, info.passwd, info.database)
    if mysql.open(context) then
      local sql = "select count(*) from ani where  status=0 and username=@username and passwd=@password;";
      local parameter = {}
      parameter["@username"] = username
      parameter["@password"] = crypt.md5(passwd)
      local dataset = mysql.queryjson(context, sql, parameter)
      local t = json.decode(dataset)
      if #t > 0 then
        count = t[1]["count(*)"]
      end
      mysql.close(context)
    end
    return count
  end


function db.online(address, machine, date)
    local count = 0
    local context = mysql.create(info.host, info.port, info.user, info.passwd, info.database)
    if mysql.open(context) then
        local sql = "insert into record (`address`, `machine`, `action`, `creationtime`) values (@address, @machine, @action, @creationtime);"
        local parameter = {}
        parameter["@address"] = address
        parameter["@machine"] = machine
        parameter["@action"] = "online: " .. date
        parameter["@creationtime"] = os.time()
        count = mysql.noquery(context, sql, parameter)
        mysql.close(context)
    end
   
    return count
end

function db.offline(address, machine, date)
    local count = 0
    local context = mysql.create(info.host, info.port, info.user, info.passwd, info.database)
    if mysql.open(context) then
        local sql = "insert into record (`address`, `machine`, `action`, `creationtime`) values (@address, @machine, @action, @creationtime);"
        local parameter = {}
        parameter["@address"] = address
        parameter["@machine"] = machine
        parameter["@action"] = "offline: " .. date
        parameter["@creationtime"] = os.time()
        count = mysql.noquery(context, sql, parameter)
        mysql.close(context)
    end
    return count
end

return db
