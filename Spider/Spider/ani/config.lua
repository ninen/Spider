Root = "./ani"
IP = "192.168.1.11"
--IP = "172.26.111.35"
Port = 30080
NetProtocolType = 0 -- TSLF 0, TSSEP 1 WS 2
SocketThreads = 1
WorkerThreads = 1
Hotfix = "./hotfix.json"
LuaPath = ";./lualib/?.lua;" .. Root .. "/?.lua;"
LuaCPath = ";./luaclib/?.so;./luaclib/?.dll;"
LoadServices = {
    {
      DllFileName = "./LogService.dll",
      Args = Root .. "/log4net.config",
      Alias = ".LogService",
    },
    {
      DllFileName = "./HttpServerService.dll",
      Args = "30081",
      Alias = ".HttpServerService",
    },
    {
      DllFileName = "./LuaService.dll",
      Args = "bootloader.lua",
      Alias = ".BootloaderService",
    }
}
WSOption = {
    AllIdleTimeSeconds = 0,
    WriterIdleTimeSeconds = 0,
    ReaderIdleTimeSeconds = 120,
    MaxContentLength = 8192,
    WebsocketPath = "/ws"
}
TSLFOption = {
  ReaderIdleTimeSeconds = 120,
  WriterIdleTimeSeconds = 0,
  AllIdleTimeSeconds = 0,
  MaxFrameLength = 8192,
  LengthFieldOffset = 0,
  LengthFieldLength = 2,
  LengthAdjustment = 0,
  InitialBytesToStrip = 2
}
TSSEPOption = {
  ReaderIdleTimeSeconds = 120,
  WriterIdleTimeSeconds = 0,
  AllIdleTimeSeconds = 0,
  MaxFrameLength = 8192,
  BeginDelimiter = 0x2,
  EndDelimiter = 0x3 
}

