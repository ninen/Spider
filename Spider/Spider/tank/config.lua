Root = "./tank"
IP = "192.168.1.104"
--IP = "172.26.111.35"
Port = 30070
SocketType = 3 -- TSLF 0, TSSEP 1 WS 2 kcp 3 http 4
SocketThreads = 1
WorkerThreads = 1
HotterConfigFilename = "./Services/hotter.json"
ModuelDirectory = "./"
LuaPath = ";./lualib/?.lua;" .. Root .. "/?.lua;"
LuaCPath = ";./luaclib/?.so;./luaclib/?.dll;"
LoadServices = {
    {
      Name = "LoggerService",
      DllName = "LoggerService",
      Param = Root .. "/log4net.config"
    },
    {
      Name = "HttpService",
      DllName = "HttpService",
      Param = ""
    },
    {
      Name = "BootloaderService",
      DllName = "LuaService",
      Param = "bootloader.lua"
    }
}
WSOption = {
    AllIdleTimeSeconds = 0,
    WriterIdleTimeSeconds = 0,
    ReaderIdleTimeSeconds = 120,
    MaxContentLength = 8192,
    WebsocketPath = "/ws"
}
TSLFOption = {
  ReaderIdleTimeSeconds = 120,
  WriterIdleTimeSeconds = 0,
  AllIdleTimeSeconds = 0,
  MaxFrameLength = 8192,
  LengthFieldOffset = 0,
  LengthFieldLength = 2,
  LengthAdjustment = 0,
  InitialBytesToStrip = 2
}
TSSEPOption = {
  ReaderIdleTimeSeconds = 120,
  WriterIdleTimeSeconds = 0,
  AllIdleTimeSeconds = 0,
  MaxFrameLength = 8192,
  BeginDelimiter = 0x2,
  EndDelimiter = 0x3 
}
