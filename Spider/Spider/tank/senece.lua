local guid = require "guid"

local senece = {}
senece.prefabs = {
    player = "Player" 
}


senece.ENTITY_TYPE = {
    player = 1,
}

senece.entities = {}
senece.id = guid.new16()


function senece.createEntity(managerId, prefab, x, y, z, ex, ey, ez, sx, sy, sz, dx, dy, dz)
    local entity = {
        id = guid.new16(),
        managerId = managerId,
        prefab = prefab,
    }

    entity.transform = {
        x = x,
        y = y,
        z = z,
        ex = ex,
        ey = ey,
        ez = ez,
        sx = sx,
        sy = sy,
        sz = sz,
        dx = dx,
        dy = dy,
        dz = dz,
    }

    senece.entities[entity.id] = entity
    return senece.entities[entity.id]
end

function senece.getEntity(id)
    local entity = senece.entities[id]
    return entity
end

function senece.getManagerId(entity)
    return entity.managerId
end

function senece.getId(entity)
    return entity.id
end

function senece.getTransform(id)
    local entity = senece.entities[id]
    if entity then
        return entity.transform
    else
        return nil
    end
end

function senece.setPosition(entity, x, y, z)
    entity.transform.x = x
    entity.transform.y = y
    entity.transform.z = z
end

function senece.getPosition(entity)
    local x = entity.transform.x
    local y = entity.transform.y
    local z = entity.transform.z
    return {
        x = x,
        y = y,
        z = z
    }
end

function senece.setRotation(entity, x, y, z)
    entity.transform.ex = x
    entity.transform.ey = y
    entity.transform.ez = z
end

function senece.getRotation(entity)
    local ex = entity.transform.ex
    local ey = entity.transform.ey
    local ez = entity.transform.ez
    return {
        x = ex,
        y = ey,
        z = ez
    }
end

function senece.setScale(entity, x, y, z)
    entity.transform.sx = x
    entity.transform.sy = y
    entity.transform.sz = z
end

function senece.getScale(entity)
    local sx = entity.transform.sx
    local sy = entity.transform.sy
    local sz = entity.transform.sz
    return {
        x = sx,
        y = sy,
        z = sz
    }
end

function senece.setDir(entity, x, y, z)
    entity.transform.dx = x
    entity.transform.dy = y
    entity.transform.dz = z
end

function senece.getDir(entity)
    local dx = entity.transform.dx
    local dy = entity.transform.dy
    local dz = entity.transform.dz
    return {
        x = dx,
        y = dy,
        z = dz
    }
end

function senece.removeEntity(id)
    if senece.entities[id] then
        senece.entities[id] = nil
        return true
    else
        return false
    end
end

function senece.createPlayer(managerId, prefab)
    local entity = senece.createEntity(managerId, prefab, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0)
    return entity
end


local function createRandomPosition()
    while true do
        local x = math.random( -9, 10 )
        local y = math.random( -7, 8 )
        local z = 0
        for _, value in pairs(senece.entities) do
            local position = senece.getPosition(value)
            if position then
                if position.x == x and position.y == y and position.z == z then

                else
                    return {
                        x = x,
                        y = y,
                        z = z
                    }
                end
            end
        end
    end
    
end


-- 产生敌人的方法
function senece.createEnemy()
    local num = math.random( 0, 3 )
    local position = {}
    if num==0 then
        position = { x = -10, y = 8, z = 0}
    elseif num==1 then
        position = { x = 0, y = 8, z = 0}
    else
        position = { x = -10, y = 8, z = 0}
    end

    return position
end

-- 初始化场景
function senece.initMap()
    --实例化老家
    senece.createEntity(senece.id, "Heart", 0, -8, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)

    --用墙把老家围起来
    senece.createEntity(senece.id, "Wall", -1, -8, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    senece.createEntity(senece.id, "Wall", 1, -8, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    for i = -1, 2, 1 do
        senece.createEntity(senece.id, "Wall", i, -7, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    end

    -- 创建空气墙
    for i = -11, 12, 1 do
        senece.createEntity(senece.id, "AirBarriar", i, 9, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    end
    for i = -11, 12, 1 do
        senece.createEntity(senece.id, "AirBarriar", i, -9, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    end
    for i = -8, 9, 1 do
        senece.createEntity(senece.id, "AirBarriar", -11, i, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    end
    for i = -8, 9, 1 do
        senece.createEntity(senece.id, "AirBarriar", 11, i, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    end

    -- 创建敌人
    senece.createEntity(senece.id, "SmallEnemy", -10, 8, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    senece.createEntity(senece.id, "SmallEnemy", 0, 8, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    senece.createEntity(senece.id, "SmallEnemy", 10, 8, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0)


    -- 创建墙
    for i = 1, 60, 1 do
        local position = createRandomPosition()
        senece.createEntity(senece.id, "Wall", position.x, position.y,  position.z, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    end

    -- 创建障碍
    for i = 1, 20, 1 do
        local position = createRandomPosition()
        senece.createEntity(senece.id, "Barriar", position.x, position.y,  position.z, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    end

    -- 创建河流
    for i = 1, 20, 1 do
        local position = createRandomPosition()
        senece.createEntity(senece.id, "River", position.x, position.y,  position.z, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    end

    -- 创建草
    for i = 1, 20, 1 do
        local position = createRandomPosition()
        senece.createEntity(senece.id, "Grass", position.x, position.y,  position.z, 0, 0, 0, 3, 3, 3, 0, 0, 0)
    end
end


return senece