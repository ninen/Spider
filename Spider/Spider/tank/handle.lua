local lazynet = require "lazynet"
local socketmanager = require "socketmanager"
local cjson = require "json"
local guid = require "guid"
local senece = require "senece"
local queue = require "queue"


local sessions = {}
local handle = {}

local msg_queue = queue.new()

function handle.connected(fd)
    if not sessions[fd] then
        lazynet.log("connect")
        local mangerId = guid.new16()
        local player = senece.createEntity(mangerId, senece.prefabs.player)
        local session = {
            fd = fd,
            player = player
        }
        sessions[fd] = session

        -- 登录
        Login(session)
    end
end

function handle.message(fd, message)
    local session = sessions[fd]
    if session then
        lazynet.log(message);
        local t = cjson.decode(message)
        if t.cmd == "CS_Value" then
            -- 同步变量信息
            SyncValue(session, t)
        elseif t.cmd == "CS_Value2" then
            SyncValue2(session, t)
        elseif t.cmd == "CS_InitScene" then
            -- 初始化场景
            InitScene(session, t)
        elseif t.cmd == "CS_CreateEntity" then
            CreateEntity(session, t)
        elseif t.cmd == "CS_RemoveEntity" then
            RemoveEntity(session, t)
        else
            lazynet.log(message)
        end
    end
end

function handle.close(fd)
    local session = sessions[fd]
    if session then
        Leave(session)
        senece.removeEntity(session.player.id)
        sessions[fd] = nil
        lazynet.log("fd=" .. fd .. " close")
    end
end


lazynet.start(function()

    -- 监听
    socketmanager.listen()



    lazynet.name(".handle")
    lazynet.log("handle service start")

    lazynet.dispatch("socket", function(session, address, type, fd, buffer)
        if type == lazynet.STYPE_CONNECT then
            lazynet.log("connect fd=" .. fd)
            handle.connected(fd)
        elseif type == lazynet.STYPE_DATA then
            handle.message(fd, buffer)
        elseif type == lazynet.STYPE_CLOSE then
             handle.close(fd)
        end
    end)

    -- lazynet.dispatch("lua", function(session, address, cmd, fd, buffer, sz)
    --     lazynet.log(cmd)
    --     if cmd == "connect" then
    --         handle.connected(fd)
    --     elseif cmd == "message" then
    --         handle.message(fd, buffer, sz)
    --     elseif cmd == "close" then
    --         handle.close(fd)
    --     end
    -- end)


     lazynet.fork(function ()
        while true do
            --lazynet.log("====================fork")
            while true do
                local len = queue.length(msg_queue)
                if len > 0 then
                    local item = queue.dq(msg_queue)
                    if item.type == 0 then
                        -- send
                        socketmanager.send(item.session.fd, item.msg, #item.msg)
                    else
                        --
                        Broadcast(item.session, item.msg, item.myself)
                    end
                else
                    --lazynet.log("queue len = " .. len)
                    break
                end
            end
    
            -- 0.066
            lazynet.sleep(6)
        end
    end)


    -- 初始化场景
    senece.initMap()

    function Login(session)
        local rsp = {}
        rsp.cmd = "SC_Login"
        rsp.managerId = session.player.managerId
        local str = cjson.encode(rsp)
        --socketmanager.send(session.fd, str, #str)
        queue.eq(msg_queue, {
            type = 0,
            session = session,
            msg = str,
            myself = true
        })
    end

    function Leave(session)
        local rsp = {}
        rsp.cmd = "SC_Leave"
        rsp.id = session.player.id
        local str = cjson.encode(rsp)
        --Broadcast(session, str, false)
        queue.eq(msg_queue, {
            type = 1,
            session = session,
            msg = str,
            myself = false
        })
    end

    function InitScene(session, t)
        if sessions[session.fd] then
            if true then
                local rsp = {}
                rsp.cmd = "SC_InitScene"
                rsp.sceneId = senece.id
                rsp.entities = {}
                for k, v in pairs(senece.entities) do
                    local transform = v.transform
                    table.insert(rsp.entities, {
                        managerId = v.managerId,
                        id = v.id,
                        prefab = v.prefab,
                        x = transform.x,
                        y = transform.y,
                        z = transform.z,
                        ex = transform.ex,
                        ey = transform.ey,
                        ez = transform.ez,
                        sx = transform.sx,
                        sy = transform.sy,
                        sz = transform.sz,
                        dx = transform.dx,
                        dy = transform.dy,
                        dz = transform.dz
                    })
                end
                local str = cjson.encode(rsp)
                --socketmanager.send(session.fd, str, #str)
                queue.eq(msg_queue, {
                    type = 0,
                    session = session,
                    msg = str,
                    myself = true
                })
            end
            
           if true then
                local rsp = {}
                rsp.cmd = "SC_CreateEntity"
                rsp.ticks = t.ticks
                rsp.entity = session.player
                local str = cjson.encode(rsp)
                --Broadcast(session, str, false)
                queue.eq(msg_queue, {
                    type = 1,
                    session = session,
                    msg = str,
                    myself = false
                })
           end
        end
    end

    function CreateEntity(session, t)
        if sessions[session.fd] then
            local param = t.entity
            local entity = senece.createEntity(session.player.managerId, param.prefab, param.x, param.y, param.z, param.ex, param.ey, param.ez, param.sz, param.sy, param.sz, param.dx, param.dy, param.dz)

            local position = senece.getPosition(entity)
            local rotation = senece.getRotation(entity)
            local scale = senece.getScale(entity)

            local rsp = {}
            rsp.cmd = "SC_CreateEntity"
            rsp.ticks = t.ticks
            rsp.entity = {
                id = senece.getId(entity),
                managerId = senece.getManagerId(entity),
                prefab = entity.prefab,
                x = position.x,
                y = position.y,
                z = position.z,
                ex = rotation.x,
                ey = rotation.y,
                ez = rotation.z,
                sx = scale.x,
                sy = scale.y,
                sz = scale.z,
                dx = 0,
                dy = 0,
                dz = 0
            }
            local str = cjson.encode(rsp)
            --Broadcast(session, str, true)
            queue.eq(msg_queue, {
                type = 1,
                session = session,
                msg = str,
                myself = true
            })
        end
    end


    function RemoveEntity(session, t)
        if sessions[session.fd] then
            if senece.removeEntity(t.id) then
                local rsp = {}
                rsp.cmd = "SC_RemoveEntity"
                rsp.ticks = t.ticks
                rsp.id = t.id
                local str = cjson.encode(rsp)
                --Broadcast(session, str, true)
                queue.eq(msg_queue, {
                    type = 1,
                    session = session,
                    msg = str,
                    myself = true
                })
            end
        end
    end



    function SyncValue2(session, t)
        local enities = {}
        for _, value in ipairs(t.values) do
            local entity = senece.getEntity(value.id)
            if entity and entity.managerId == value.mid then
                local result, rsp = CheckValueOne(entity, value)
                if result then
                    table.insert(enities, rsp)
                end
            end
        end

        if #enities > 0 then
            local rsp = {}
            rsp.cmd = "SC_Value2"
            rsp.values = enities
            rsp.ticks = t.ticks
            local str = cjson.encode(rsp)
            --Broadcast(session, str, false)
            queue.eq(msg_queue, {
                type = 1,
                session = session,
                msg = str,
                myself = false
            })
        end
    end

    function SyncValue(session, t)
        if not sessions[session.fd] then
            return
        end

        local rsp = {}
        rsp.cmd = "SC_Value"
        rsp.varInfos = t.varInfos
        rsp.methodInfos = t.methodInfos

        local str = cjson.encode(rsp)
        --Broadcast(session, str, false)
        queue.eq(msg_queue, {
            type = 1,
            session = session,
            msg = str,
            myself = false
        })
    end

   -- 校验合法性
    function CheckValueOne(entity, value)
        senece.setPosition(entity, value.x, value.y, value.z)
        senece.setRotation(entity, value.ex, value.ey, value.ez)
        senece.setScale(entity, value.sx, value.sy, value.sz)
        senece.setDir(entity, value.dx, value.dy, value.dz)

        local position = senece.getPosition(entity)
        local rotation = senece.getRotation(entity)
        local scale = senece.getScale(entity)
        local dir = senece.getDir(entity)

        local rsp = {}
        rsp.mid = senece.getManagerId(entity)
        rsp.id = senece.getId(entity)
        rsp.x = position.x
        rsp.y = position.y
        rsp.z = position.z
        rsp.ex = rotation.x
        rsp.ey = rotation.y
        rsp.ez = rotation.z
        rsp.sx = scale.x
        rsp.sy = scale.y
        rsp.sz = scale.z
        rsp.dx = dir.x
        rsp.dy = dir.y
        rsp.dz = dir.z

        return true, rsp
    end


    function Broadcast(session, message, syncMyself)
        for k, v in pairs(sessions) do
            if syncMyself then
                socketmanager.send(v.fd, message, #message)
            else
                if session.fd ~= k then
                    socketmanager.send(v.fd, message, #message)
                end
            end
        end
    end
    

end)



