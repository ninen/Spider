local lazynet = require "lazynet"
local cjson = require "json"
local socketmanager = require "socketmanager"

lazynet.start(function()
    lazynet.log("sockettest service start")
    socketmanager.listen()
    local function onmessage(fd, message)
       lazynet.log(fd .. "   " .. message)
       socketmanager.send(fd, message)
    end
    
    local function onclose(fd)
         lazynet.log(fd)
    end
	lazynet.dispatch("socket", function(session, address, type, fd, buffer)
        if type == lazynet.STYPE_CONNECT then
            lazynet.log(fd .. "=" .. type)
        elseif type == lazynet.STYPE_DATA then
            onmessage(fd, buffer)
        elseif type == lazynet.STYPE_CLOSE then
            onclose(fd)
        end
    end)

end)

