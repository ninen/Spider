local spider = require "spider"

spider.start(function()
    spider.log("crypt test start")

    spider.log(type(crypt))

    spider.log(crypt.md5("123456"))
    local bstr = crypt.b64en("123456")
    spider.log(bstr)
    spider.log(crypt.b64de(bstr))

    -- 256
    local aesStr = crypt.aesencrypt("123456", "12345678901234561234567890123456", "1234567890123456")
    spider.log(aesStr)
    spider.log(crypt.aesdecrypt(aesStr, "12345678901234561234567890123456", "1234567890123456"))

    -- 128
    --local aesStr = crypt.aesencrypt("123456", "1234567890123456", "1234567890123456")
    --spider.log(aesStr)
    --spider.log(crypt.aesdecrypt(aesStr, "1234567890123456", "1234567890123456"))
    
    local desStr = crypt.desencrypt("123456", "12345678", "12345678")
    spider.log(desStr)
    spider.log(crypt.desdecrypt(desStr, "12345678", "12345678"))

    spider.exit()
end)

