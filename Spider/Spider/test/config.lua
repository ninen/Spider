Root = "./test"
IP = "192.168.1.106"
--IP = "172.26.111.35"
Port = 30070
SocketType = 1 -- TSLF 0, TSSEP 1 WS 2
SocketThreads = 1
WorkerThreads = 1
HotterConfigFilename = "./Services/hotter.json"
ModuelDirectory = "./"
LuaPath = ";./lualib/?.lua;" .. Root .. "/?.lua;"
LuaCPath = ";./luaclib/?.so;./luaclib/?.dll;"
LoadServices = {
    {
      Name = "LogService",
      DllName = "./LogService",
      Param = Root .. "/log4net.config"
    },
    {
      Name = "HttpServerService",
      DllName = "./HttpServerService",
      Param = ""
    },
    {
      Name = "BootloaderService",
      DllName = "JSService",
      Param = "test.js"
    }
}
WSOption = {
    AllIdleTimeSeconds = 0,
    WriterIdleTimeSeconds = 0,
    ReaderIdleTimeSeconds = 120,
    MaxContentLength = 8192,
    WebsocketPath = "/ws"
}
TSLFOption = {
  ReaderIdleTimeSeconds = 120,
  WriterIdleTimeSeconds = 0,
  AllIdleTimeSeconds = 0,
  MaxFrameLength = 8192,
  LengthFieldOffset = 0,
  LengthFieldLength = 2,
  LengthAdjustment = 0,
  InitialBytesToStrip = 2
}
TSSEPOption = {
  ReaderIdleTimeSeconds = 120,
  WriterIdleTimeSeconds = 0,
  AllIdleTimeSeconds = 0,
  MaxFrameLength = 8192,
  BeginDelimiter = 0x2,
  EndDelimiter = 0x3 
}

