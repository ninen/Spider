﻿local lazynet = require "lazynet"

lazynet.start(function( ... )
    lazynet.dispatch("lua", function(session, address, ...)
        lazynet.log("session=" .. session)
        lazynet.log("address=" .. address)
       local str = table.concat({...}, ',')
       lazynet.log(str)
    end)
    lazynet.name(".recv2")
	lazynet.log("recv2")
end)

