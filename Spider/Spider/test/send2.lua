﻿local lazynet = require "lazynet"

lazynet.start(function()
	lazynet.fork(function (...)
		local str = table.concat({...}, ',')
		lazynet.log("fork = " .. str)
	end, "liwei")
	lazynet.timeout(1000, function (b, ...)
		local str = table.concat({...}, ',')
		lazynet.log("timeout = " .. str)
	end, "liwei2")
	lazynet.log("send2")
	local handle = lazynet.localname(".recv2")
	lazynet.log(handle)
	lazynet.send(handle, "lua", "hello world", 10, "my name is liwei")

	lazynet.newservice("test")

	--lazynet.exit()
end)

