﻿using Core;

namespace Spider
{
    class Program
    {
        static void Main(string[] args)
        {
            Application.Exec(args.Length > 0 ? args[0] : "thirdeye");
        }
    }
}
