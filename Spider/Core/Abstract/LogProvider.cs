﻿/*
 * ==============================================================================
 *
 * Filename: LogProvider
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/25 0:02:19
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public abstract class LogProvider
    {
        public abstract void Log(string str);
        public abstract void Error(string str);
    }
}
