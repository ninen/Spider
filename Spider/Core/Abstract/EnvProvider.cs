﻿/*
 * ==============================================================================
 *
 * Filename: EnvProvider
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/22 23:15:15
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public abstract class EnvProvider : IQueryable, IUpdateable
    {
        public abstract string Query(string key);

        public abstract void Update(string key, string value);
    }
}
