﻿/*
 * ==============================================================================
 *
 * Filename: DBProvider
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/25 20:38:41
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public abstract class DBProvider
    {
        public abstract bool Open();
        public abstract int NoQuery(string sql, Dictionary<string, object> parameters);
        public abstract object[] Query(string sql, Dictionary<string, object> parameters);
        public abstract void Close();
    }
}
