﻿/*
 * ==============================================================================
 *
 * Filename: QueueProvider
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/7/7 9:37:59
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public abstract class QueueProvider<T>
    {
        public abstract void Enqueue(T value);
        public abstract T Dequeue();
        public abstract int Count();
    }
}
