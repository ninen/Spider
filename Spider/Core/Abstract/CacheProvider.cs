﻿/*
 * ==============================================================================
 *
 * Filename: CacheProvider
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/24 11:06:04
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Core
{
    public abstract class CacheProvider<KeyT, ValueT>
    {
        public abstract List<KeyT> GetKeyList();
        public abstract List<ValueT> GetValueList();
        public abstract ValueT GetValue(KeyT key);
        public abstract KeyT GetKey(ValueT value);
        public abstract bool Add(KeyT key, ValueT value);
        public abstract void Update(KeyT key, ValueT value);
        public abstract bool Delete(KeyT key);
        public abstract int Count();
        public abstract bool ContainsKey(KeyT key);
        public abstract void ForEach(Action<KeyT, ValueT> action);
    }
}
