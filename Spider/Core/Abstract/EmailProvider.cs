﻿/*
 * ==============================================================================
 *
 * Filename: EmailProvider
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/25 21:02:06
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public abstract class EmailProvider
    {
        public abstract string Send(EmailKitParameter parameter);
    }
}
