﻿/*
 * ==============================================================================
 *
 * Filename: JavaScriptEngine
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/7 23:44:01
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using JavaScriptEngineSwitcher.ChakraCore;
using JavaScriptEngineSwitcher.Core;
using System;
using System.Text;

namespace Core
{
    public class JavaScriptEngine
    {
        private IJsEngine _engine;
        public JavaScriptEngine()
        {
            var engineSwitcher = JsEngineSwitcher.Current;
            engineSwitcher.EngineFactories.Add(new ChakraCoreJsEngineFactory());
            engineSwitcher.DefaultEngineName = ChakraCoreJsEngine.EngineName;
            _engine = engineSwitcher.CreateDefaultEngine();
        }

        //public string DoString(string code)
        //{
        //    string result = string.Empty;
        //    using (IJsEngine engine = _engineSwitcher.CreateDefaultEngine())
        //    {
        //        //engine.ExecuteFile(string.Format(@"{0}/Scripts/myscipt.js", basePath));
        //        //string[] arr = new string[] { cert, ts, nullPara, page, token };
        //        //engine.Execute("var $CFMethod=$.ck;");
        //        //var publickey = engine.CallFunction("$CFMethod", arr);
        //        //return publickey.ToString();
        //        var scriptStr = @"function hash2(a,b){ return a+b}";
        //        engine.Execute(scriptStr);

        //        // 调用js方法
        //        result = engine.CallFunction<string>("hash2", 1, 2);
        //    }

        //    return result;
        //}

        public void RemoveVariable(string key)
        {
            this._engine.RemoveVariable(key);
        }

        public void SetVariable(string key, object value)
        {
            this._engine.SetVariableValue(key, value);
        }

        public object GetVariable(string key)
        {
            var result = this._engine.GetVariableValue(key);
            return result;
        }

        public int GetInteger(string key)
        {
            int result = this._engine.GetVariableValue<int>(key);
            return result;
        }

        public double GetNumber(string key)
        {
            double result = this._engine.GetVariableValue<double>(key);
            return result;
        }

        public string GetString(string key)
        {
            string result = this._engine.GetVariableValue<string>(key);
            return result;
        }

        public void DoString(string scriptStr)
        {
            this._engine.Execute(scriptStr);
        }

        public void ExecuteFile(string filename)
        {
            this._engine.ExecuteFile(filename, Encoding.UTF8);
        }

        public object CallFunction(string functionName, params object[] args)
        {
            var result = this._engine.CallFunction(functionName, args);
            return result;
        }

        /// <summary>
        /// static function
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="type"></param>
        public void RegisterFunction(string itemName, Type type)
        {
            this._engine.EmbedHostType(itemName, type);
        }

        /// <summary>
        /// method
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="obj"></param>
        public void RegisterMethod(string itemName, object obj)
        {
            this._engine.EmbedHostObject(itemName, obj);
        }

        public T CallFunction<T>(string functionName, params object[] args)
        {
            var result = this._engine.CallFunction<T>(functionName, args);
            return result;
        }

        public void Close()
        {
            this._engine.Dispose();
        }

    }
}
