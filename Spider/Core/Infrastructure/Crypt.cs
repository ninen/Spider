﻿/*
 * ==============================================================================
 *
 * Filename: Crypt
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/2 9:13:49
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Crypt
    {
        /// <summary>
        /// md5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetMD5(string str)
        {
            //MD5类是抽象类
            MD5 md5 = MD5.Create();
            //需要将字符串转成字节数组
            byte[] buffer = Encoding.Default.GetBytes(str);
            //加密后是一个字节类型的数组，这里要注意编码UTF8/Unicode等的选择
            byte[] md5buffer = md5.ComputeHash(buffer);
            StringBuilder sb = new StringBuilder();
            // 通过使用循环，将字节类型的数组转换为字符串，此字符串是常规字符格式化所得
            foreach (byte b in md5buffer)
            {
                //得到的字符串使用十六进制类型格式。格式后的字符是小写的字母，如果使用大写（X）则格式后的字符是大写字符 
                //但是在和对方测试过程中，发现我这边的MD5加密编码，经常出现少一位或几位的问题；
                //后来分析发现是 字符串格式符的问题， X 表示大写， x 表示小写， 
                //X2和x2表示不省略首位为0的十六进制数字；
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Base64Encode(string str)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Base64Decode(string str)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(str));
        }
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        public static string AesEncrypt(string str, string key, string iv)
        {
            byte[] keyArray = Encoding.UTF8.GetBytes(key);
            byte[] toEncryptArray = Encoding.UTF8.GetBytes(str);
            byte[] ivArray = Encoding.UTF8.GetBytes(iv);
            using (RijndaelManaged rDel = new RijndaelManaged())
            {
                rDel.Key = keyArray;
                rDel.IV = ivArray;
                rDel.Mode = CipherMode.CBC;
                rDel.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = rDel.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        public static string AesDecrypt(string str, string key, string iv)
        {
            byte[] keyArray = Encoding.UTF8.GetBytes(key);
            byte[] toEncryptArray = Convert.FromBase64String(str);
            byte[] ivArray = Encoding.UTF8.GetBytes(iv);
            using (RijndaelManaged rDel = new RijndaelManaged())
            {
                rDel.Key = keyArray;
                rDel.IV = ivArray;
                rDel.Mode = CipherMode.CBC;
                rDel.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = rDel.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                return Encoding.UTF8.GetString(resultArray);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        public static string DesEncrypt(string str, string key, string iv)
        {
            //把密钥转换成字节数组
            byte[] keyBytes = Encoding.ASCII.GetBytes(key);
            //把向量转换成字节数组
            byte[] ivBytes = Encoding.ASCII.GetBytes(iv);
            //声明1个新的DES对象 
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            //开辟一块内存流
            MemoryStream msEncrypt = new MemoryStream();
            //把内存流对象包装成加密流对象 
            CryptoStream csEncrypt = new CryptoStream(msEncrypt, des.CreateEncryptor(keyBytes, ivBytes), CryptoStreamMode.Write);
            //把加密流对象包装成写入流对象
            StreamWriter swEncrypt = new StreamWriter(csEncrypt);
            //写入流对象写入明文  
            swEncrypt.WriteLine(str);
            //写入流关闭  
            swEncrypt.Close();
            //加密流关闭  
            csEncrypt.Close();
            //把内存流转换成字节数组，内存流现在已经是密文了  
            byte[] bytesCipher = msEncrypt.ToArray();
            //内存流关闭 
            msEncrypt.Close();
            //将字节数组转化成Base64字符串
            return Convert.ToBase64String(bytesCipher);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        public static string DesDecrypt(string str, string key, string iv)
        {
            //将密文通过Base64位还原成字节数组
            byte[] cipherByte = Convert.FromBase64String(str);
            //把密钥转换成字节数组
            byte[] keyBytes = Encoding.ASCII.GetBytes(key);
            //把向量转换成字节数组
            byte[] ivBytes = Encoding.ASCII.GetBytes(iv);
            //声明1个新的DES对象 
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            //开辟一块内存流，并存放密文字节数组
            MemoryStream msDecrypt = new MemoryStream(cipherByte);
            //把内存流对象包装成解密流对象 
            CryptoStream csDecrypt = new CryptoStream(msDecrypt, des.CreateDecryptor(keyBytes, ivBytes), CryptoStreamMode.Read);
            //把解密流对象包装成写入流对象
            StreamReader srDecrypt = new StreamReader(csDecrypt);
            //明文=读出流的读出内容   
            string strPlainText = srDecrypt.ReadLine();
            //读出流关闭  
            srDecrypt.Close();
            //解密流关闭  
            csDecrypt.Close();
            //内存流关闭  
            msDecrypt.Close();
            //返回明文  
            return strPlainText;
        }

    }
}
