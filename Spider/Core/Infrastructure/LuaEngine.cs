﻿/*
 * ==============================================================================
 *
 * Filename: LuaEngine
 * Description: 注意:NLua的版本要用1.5,因为服务器不是GCLIB2.9版本
 *
 * Version: 1.0
 * Created: 2022/3/7 23:26:33
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NLua;

namespace Core
{
    public class LuaEngine : Lua
    {
        public LuaEngine()
        {
            this.State.Encoding = Encoding.UTF8;
        }

        public LuaFunction RegisterMethod(string path, object target, MethodBase function)
        {
            var luaFunction = this.RegisterFunction(path, target, function);
            return luaFunction;
        }

        public object GetUserData(string key)
        {
            var result = this.GetObjectFromPath(key);
            return result;
        }

        public void SetUserData(string path, object target)
        {
            this.SetObjectToPath(path, target);
        }

        public void RequireF(string moduleName, object target)
        {
            this.NewTable(moduleName);
            var methods = target.GetType().GetMethods();
            foreach (MethodInfo item in methods)
            {
                var luaApiAttr = item.GetCustomAttribute<LuaAPIAttribute>();
                if (luaApiAttr != null)
                {
                    if (string.IsNullOrEmpty(luaApiAttr.Name))
                    {
                        string path = string.Format("{0}.{1}", moduleName, item.Name.ToLower());
                        this.RegisterMethod(path, target, item);
                    }
                    else
                    {
                        string path = string.Format("{0}.{1}", moduleName, luaApiAttr.Name);
                        this.RegisterMethod(path, target, item);
                    }
                }
            }
        }

        public void RequireF(object target)
        {
            string moduleName = target.GetType().Name.ToLower();
            this.RequireF(moduleName, target);
        }

        public void Clear()
        {
            this.Close();
            this.Dispose();
        }
    }
}
