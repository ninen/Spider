﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IEntryable
    {
        public object Create();
        public Action<object, MessageType, int, int, object[]> Init(ICommandable cmd, bool isHotfix, object ud, string args);
        public void Release(bool isHotfix, object ud);
        public void Signal(object ud);
    }
}
