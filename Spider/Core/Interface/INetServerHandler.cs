﻿/*
 * ==============================================================================
 *
 * Filename: ISocketServerHandler
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 14:46:35
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Transport.Channels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface INetServerHandler
    {
        void Read(IHandlerContext ctx, string msg);
        void Connect(IHandlerContext ctx);
        void DisConnect(IHandlerContext ctx);
        void Exception(IHandlerContext ctx, Exception exception);
    }
}
