﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface INetServer
    {
        void Start(int inetPort);
        void Start(string inetHost, int inetPort);
        void Stop();
    }
}
