﻿namespace Core
{
    public interface IHandlerContext
    {
        public string RemoteAddress();
        public void CloseAsync();
        public void WriteAsync(object message);
        public void WriteAndFlushAsync(object message);
    }
}
