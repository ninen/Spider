﻿namespace Core
{
    internal interface IConfigureable
    {
        AppConfig Configure(string filename);
    }
}
