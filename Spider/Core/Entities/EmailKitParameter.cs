﻿/*
 * ==============================================================================
 *
 * Filename: EmailKitParameter
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/9 0:12:46
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class EmailKitParameter
    {
        /// <summary>
        /// 发送人名字
        /// </summary>
        public string FromName { get; set; }
        /// <summary>
        /// 发送人地址
        /// </summary>
        public string FromAddress { get; set; }
        /// <summary>
        /// 接收者名字
        /// </summary>
        public string ToName { get; set; }
        /// <summary>
        /// 接收者地址
        /// </summary>
        public string ToAddress { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Body { get; set; }
    }
}
