﻿/*
 * ==============================================================================
 *
 * Filename: Message
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/10 0:01:28
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    internal class Message
    {
        public MessageType Type { get; set; }
        public int Source { get; set; }
        public int Session { get; set; }
        public object[] Args { get; set; }
    }
}
