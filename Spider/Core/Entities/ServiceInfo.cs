﻿/*
 * ==============================================================================
 *
 * Filename: ServiceInfo
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/25 18:50:02
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ServiceInfo
    {
        public int Handle { get; set; }
        public string Alias { get; set; }
        public string DllFileName { get; set; }
        public string Result { get; set; }
        public string Args { get; set; }
        public int MsgCount { get; set; }
        public int Session { get; set; }
        public int Status { get; set; }
    }
}
