﻿/*
 * ==============================================================================
 *
 * Filename: LoadService
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/26 19:45:12
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// 服务参数
    /// </summary>
    internal class LoadService
    {
        public string DllFileName { get; set; }
        public string Args { get; set; }
        public string Alias { get; set; }
    }
}
