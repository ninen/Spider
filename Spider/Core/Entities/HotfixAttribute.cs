﻿/*
 * ==============================================================================
 *
 * Filename: HotfixAttribute
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/2 10:53:40
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class HotfixAttribute : Attribute
    {
        public bool IsIgnore { get; }

        public HotfixAttribute(bool isIgnore)
        {
            this.IsIgnore = isIgnore;
        }
    }
}
