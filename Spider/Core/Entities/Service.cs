﻿/*
 * ==============================================================================
 *
 * Filename: Service
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 22:45:06
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core
{
    internal class Service : ICommandable
    {
        private static object rwlock = new object();
        private ServiceStatus status;
        private double busyTime;
        private Dictionary<string, Func<object[], string>> cmdFuncs;
        private int session;
        private string dllFileName;
        private string args;
        private IEntryable entry;
        private string result;
        private object userData; // service user data
        public int Handle { get; }
        LogProvider Logger { get; set; }
        Action<object, MessageType, int, int, object[]> Callback { get; set; }
        public AppContext Context { get; }
        public Service(AppContext context, int handle,
            IEntryable entry, string dllFileName, string args)
        {
            this.Context = context;
            this.session = 1;
            this.Logger = null;
            this.Handle = handle;
            this.dllFileName = dllFileName;
            this.args = args;
            this.busyTime = UtilMethod.GetTimeStamp();
            this.result = string.Empty;
            this.entry = entry;
            this.cmdFuncs = new Dictionary<string, Func<object[], string>>();
            this.cmdFuncs.Add("TIMEOUT", this.CMD_TIMEOUT);
            this.cmdFuncs.Add("REG", this.CMD_REG);
            this.cmdFuncs.Add("QUERY", this.CMD_QUERY);
            this.cmdFuncs.Add("EXIT", this.CMD_EXIT);
            this.cmdFuncs.Add("KILL", this.CMD_KILL);
            this.cmdFuncs.Add("LAUNCH", this.CMD_LAUNCH);
            this.cmdFuncs.Add("GETSTRING", this.CMD_GETSTRING);
            this.cmdFuncs.Add("SETSTRING", this.CMD_SETSTRING);
            this.cmdFuncs.Add("LOG", this.CMD_LOG);
            this.cmdFuncs.Add("ERROR", this.CMD_ERROR);
            this.cmdFuncs.Add("SERVICES", this.CMD_SERVICES);
            this.cmdFuncs.Add("HANDLE", this.CMD_HANDLE);
            this.cmdFuncs.Add("LISTEN", this.CMD_LISTEN);
            this.cmdFuncs.Add("UNLISTEN", this.CMD_UNLISTEN);
            this.cmdFuncs.Add("SEND", this.CMD_SEND);
            this.cmdFuncs.Add("REDIRECT", this.CMD_REDIRECT);
            this.cmdFuncs.Add("NETSEND", this.CMD_NETSEND);

            // 为什么要在这里初始化服务
            // 因为lua服务中要第一䖞取别名,不然通过消息别的服务可能通过别名查找服务时找不到
            this.userData = this.entry.Create();
            this.Callback = this.entry.Init(this, false, this.userData, this.args);
            this.status = ServiceStatus.Init;
        }

        /// <summary>
        /// 调用ReleaseFunc
        /// </summary>
        public void InvokeReleaseFunc()
        {
            this.entry?.Release(false, this.userData);
        }

        /// <summary>
        /// 调用Callback
        /// </summary>
        /// <param name="type"></param>
        /// <param name="session"></param>
        /// <param name="source"></param>
        /// <param name="args"></param>
        public void InvokeCallback(MessageType type, int session, int source, object[] args)
        {
            this?.Callback(this.userData, type, session, source, args);
        }

        public ServiceStatus Status
        {
            get
            {
                ServiceStatus status;
                lock (rwlock)
                {
                    status = this.status;
                }
                return status;
            }
            set
            {
                bool result = true;
                ServiceStatus preStatus;
                lock (rwlock)
                {
                    // 去更新
                    preStatus = this.status;
                    if (value == ServiceStatus.Dispatch
                        && this.status == ServiceStatus.Init)
                    {
                        this.status = value;
                    }
                    else if (value == ServiceStatus.Hotfix
                        && this.status == ServiceStatus.Dispatch)
                    {
                        this.status = value;
                    }
                    else if (value == ServiceStatus.Dispatch
                        && this.status == ServiceStatus.Hotfix)
                    {
                        this.status = value;
                    }
                    else if (value == ServiceStatus.Delete
                        && (this.status == ServiceStatus.Hotfix
                        || this.status == ServiceStatus.Dispatch))
                    {
                        this.status = value;
                    }
                    else if (value == ServiceStatus.Clear
                        && this.status == ServiceStatus.Delete)
                    {
                        this.status = value;
                    }
                    else
                    {
                        result = false;
                    }
                }
                if (result)
                {
                    Context.LogManager.Log(this.Handle, string.Format("set status success {0}->{1}", preStatus, status));
                }
                else
                {
                    Context.LogManager.Error(this.Handle, string.Format("set status failed {0}->{1}", preStatus, status));
                }
            }
        }

        /// <summary>
        /// new session
        /// </summary>
        /// <returns></returns>
        private int NewSession()
        {
            if (this.session >= int.MaxValue)
            {
                this.session = 1;
            }
            else
            {
                this.session++;
            }
            return this.session;
        }

        /// <summary>
        /// update time
        /// </summary>
        public void UpdateTime()
        {
            this.busyTime = UtilMethod.GetTimeStamp();
        }

        /// <summary>
        /// 空闲超过1分钟
        /// </summary>
        public bool IsFree()
        {
            double now = UtilMethod.GetTimeStamp();
            return now >= this.busyTime + 600000;
        }

        /// <summary>
        /// 给dest发送消息
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="type"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public int Send(int dest, MessageType type, params object[] args)
        {
            var mailbox = Context.MailboxManager.QueryMailbox(dest);
            if (mailbox != null)
            {
                int session = 0;
                if (type == MessageType.TAG_ALLOCSESSION)
                {
                    session = this.NewSession();
                }
                mailbox.Deliver(this.Handle, type, session, args);
                return session;
            }
            else
            {
                this.result = string.Format("handle={0} not found type={1}", UtilMethod.ToAddr(dest), type);
                this.Error(this.result);
                return 0;
            }
        }

        /// <summary>
        /// 对某个session进行调用
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="type"></param>
        /// <param name="session"></param>
        /// <param name="dataArray"></param>
        /// <returns></returns>
        public int Redirect(int dest, MessageType type, int session, params object[] args)
        {
            var mailbox = Context.MailboxManager.QueryMailbox(dest);
            if (mailbox != null)
            {
                mailbox.Deliver(this.Handle, type, session, args);
            }
            else
            {
                this.Error(string.Format("handle = {0} not found type={1} session={2}", UtilMethod.ToAddr(dest), type, session));
            }
            return session;
        }


        public void Hotfix()
        {
            var newEntry = UtilMethod.Load(this.dllFileName);
            if (newEntry != null)
            {
                this.entry.Release(true, this.userData);
                this.entry = newEntry;

                // 旧属性
                var oldProperties = this.userData.GetType().GetProperties();

                // 忽略不需要热更的值
                var newUserData = this.entry.Create();
                PropertyInfo[] newPropertyInfos = newUserData.GetType().GetProperties();
                foreach (PropertyInfo item in newPropertyInfos)
                {
                    var oldProperty = oldProperties
                        .Where(it => it.Name == item.Name)
                        .FirstOrDefault();
                    if (oldProperty != null)
                    {
                        // 忽略
                        var hotfixAttr = item.GetCustomAttribute<HotfixAttribute>();
                        if (hotfixAttr == null)
                        {
                            continue;
                        }
                        if (hotfixAttr.IsIgnore)
                        {
                            item.SetValue(newUserData, oldProperty.GetValue(this.userData));
                        }
                    }
                    else
                    {
                        Context.LogManager.Error("没有该属性:" + item.Name);
                    }
                }

                this.userData = newUserData;
                this.Callback = this.entry.Init(this, true, this.userData, this.args);
            }
            else
            {
                Context.LogManager.Error(string.Format("load module failed dll = {1}!", this.dllFileName));
            }
        }

        public ServiceInfo GetServiceInfo()
        {
            var msgCount = Context.MailboxManager.GetMailboxCount(this.Handle);
            var alias = Context.ServiceManager.QueryAlias(this.Handle);
            return new ServiceInfo()
            {
                Handle = this.Handle,
                Alias = alias,
                MsgCount = msgCount,
                Args = this.args,
                Status = Convert.ToInt32(this.Status),
                DllFileName = this.dllFileName,
                Result = this.result,
                Session = this.session
            };
        }

        /// <summary>
        /// 执行命令
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="paramters"></param>
        /// <returns></returns>
        public string Command(string cmd, params object[] paramters)
        {
            if (this.cmdFuncs.ContainsKey(cmd))
            {
                return this.cmdFuncs[cmd](paramters);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 定时器
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private string CMD_TIMEOUT(params object[] parameters)
        {
            if (parameters.Count() >= 1)
            {
                int session = this.NewSession();
                int ti = Convert.ToInt32(parameters[0]);
                List<object> args = new List<object>();
                for (int i = 1; i < parameters.Length; i++)
                {
                    args.Add(parameters[i]);
                }
                Context.TimerManager.Add(new TimerWheel()
                {
                    Count = 0,
                    Args = args.ToArray(),
                    Handle = this.Handle,
                    Second = ti,
                    Session = session
                });
                this.result = string.Format("{0} Success", "TIMEOUT");
                return session.ToString();
            }
            else
            {
                this.result = string.Format("{0} Fail", "TIMEOUT");
                return "0";
            }
        }

        /// <summary>
        /// 起别名
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private string CMD_REG(params object[] parameters)
        {
            string alias = parameters[0].ToString();
            if (string.IsNullOrEmpty(alias))
            {
                this.result = string.Format("{0} is null or empty", alias);
                this.Error(this.result);
            }
            else
            {
                if (alias.IndexOf('.') == 0)
                {
                    if (!Context.ServiceManager.Alias(this.Handle, alias))
                    {
                        this.result = string.Format("{0} fail", alias);
                        this.Log(this.result);
                    }
                }
                else if (alias.IndexOf(":") == 0)
                {
                    if (!Context.ServiceManager.Alias(this.Handle, alias))
                    {
                        this.result = string.Format("{0} fail", alias);
                        this.Log(this.result);
                    }
                }
            }

            return this.Handle.ToString();
        }

        /// <summary>
        /// 通过别名查服务
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private string CMD_QUERY(params object[] parameters)
        {
            string alias = parameters[0].ToString();
            if (string.IsNullOrEmpty(alias))
            {
                return string.Empty;
            }
            else
            {
                int handle = 0;
                if (alias.IndexOf('.') == 0)
                {
                    // 本地服务
                    handle = Context.ServiceManager.QueryHandle(alias);
                }
                else if (alias.IndexOf(':') == 0)
                {
                    // 全局服务
                    // 对应服务节点查
                }
                if (handle != 0)
                {
                    return UtilMethod.ToAddr(handle);
                }
                else
                {
                    this.result = string.Format("Not found service, the alias is {0} of the service", alias);
                    this.Log(this.result);
                    return string.Empty;
                }
            }
        }

        private string CMD_EXIT(params object[] parameters)
        {
            this.Status = ServiceStatus.Delete;
            return string.Empty;
        }

        private string CMD_KILL(params object[] parameters)
        {
            string alias = parameters[0].ToString();
            if (alias.IndexOf('.') == 0)
            {
                int handle = Context.ServiceManager.QueryHandle(alias);
                var service = Context.ServiceManager.QueryService(handle);
                if (service != null)
                {
                    service.Status = ServiceStatus.Delete;
                }
                return string.Empty;
            }
            else if (alias.IndexOf(':') == 0)
            {
                return string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        private string CMD_LAUNCH(params object[] paramters)
        {
            if (paramters.Count() == 3)
            {
                int handle = Context.ServiceManager.CreateService(paramters[1].ToString(), paramters[2].ToString(), paramters[0].ToString());
                return handle.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        private string CMD_GETSTRING(params object[] paramters)
        {
            string result = Context.EnvManager.Get(paramters[0].ToString());
            return result;
        }

        private string CMD_SETSTRING(params object[] paramters)
        {
            Context.EnvManager.Set(paramters[0].ToString(), paramters[1].ToString());
            return string.Empty;
        }

        private string CMD_ENDLESS(params object[] paramters)
        {
            return string.Empty;
        }

        /// <summary>
        /// 所有服务都杀死
        /// </summary>
        /// <param name="paramters"></param>
        /// <returns></returns>
        public string CMD_ABORT(params object[] paramters)
        {
            return string.Empty;
        }

        /// <summary>
        /// 关闭单独日志
        /// </summary>
        /// <param name="paramters"></param>
        /// <returns></returns>
        public string CMD_LOGOFF(params object[] paramters)
        {
            return string.Empty;
        }

        /// <summary>
        /// 开启单独日志
        /// </summary>
        /// <param name="paramters"></param>
        /// <returns></returns>
        public string CMD_LOGON(params object[] paramters)
        {
            return string.Empty;
        }

        /// <summary>
        /// 打印信息
        /// </summary>
        /// <param name="paramters"></param>
        /// <returns></returns>
        public string CMD_LOG(params object[] paramters)
        {
            List<string> args = new List<string>();
            foreach (var item in paramters)
            {
                args.Add(item.ToString());
            }
            Context.LogManager.Log(this.Handle, args.ToArray());
            return string.Empty;
        }

        /// <summary>
        /// 打印错误信息
        /// </summary>
        /// <param name="paramters"></param>
        /// <returns></returns>
        public string CMD_ERROR(params object[] paramters)
        {
            List<string> args = new List<string>();
            foreach (var item in paramters)
            {
                args.Add(item.ToString());
            }
            Context.LogManager.Error(this.Handle, args.ToArray());
            return string.Empty;
        }

        /// <summary>
        /// 服务信息
        /// </summary>
        /// <param name="paramters"></param>
        /// <returns></returns>
        public string CMD_SERVICES(params object[] paramters)
        {
            var serviceInfoList = Context.ServiceManager.AllServices();
            return JsonHelper.Serialize(serviceInfoList);
        }

        /// <summary>
        /// handle
        /// </summary>
        /// <param name="paramters"></param>
        /// <returns></returns>
        public string CMD_HANDLE(params object[] paramters)
        {
            return this.Handle.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        public string CMD_LISTEN(params object[] paramters)
        {
            Context.NetManager.AddListener(this.Handle);
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        public string CMD_UNLISTEN(params object[] paramters)
        {
            Context.NetManager.RemoveListener(this.Handle);
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paramters"></param>
        public string CMD_SEND(params object[] paramters)
        {
            int dest = Convert.ToInt32(paramters[0]);
            MessageType type = (MessageType)paramters[1];
            object[] args = paramters[2] as object[];
            int count = this.Send(dest, type, args);
            return count.ToString();
        }

        public string CMD_REDIRECT(params object[] paramters)
        {
            int dest = Convert.ToInt32(paramters[0]);
            MessageType type = (MessageType)paramters[1];
            int session = Convert.ToInt32(paramters[2]);
            object[] args = paramters[3] as object[];
            int count = this.Redirect(dest, type, session, args);
            return count.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paramters"></param>
        public string CMD_NETSEND(params object[] paramters)
        {
            string remoteAddress = paramters[0].ToString();
            object message = paramters[1];
            Context.NetManager.Write(remoteAddress, message);
            return string.Empty;
        }

        /// <summary>
        /// 升级
        /// </summary>
        /// <param name="dllFileName"></param>
        /// <param name="args"></param>
        public void Upgrade(string dllFileName, string args)
        {
            this.dllFileName = dllFileName;
            this.args = args;
            this.Status = ServiceStatus.Hotfix;
        }

        public int MessageDispatch(Monitor monitor, Mailbox mailbox)
        {
            int msgCount = mailbox.Count();
            if (msgCount > 0)
            {
                Message message = mailbox.Take();
                if (message != null)
                {
                    monitor.Trigger(message.Source, mailbox.Handle);
                    try
                    {
                        this.UpdateTime();
                        this.InvokeCallback(message.Type, message.Session, message.Source, message.Args);
                    }
                    catch (Exception ex)
                    {
                        Context.LogManager.Error(this.Handle, ex.ToString());
                    }

                    monitor.Trigger(0, 0);
                }

                return msgCount - 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
