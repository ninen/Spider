﻿/*
 * ==============================================================================
 *
 * Filename: HotfixConfig
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/26 19:45:12
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// 服务热更参数
    /// </summary>
    internal class HotfixConfig
    {
        public int Handle { get; set; }
        public string DllFileName { get; set; }
        public string Args { get; set; }
    }
}
