﻿/*
 * ==============================================================================
 *
 * Filename: LuaAPIAttribute
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/11 23:19:25
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class LuaAPIAttribute : Attribute
    {
        public string Name { get; }

        public LuaAPIAttribute()
        {
            this.Name = string.Empty;
        }

        public LuaAPIAttribute(string name)
        {
            this.Name = name;
        }
    }
}
