﻿/*
 * ==============================================================================
 *
 * Filename: TimerWheel
 * Description: 时间轮节点
 *
 * Version: 1.0
 * Created: 2022/3/26 11:01:02
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    internal class TimerWheel
    {
        public int Handle { get; set; } 
        public int Session { get; set; }
        /// <summary>
        /// 0.01s
        /// </summary>
        public int Second { get; set; }
        public object[] Args { get; set; }
        public int Count { get; set; }
    }
}
