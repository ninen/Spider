﻿/*
 * ==============================================================================
 *
 * Filename: AppContext
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/1 17:17:18
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    internal sealed class AppContext
    {
        public EnvManager EnvManager { get; }
        public ServiceManager ServiceManager { get; }
        public MonitorManager MonitorManager { get; }
        public NetManager NetManager { get; }
        public ModuleManager ModuleManager { get; }
        public TimerManager TimerManager { get; }
        public LogManager LogManager { get; }
        public MailboxManager MailboxManager { get; }
        public AppConfig Config { get; }

        public AppContext(AppConfig config)
        {
            this.Config = config;
            this.EnvManager = new EnvManager();
            this.MailboxManager = new MailboxManager();
            this.MonitorManager = new MonitorManager(this);
            this.ServiceManager = new ServiceManager(this);
            this.ModuleManager = new ModuleManager(this);
            this.LogManager = new LogManager(this);
            this.NetManager = new NetManager(this);
            this.TimerManager = new TimerManager();
        }
    }
}
