﻿/*
 * ==============================================================================
 *
 * Filename: Monitor
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 23:29:19
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    internal class Monitor
    {
        /// <summary>
        ///版本号 
        /// </summary>
        public int Version { get; set; }
        /// <summary>
        ///上个版本号 
        /// </summary>
        public int CheckVersion { get; set; }
        /// <summary>
        /// 源
        /// </summary>
        public int Source { get; set; }
        /// <summary>
        /// 目标
        /// </summary>
        public int Destination { get; set; }

        public Monitor(int version, int checkVersion, int source, int destination)
        {
            this.Version = version;
            this.CheckVersion = checkVersion;
            this.Source = source;
            this.Destination = destination;
        }

        public void Trigger(int source, int destination)
        {
            this.Source = source;
            this.Destination = destination;
            this.Version++;
        }

        public bool Check()
        {
            if (this.Version == this.CheckVersion)
            {
                if (this.Destination != 0)
                {
                    return false;
                }
            }
            else
            {
                this.CheckVersion = this.Version;
            }

            return true;
        }

        public string GetError()
        {
            string data = string.Format("A message from [ {0} ] to [ {1} ] maybe in an endless loop (version = {2})", UtilMethod.ToAddr(this.Source), UtilMethod.ToAddr(this.Destination), this.Version);
            return data;
        }

    }
}
