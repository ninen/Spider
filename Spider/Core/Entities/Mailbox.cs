﻿/*
 * ==============================================================================
 *
 * Filename: Mailbox
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/11 19:03:20
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    internal class Mailbox
    {
        public int Handle { get; }
        private Queue<Message> dataPool;
        private static object rwlock = new object();

        public Mailbox(int handle)
        {
            this.Handle = handle;
            this.dataPool = new Queue<Message>();
        }

        public int Deliver(int source, MessageType type, int session, params object[] args)
        {
            Message message = new Message()
            {
                Args = args,
                Session = session,
                Source = source,
                Type = type
            };
            lock (rwlock)
            {
                this.dataPool.Enqueue(message);
            }
            return session;
        }

        public Message Take()
        {
            Message message = null;
            lock (rwlock)
            {
                message = this.dataPool.Dequeue();
            }
            return message;
        }

        public int Count()
        {
            return this.dataPool.Count();
        }
    }
}
