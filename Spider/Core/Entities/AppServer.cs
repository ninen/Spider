﻿/*
 * ==============================================================================
 *
 * Filename: AppServer
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 22:33:19
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Linq;
using System.Threading;

namespace Core
{
    internal sealed class AppServer
    {
        private AppContext Context { get; }
        private AutoResetEvent autoResetEvent;
        private RegisteredWaitHandle registeredWaitHandle;

        public AppServer(AppConfig config)
        {
            this.autoResetEvent = new AutoResetEvent(false);
            this.Context = new AppContext(config);
        }

        public void HotfixWorker(object obj)
        {
            Context.LogManager.Log("hotfix start");
            while (!Context.MonitorManager.Quit)
            {
                Context.ModuleManager.Hotfix();
            }
        }

        public void MonitorWorker(object obj)
        {
            Context.LogManager.Log("monitor start");
            while (!Context.MonitorManager.Quit)
            {
                Context.MonitorManager.Check();
            }
            Context.NetManager.Stop();
        }

        public void TimerWorker(object obj)
        {
            Context.LogManager.Log("timer start");
            while (!Context.MonitorManager.Quit)
            {
                Context.TimerManager.Tick((timerWheel) =>
                {
                    var mailbox = Context.MailboxManager.QueryMailbox(timerWheel.Handle);
                    if (mailbox != null)
                    {
                        mailbox.Deliver(UtilConstants.SystemHandle, MessageType.RESPONSE, timerWheel.Session, timerWheel.Args);
                    }
                    return true;
                });
            }
        }

        public void HandleWorker(object obj)
        {
            Context.LogManager.Log("handle start");
            Monitor monitor = Context.MonitorManager.QueryMonitor(obj.ToString());
            while (!Context.MonitorManager.Quit)
            {
                Mailbox mailbox = Context.MailboxManager.Take();
                if (mailbox != null)
                {
                    if (Context.ServiceManager.Action(monitor, mailbox))
                    {
                        Context.MailboxManager.Deliver(mailbox);
                    }
                }
                else
                {
                    Context.MonitorManager.Wait();
                }
            }
        }
     
        private void CheckThreadPool(object state, bool timeout)
        {
            int workerThreads = 0;
            int maxWorkerThreads = 0;
            int compleThreads = 0;
            int maxCompleThreads = 0;
            ThreadPool.GetAvailableThreads(out workerThreads, out compleThreads);
            ThreadPool.GetMaxThreads(out maxWorkerThreads, out maxCompleThreads);
            //当可用的线数与池程池最大的线程相等时表示线程池中所有的线程已经完成
            // 当前完成端口线程同样
            if (workerThreads == maxWorkerThreads)
            {
                //当执行此方法后CheckThreadPool将不再执行
                registeredWaitHandle.Unregister(null);
                //此处加入所有线程完成后的处理代码
                //线程全部退出了,退出
                this.autoResetEvent.Set();
            }
        }

        private void PrintBlessing()
        {
            Context.LogManager.Log("                 _ooOoo_  ");
            Context.LogManager.Log("                o8888888o  ");
            Context.LogManager.Log("                88\" . \"88    ");
            Context.LogManager.Log("                (| -_- |)    ");
            Context.LogManager.Log("             ____/`---'\\____   ");
            Context.LogManager.Log("           .'  \\|     |//  `.    ");
            Context.LogManager.Log("           /  \\|||  :  |||//  \\    ");
            Context.LogManager.Log("          /  _||||| -:- |||||-  \\    ");
            Context.LogManager.Log("          |   | \\\\  -  /// |   |    ");
            Context.LogManager.Log("         | \\_|  ''\\---/''  |   |   ");
            Context.LogManager.Log("          \\  .-\\__  `-`  ___/-. /    ");
            Context.LogManager.Log("         ___`. .'  /--.--\\  `. . __    ");
            Context.LogManager.Log("      .\"\" '<  `.___\\_<|>_/___.'  >'\"\".    ");
            Context.LogManager.Log("     | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |    ");
            Context.LogManager.Log("   \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /    ");
            Context.LogManager.Log("  =====`-.____`-.___\\_____/___.-`____.-'======    ");
            Context.LogManager.Log(" `^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^      ");
            Context.LogManager.Log(" `          佛祖保佑       永无Bug     ");
            Context.LogManager.Log(" `          快加工资       不改需求      ");
        }

        /// <summary>
        /// 
        /// </summary>
        private void PrintSystemInfo()
        {
            // 创建工作线程
            int processorCount = Environment.ProcessorCount;
            Context.LogManager.Log("the count of the processor is ", processorCount.ToString());
            Context.LogManager.Log("root directory: ", Context.Config.Root);
            Context.LogManager.Log("ip address: ", Context.Config.IP, ":", Context.Config.Port.ToString());
            Context.LogManager.Log("hotfix: ", Context.Config.Hotfix);
        }

        public void Start()
        {
            // 环境变量设置
            Context.EnvManager.Set("Root", Context.Config.Root);
            Context.EnvManager.Set("LuaPath", Context.Config.LuaPath);
            Context.EnvManager.Set("LuaCPath", Context.Config.LuaCPath);

            // 先初始化LogService
            var logModule = Context.Config.LoadModules
                .Where(it => it.Alias == UtilConstants.LogServiceAlias)
                .FirstOrDefault();
            if (logModule != null)
            {
                Context.ServiceManager.CreateService(logModule.DllFileName, logModule.Args, logModule.Alias);
            }
            else
            {
                ErrorMessage.ThrowUnloadException("Log service");
            }

            // 打印祝福信息
            this.PrintBlessing();

            // 打印系统信息
            this.PrintSystemInfo();

            // 再初始化其它服务
            foreach (var item in Context.Config.LoadModules)
            {
                if (item.Alias != UtilConstants.LogServiceAlias)
                {
                    Context.ServiceManager.CreateService(item.DllFileName, item.Args, item.Alias);
                }
            }

            // hotfix 热更线程
            ThreadPool.QueueUserWorkItem(new WaitCallback(HotfixWorker));

            // worker
            for (int i = 1; i <= Context.Config.WorkerThreads; i++)
            {
                string id = Context.MonitorManager.CreateMonitor();
                ThreadPool.QueueUserWorkItem(new WaitCallback(HandleWorker), id);
            }

            // monitor
            ThreadPool.QueueUserWorkItem(new WaitCallback(MonitorWorker));

            // timer
            ThreadPool.QueueUserWorkItem(new WaitCallback(TimerWorker));

            // 启动net服务
            Context.NetManager.Start();

            // 阻塞,需要所有线程退出才会退出
            this.registeredWaitHandle = ThreadPool.RegisterWaitForSingleObject(this.autoResetEvent, this.CheckThreadPool, null, 1000, false);
            this.autoResetEvent.WaitOne();
        }
    }
}

