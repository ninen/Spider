﻿/*
 * ==============================================================================
 *
 * Filename: AppConfig
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/26 8:39:48
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System.Collections.Generic;

namespace Core
{
    internal sealed class AppConfig
    {
        /// <summary>
        /// 根目录
        /// </summary>
        public string Root { get; set; }

        /// <summary>
        /// ip
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// port
        /// </summary>
        public int Port { get; set; }

        // 网络类型
        public NetProtocolType NetProtocolType { get; set; }

        /// <summary>
        /// socket's threads
        /// </summary>
        public int SocketThreads { get; set; }

        /// <summary>
        /// 工作线程
        /// </summary>
        public int WorkerThreads { get; set; }

        /// <summary>
        /// 热更配置文件
        /// </summary>
        public string Hotfix { get; set; }

        /// <summary>
        /// 要加载的模块
        /// </summary>
        public List<LoadService> LoadModules { get; set; }

        /// <summary>
        /// lua的路径 
        /// </summary>
        public string LuaPath { get; set; }

        /// <summary>
        /// lua的c路径
        /// </summary>
        public string LuaCPath { get; set; }

        /// <summary>
        /// the option of the websocket
        /// </summary>
        public NettyWSServerOption WSServerOption { get; set; }

        /// <summary>
        /// the option of the tcp 
        /// </summary>
        public NettyTcpServer01Option TcpServer01Option { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public NettyTcpServer02Option TcpServer02Option { get; set; }
    }
}
