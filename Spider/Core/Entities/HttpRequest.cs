﻿/*
 * ==============================================================================
 *
 * Filename: HttpRequest
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/25 23:51:15
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class HttpRequest
    {
        public string Uri { get; set; }
        public string Method { get; set; }
        public string ProtocolName { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public string Parameters { get; set; }
    }
}
