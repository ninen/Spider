﻿/*
 * ==============================================================================
 *
 * Filename: NetManager
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/13 11:18:44
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    internal sealed class NetManager
    {
        private INetServer netServer;
        private List<int> listenHandles;
        private CacheProvider<string, IHandlerContext> handlerContexts;
        public AppContext Context { get; }
        public NetManager(AppContext context)
            : this(context, context.Config.NetProtocolType)
        {

        }

        public NetManager(AppContext context, NetProtocolType protocolType)
        {
            this.listenHandles = new List<int>();
            this.handlerContexts = new LocalCache<string, IHandlerContext>();
            this.Context = context;
            switch (protocolType)
            {
                case NetProtocolType.NettyTcpServer01:
                    this.netServer = new NettyTcpServer01(Context.Config.SocketThreads, new Bridge(context), Context.Config.TcpServer01Option);
                    break;
                case NetProtocolType.NettyTcpServer02:
                    this.netServer = new NettyTcpServer02(Context.Config.SocketThreads, new Bridge(context), Context.Config.TcpServer02Option);
                    break;
                case NetProtocolType.NettyWSServer:
                    this.netServer = new NettyWSServer(Context.Config.SocketThreads, new Bridge(context), Context.Config.WSServerOption);
                    break;
                case NetProtocolType.KCPServer:
                    this.netServer = new KcpServer(new Bridge(context));
                    break;
                case NetProtocolType.NettyHttpServer:
                    this.netServer = new NettyHttpServer(Context.Config.SocketThreads, new Bridge(context));
                    break;
                default:
                    break;
            }
        }

        public bool ContainsHandlerContext(IHandlerContext ctx)
        {
            string id = Crypt.GetMD5(ctx.RemoteAddress());
            return this.handlerContexts.ContainsKey(id);
        }

        public void AddHandlerContext(IHandlerContext ctx)
        {
            string id = Crypt.GetMD5(ctx.RemoteAddress());
            this.handlerContexts.Add(id, ctx);
        }

        public void RemoveHandlerContext(IHandlerContext ctx)
        {
            string id = Crypt.GetMD5(ctx.RemoteAddress());
            this.handlerContexts.Delete(id);
        }

        public void Write(string remoteAddress, object msg)
        {
            string id = Crypt.GetMD5(remoteAddress);
            var handleContext = this.handlerContexts.GetValue(id);
            handleContext?.WriteAndFlushAsync(msg);
        }

        public void ForeachListener(Action<int> action)
        {
            for (int i = 0; i < this.listenHandles.Count(); i++)
            {
                action?.Invoke(this.listenHandles[i]);
            }
        }

        public void AddListener(int handle)
        {
            if (!this.listenHandles.Contains(handle))
            {
                this.listenHandles.Add(handle);
            }
        }

        public void RemoveListener(int handle)
        {
            if (this.listenHandles.Contains(handle))
            {
                this.listenHandles.Remove(handle);
            }
        }

        public void Start()
        {
            Start(Context.Config.IP, Context.Config.Port);
        }

        public void Start(string ip, int port)
        {
            try
            {
                if (string.IsNullOrEmpty(ip))
                {
                    this.netServer.Start(port);
                }
                else
                {
                    this.netServer.Start(ip, port);
                }
            }
            catch (Exception ex)
            {
                Context.LogManager.Error(ex.ToString());
            }
        }

        public void Stop()
        {
            this.netServer.Stop();
        }
    }
}
