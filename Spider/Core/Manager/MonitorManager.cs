﻿/*
 * ==============================================================================
 *
 * Filename: MonitorManager
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 23:28:44
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Threading;

namespace Core
{
    internal sealed class MonitorManager
    {
        /// <summary>
        /// monitor
        /// </summary>
        private CacheProvider<string, Monitor> monitorProvider;
        private AutoResetEvent autoResetEvent;
        public int Sleep { get; set; }
        public bool Quit { get; set; }
        public AppContext Context { get; }

        public MonitorManager(AppContext context)
        {
            this.autoResetEvent = new AutoResetEvent(false);
            this.monitorProvider = new LocalCache<string, Monitor>();
            this.Sleep = 0;
            this.Quit = false;
            this.Context = context;
        }

        public string CreateMonitor()
        {
            string id = Guid.NewGuid().ToString();
            this.monitorProvider.Add(id, new Monitor(0, 0, 0, 0));
            return id;
        }

        public Monitor QueryMonitor(string id)
        {
            return this.monitorProvider.GetValue(id);
        }

        public void Check()
        {
            this.monitorProvider.ForEach((key, value) => {
                if (!value.Check())
                {
                    Context.LogManager.Error(value.GetError());
                }
            });
           
            for (int i = 0; i < this.monitorProvider.Count(); i++)
            {
                Thread.Sleep(1000);
            }
        }
     

        public void Wait()
        {
            //所有消息队列都是空的
            ++this.Sleep;
            this.autoResetEvent.WaitOne();
            --this.Sleep;
        }


        public void Wakeup()
        {
            this.autoResetEvent.Set();
        }

    }
}
