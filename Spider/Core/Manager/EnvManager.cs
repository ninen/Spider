﻿/*
 * ==============================================================================
 *
 * Filename: EnvManager
 * Description: 全局环境变量管理
 *
 * Version: 1.0
 * Created: 2022/4/19 9:47:18
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

namespace Core
{
    internal sealed class EnvManager
    {
        private EnvProvider envProvider;
        public EnvManager()
        {
            this.envProvider = new LocalEnv();
        }

        public string Get(string k)
        {
            return envProvider.Query(k);
        }

        public void Set(string k, string v)
        {
            envProvider.Update(k, v);
        }

    }
}
