﻿/*
 * ==============================================================================
 *
 * Filename: TimerManager
 * Description: 定器时管理,使用时间轮算法
 *
 * Version: 1.0
 * Created: 2022/3/26 10:57:33
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Core
{
    internal sealed class TimerManager
    {
        private int currentIndex;
        private const int TIMER_WHEEL_GROUP_NUM = 10;
        private const int MAX_GROUP_INDEX = TIMER_WHEEL_GROUP_NUM - 1;
        private QueueProvider<TimerWheel>[] timerWheelGroups;
        private Stopwatch sw;
        private int tickCount;

        public TimerManager()
        {
            this.tickCount = 0;
            this.sw = new Stopwatch();
            this.currentIndex = 0;
            this.timerWheelGroups = new LocalQueue<TimerWheel>[TIMER_WHEEL_GROUP_NUM];
            for (int i = 0; i < TIMER_WHEEL_GROUP_NUM; i++)
            {
                this.timerWheelGroups[i] = new LocalQueue<TimerWheel>();
            }
        }

        public void Add(TimerWheel timerWheel)
        {
            // 计算在哪个齿上
            timerWheel.Count = timerWheel.Second / MAX_GROUP_INDEX;
            int index = (timerWheel.Second + this.currentIndex) % MAX_GROUP_INDEX;
            this.timerWheelGroups[index].Enqueue(timerWheel);
        }

        /// <summary>
        /// 对精度要求高就用这个
        /// </summary>
        /// <param name="w"></param>
        /// <param name="duration"></param>
        void Spin(Stopwatch w, int duration)
        {
            var current = w.ElapsedMilliseconds;
            while ((w.ElapsedMilliseconds - current) < duration)
            {
                Thread.SpinWait(10);
            }
        }

        public void Tick(Func<TimerWheel, bool> callback)
        {
            int duration = 10;
            sw.Start();
            //this.Spin(this.sw, duration / 2);
            Thread.Sleep(duration / 2);
            sw.Stop();

            long timeout = (sw.ElapsedMilliseconds / duration) - this.tickCount;
            sw.Start();
            while (timeout-- > 0)
            {
                if (this.tickCount >= int.MaxValue - 1)
                {
                    this.sw.Reset();
                    this.tickCount = 0;
                }
                else
                {
                    this.tickCount++;
                }
                this.currentIndex++;
                this.currentIndex %= MAX_GROUP_INDEX;
                var timerWheelQueue = this.timerWheelGroups[this.currentIndex];
                int count = timerWheelQueue.Count();
                for (int i = 0; i < count; i++)
                {
                    var timerWheel = timerWheelQueue.Dequeue();
                    if (timerWheel.Count <= 0)
                    {
                        callback?.Invoke(timerWheel);
                    }
                    else
                    {
                        timerWheel.Count--;
                        timerWheelQueue.Enqueue(timerWheel);
                    }
                }
            }

            sw.Stop();
        }


    }
}
