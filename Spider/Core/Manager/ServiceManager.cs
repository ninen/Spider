﻿/*
 * ==============================================================================
 *
 * Filename: ServiceManager
 * Description: 服务管理
 *
 * Version: 1.0
 * Created: 2022/3/9 18:35:36
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System.Collections.Generic;

namespace Core
{
    internal sealed class ServiceManager
    {
        const int HANDLE_REMOTE_SHIFT = 24;
        const int HANDLE_MASK = 0xffffff;

        private int handleIndex;
        private CacheProvider<int, Service> serviceProvider;
        private CacheProvider<int, string> aliasProvider;
        public AppContext Context { get; }

        public ServiceManager(AppContext context)
        {
            this.handleIndex = 1;
            this.serviceProvider = new LocalCache<int, Service>();
            this.aliasProvider = new LocalCache<int, string>();
            this.Context = context;
        }

        private int GenerateHandle()
        {
            int handle = this.handleIndex & HANDLE_MASK;
            this.handleIndex = handle + 1;
            return handle;
        }

        public bool Alias(int handle, string alias)
        {
            if (this.aliasProvider.GetKey(alias) != 0)
            {
                return false;
            }
            var value = this.aliasProvider.GetValue(handle);
            if (string.IsNullOrEmpty(value))
            {
                this.aliasProvider.Add(handle, alias);
            }
            else
            {
                this.aliasProvider.Update(handle, alias);
            }
            return true;
        }

        public int QueryHandle(string alias)
        {
            return this.aliasProvider.GetKey(alias);
        }

        public string QueryAlias(int handle)
        {
            return this.aliasProvider.GetValue(handle);
        }

        public bool DeleteAlias(int handle)
        {
            return this.aliasProvider.Delete(handle);
        }

        public int CreateService(string dllfilename, string args, string alias)
        {
            var entry = UtilMethod.Load(dllfilename);
            if (entry != null)
            {
                int handle = this.GenerateHandle();
                this.serviceProvider.Add(handle, new Service(Context, handle, entry, dllfilename, args));
                Context.MailboxManager.CreateMailbox(handle);
                this.Alias(handle, alias);
                return handle;
            }
            else
            {
                Context.LogManager.Error(string.Format("{0} not found!", dllfilename));
                return 0;
            }
        }

        /// <summary>
        /// 创建服务
        /// </summary>
        /// <param name="dllfilename"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public int CreateService(string dllfilename, string args)
        {
            var entry = UtilMethod.Load(dllfilename);
            if (entry != null)
            {
                int handle = this.GenerateHandle();
                this.serviceProvider.Add(handle, new Service(Context, handle, entry, dllfilename, args));
                Context.MailboxManager.CreateMailbox(handle);
                return handle;
            }
            else
            {
                Context.LogManager.Error(string.Format("{0} not found!", dllfilename));
                return 0;
            }
        }

        public Service QueryService(int handle)
        {
            return this.serviceProvider.GetValue(handle);
        }

        public Service QueryService(string alias)
        {
            var handle = this.QueryHandle(alias);
            if (handle != 0)
            {
                return this.QueryService(handle);
            }
            else
            {
                return null;
            }
        }

        public List<ServiceInfo> AllServices()
        {
            List<ServiceInfo> serviceInfoList = new List<ServiceInfo>();
            this.serviceProvider.ForEach((k, v) =>
            {
                serviceInfoList.Add(v.GetServiceInfo());
            });
            return serviceInfoList;
        }

        public bool Action(Monitor monitor, Mailbox mailbox)
        {
            bool result = false;
            Service service = Context.ServiceManager.QueryService(mailbox.Handle);
            if (service != null)
            {
                ServiceStatus serviceStatus = service.Status;
                switch (serviceStatus)
                {
                    case ServiceStatus.Init:
                        result = InitAction(service);
                        break;
                    case ServiceStatus.Dispatch:
                        result = DispatchAction(monitor, service, mailbox);
                        break;
                    case ServiceStatus.Hotfix:
                        result = HotfixAction(service);
                        break;
                    case ServiceStatus.Delete:
                        result = DeleteAction(monitor, service, mailbox);
                        break;
                    case ServiceStatus.Clear:
                        result = ClearAction(service);
                        break;
                }
            }

            return result;
        }

        private bool InitAction(Service service)
        {
            service.Status = ServiceStatus.Dispatch;
            return true;
        }

        private bool DispatchAction(Monitor monitor, Service service, Mailbox mailbox)
        {
            service.MessageDispatch(monitor, mailbox);
            return true;
        }

        private bool HotfixAction(Service service)
        {
            // 热更新
            service.Hotfix();
            service.Status = ServiceStatus.Dispatch;
            return true;
        }

        private bool DeleteAction(Monitor monitor, Service service, Mailbox mailbox)
        {
            // 可以分发消息
            int msgCount = service.MessageDispatch(monitor, mailbox);
            if (msgCount <= 0 && service.IsFree())
            {
                service.Status = ServiceStatus.Clear;
            }
            return true;
        }

        private bool ClearAction(Service service)
        {
            Context.LogManager.Log(service.Handle, "Clear");
            Context.NetManager.RemoveListener(service.Handle);
            Context.MailboxManager.DeleteMailbox(service.Handle);
            Context.ServiceManager.DeleteAlias(service.Handle);
            this.serviceProvider.Delete(service.Handle);
            service.InvokeReleaseFunc();
            return false;
        }
    }
}
