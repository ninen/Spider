﻿/*
 * ==============================================================================
 *
 * Filename: ModuleManager
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 19:37:48
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Core
{
    internal sealed class ModuleManager
    {
        /// <summary>
        /// 8s
        /// </summary>
        private int clock;
        private HotfixStatus hotfixStatus;
        public AppContext Context { get; }

        public ModuleManager(AppContext context)
        {
            this.Context = context;
            this.hotfixStatus = HotfixStatus.Check;
            this.clock = 8;
        }

        private void Check()
        {
            if (!string.IsNullOrEmpty(Context.Config.Hotfix)
                && File.Exists(Context.Config.Hotfix))
            {
                this.hotfixStatus = HotfixStatus.Countdown;
                Context.LogManager.Log("需要热更,准备8s后开始热更");
            }

            Thread.Sleep(3000);
        }

        private void Countdown()
        {
            if (this.clock <= 0)
            {
                this.hotfixStatus = HotfixStatus.Handle;
                this.clock = 8;
            }
            else
            {
                Context.LogManager.Log(string.Format("热更倒计时{0}s", this.clock));
                this.clock--;
                Thread.Sleep(1000);
            }
        }

        private void Handle()
        {
            string hotfix = Context.Config.Hotfix;
            string fileContent = FileHelper.ReadFile(hotfix);
            FileHelper.DeleteFile(hotfix);
            if (!string.IsNullOrEmpty(fileContent))
            {
                try
                {
                    Handle(fileContent);
                }
                catch (Exception ex)
                {
                    Context.LogManager.Error("热更新发生异常:" + ex.ToString());
                }
            }
            else
            {
                Context.LogManager.Error("热更新配置文件为空!");
            }

            this.hotfixStatus = HotfixStatus.Finish;
        }

        private void Handle(string fileContent)
        {
            var settings = JsonHelper.Deserialize<List<HotfixConfig>>(fileContent);
            if (settings == null && settings.Count <= 0)
            {
                return;
            }
            foreach (var item in settings)
            {
                var service = Context.ServiceManager.QueryService(item.Handle);
                if (service != null)
                {
                    // 升级服务
                    service.Upgrade(item.DllFileName, item.Args);
                }
                else
                {
                    // 创建服务
                    Context.ServiceManager.CreateService(item.DllFileName, item.Args);
                }
            }
        }

        private void Finish()
        {
            Context.LogManager.Log("已经完成热更");
            this.hotfixStatus = HotfixStatus.Check;
        }

        private void Other()
        {
            Context.LogManager.Error("热更新陷入未知状态,请检查!");
            this.hotfixStatus = HotfixStatus.Check;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Hotfix()
        {
            switch (hotfixStatus)
            {
                case HotfixStatus.Check:
                    Check();
                    break;
                case HotfixStatus.Countdown:
                    Countdown();
                    break;
                case HotfixStatus.Handle:
                    Handle();
                    break;
                case HotfixStatus.Finish:
                    Finish();
                    break;
                default:
                    Other();
                    break;
            }
        }


    }
}
