﻿/*
 * ==============================================================================
 *
 * Filename: LogManager
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/13 0:28:43
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System.Linq;
using System.Text;

namespace Core
{
    internal sealed class LogManager
    {
        public AppContext Context { get; }
        public LogManager(AppContext context)
        {
            this.Context = context;
        }

        public void Log(int source, params string[] parameters)
        {
            if (parameters == null || parameters.Count() <= 0)
            {
                return;
            }
            StringBuilder sb = new StringBuilder();
            foreach (var item in parameters)
            {
                sb.Append(item);
            }

            var service = Context.ServiceManager.QueryService(UtilConstants.LogServiceAlias);
            if (service != null)
            {
                var mailbox = Context.MailboxManager.QueryMailbox(service.Handle);
                if (mailbox != null)
                {
                    mailbox.Deliver(source, MessageType.TEXT, 0, sb.ToString());
                }
            }
        }

        public void Log(params string[] parameters)
        {
            this.Log(UtilConstants.SystemHandle, parameters);
        }

        public void Error(int source, params string[] parameters)
        {
            if (parameters == null || parameters.Count() <= 0)
            {
                return;
            }
            StringBuilder sb = new StringBuilder();
            foreach (var item in parameters)
            {
                sb.Append(item);
            }

            var service = Context.ServiceManager.QueryService(UtilConstants.LogServiceAlias);
            if (service != null)
            {
                var mailbox = Context.MailboxManager.QueryMailbox(service.Handle);
                if (mailbox != null)
                {
                    mailbox.Deliver(source, MessageType.ERROR, 0, sb.ToString());
                }
            }
        }

        public void Error(params string[] parameters)
        {
            this.Error(UtilConstants.SystemHandle, parameters);
        }

    }
}
