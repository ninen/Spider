﻿/*
 * ==============================================================================
 *
 * Filename: MailboxManager
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/1 14:31:27
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System.Collections.Generic;
using System.Linq;

namespace Core
{
    internal sealed class MailboxManager
    {
        private Queue<Mailbox> globalMailbox;
        private CacheProvider<int, Mailbox> mailboxProvider;
        private static object rwlock = new object();

        public MailboxManager()
        {
            this.globalMailbox = new Queue<Mailbox>();
            this.mailboxProvider = new LocalCache<int, Mailbox>();
        }

        public void CreateMailbox(int handle)
        {
            if (!this.mailboxProvider.ContainsKey(handle))
            {
                var mailbox = new Mailbox(handle);
                this.mailboxProvider.Add(handle, mailbox);
                this.Deliver(mailbox);
            }
        }

        public void DeleteMailbox(int handle)
        {
            if (this.mailboxProvider.ContainsKey(handle))
            {
                this.mailboxProvider.Delete(handle);
            }
        }

        public Mailbox QueryMailbox(int handle)
        {
            if (this.mailboxProvider.ContainsKey(handle))
            {
                return this.mailboxProvider.GetValue(handle);
            }
            else
            {
                return null;
            }
        }

        public int GetMailboxCount(int handle)
        {
            var mailbox = this.QueryMailbox(handle);
            if (mailbox != null)
            {
                return mailbox.Count();
            }
            else
            {
                return 0;
            }
        }

        public void Deliver(Mailbox mailbox)
        {
            if (mailbox != null)
            {
                lock (rwlock)
                {
                    this.globalMailbox.Enqueue(mailbox);
                }
            }
        }

        public Mailbox Take()
        {
            if (this.globalMailbox.Count() > 0)
            {
                Mailbox mailbox = null;
                lock (rwlock)
                {
                    if (this.globalMailbox.Count() > 0)
                    {
                        mailbox = this.globalMailbox.Dequeue();
                    }
                }

                return mailbox;
            }

            return null;
        }

    }
}
