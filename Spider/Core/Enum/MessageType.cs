﻿/*
 * ==============================================================================
 *
 * Filename: MessageType
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/11 19:44:03
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

namespace Core
{
    public enum MessageType
    {
        TEXT = 0,
        RESPONSE = 1,
        MULTICAST = 2,
        CLIENT = 3,
        SYSTEM = 4,
        HARBOR = 5,
        SOCKET = 6,
        ERROR = 7,
        HTTP = 8,

        SOCKET_DATA = 101,
        SOCKET_CONNECT = 102,
        SOCKET_CLOSE = 103,
        SOCKET_ACCEPT = 104,
        SOCKET_ERROR = 105,
        SOCKET_WARNING = 106,

        TAG_ALLOCSESSION = 0x20000,
    }
}
