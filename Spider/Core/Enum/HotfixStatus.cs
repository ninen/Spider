﻿/*
 * ==============================================================================
 *
 * Filename: HotfixStatus
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/27 9:07:48
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    internal enum HotfixStatus
    {
        /// <summary>
        /// 检查
        /// </summary>
        Check,
        /// <summary>
        /// 倒计时
        /// </summary>
        Countdown,
        /// <summary>
        /// 更新
        /// </summary>
        Handle,
        /// <summary>
        /// 完成
        /// </summary>
        Finish,
    }

}
