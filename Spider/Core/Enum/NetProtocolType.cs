﻿/*
 * ==============================================================================
 *
 * Filename: NetProtocolType
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/1 21:24:12
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    internal enum NetProtocolType
    {
        NettyTcpServer01,
        NettyTcpServer02,
        NettyWSServer,
        KCPServer,
        NettyHttpServer
    }
}
