﻿/*
 * ==============================================================================
 *
 * Filename: ServiceStatus
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/11 22:04:48
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    internal enum ServiceStatus
    {
        /// <summary>
        /// 初始化
        /// </summary>
        Init,
        /// <summary>
        /// 分发消息
        /// </summary>
        Dispatch,
        /// <summary>
        /// 热更新
        /// </summary>
        Hotfix,
        /// <summary>
        /// 删除
        /// </summary>
        Delete = 100,
        /// <summary>
        /// 清除
        /// </summary>
        Clear
    }
}
