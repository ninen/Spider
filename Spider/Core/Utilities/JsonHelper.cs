﻿/*
 * ==============================================================================
 *
 * Filename: JsonHelper
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/25 17:20:53
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class JsonHelper
    {
        public static string Serialize(object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        public static T Deserialize<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}
