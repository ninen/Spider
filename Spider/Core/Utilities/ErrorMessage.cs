﻿/*
 * ==============================================================================
 *
 * Filename: ErrorMessage
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/25 16:52:22
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    internal static partial class ErrorMessage
    {
        public static void ThrowUnloadException(string alias)
        {
            throw new Exception(string.Format("{0} isn't loaded", alias));
        }

    }
}
