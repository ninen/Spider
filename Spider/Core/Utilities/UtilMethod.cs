﻿/*
 * ==============================================================================
 *
 * Filename: UtilMethod
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/26 21:26:05
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Globalization;
using System.Linq;
using System.Runtime.Loader;
using System.Text;

namespace Core
{
    public class UtilMethod
    {
        public static double GetTimeStamp()
        {
            TimeSpan ts = DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return ts.TotalMilliseconds;
        }

        public static string ToAddr(int handle)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(":");
            sb.Append(handle.ToString("x8"));
            return sb.ToString();
        }

        public static int ToHandle(string hexHandle)
        {
            if (hexHandle.IndexOf(":") == 0)
            {
                string handleStr = hexHandle.Substring(1);
                int handle = int.Parse(handleStr, NumberStyles.HexNumber);
                return handle;
            }
            else
            {
                return UtilConstants.SystemHandle;
            }
        }

        public static IEntryable Load(string filename)
        {
            var fs = FileHelper.ReadStream(filename);
            if (fs != null)
            {
                var assContext = new AssemblyLoadContext(Guid.NewGuid().ToString("N"), true);
                try
                {
                    var assembly = assContext.LoadFromStream(fs);
                    var type = assembly.GetTypes()
                        .Where(it => it.Name == UtilConstants.ModuleEntry)
                        .FirstOrDefault();
                    if (type != null && typeof(IEntryable).IsAssignableFrom(type))
                    {
                        var entry = Activator.CreateInstance(type) as IEntryable;
                        return entry;
                    }
                    assContext?.Unload();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    assContext = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }

            return null;
        }
    }
}
