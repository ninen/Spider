﻿/*
 * ==============================================================================
 *
 * Filename: CommandExtensions
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/7/7 11:19:29
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public static class CommandExtensions
    {
        public static int Handle(this ICommandable cmd)
        {
            int handle = Convert.ToInt32(cmd.Command("HANDLE"));
            return handle;
        }

        public static string Services(this ICommandable cmd)
        {
            string str = cmd.Command("SERVICES");
            return str;
        }

        public static void Log(this ICommandable cmd, params object[] paramters)
        {
            cmd.Command("LOG", paramters);
        }

        public static void Error(this ICommandable cmd, params object[] paramters)
        {
            cmd.Command("ERROR", paramters);
        }

        public static int Send(this ICommandable cmd, int dest, MessageType type, params object[] args)
        {
            int count = Convert.ToInt32(cmd.Command("SEND", dest, type, args));
            return count;
        }

        public static int Redirect(this ICommandable cmd, int dest, MessageType type, int session, params object[] args)
        {
            int count = Convert.ToInt32(cmd.Command("REDIRECT", dest, type, args));
            return count;
        }

        public static void Listen(this ICommandable cmd)
        {
            cmd.Command("LISTEN");
        }

        public static void Unlisten(this ICommandable cmd)
        {
            cmd.Command("UNLISTEN");
        }

        public static void NetSend(this ICommandable cmd, string remoteAddress, object msg)
        {
            cmd.Command("NETSEND", remoteAddress, msg);
        }

    }
}
