﻿/*
 * ==============================================================================
 *
 * Filename: NettySepBaseFrameDecoder
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/23 16:17:25
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using System.Collections.Generic;

namespace Core
{
    public class NettySepBaseFrameDecoder : ByteToMessageDecoder
    {
        private int maxFrameLength;
        private bool stripDelimiter;
        private bool failFast;
        private IByteBuffer beginDelimiter;
        private IByteBuffer endDelimiter;

        public NettySepBaseFrameDecoder(int maxFrameLength, bool stripDelimiter, bool failFast,  IByteBuffer beginDelimiter, IByteBuffer endDelimiter)
        {
            this.maxFrameLength = maxFrameLength;
            this.stripDelimiter = stripDelimiter;
            this.failFast = failFast;
            this.beginDelimiter = beginDelimiter;
            this.endDelimiter = endDelimiter;
        }

        protected override void Decode(IChannelHandlerContext context, IByteBuffer input, List<object> output)
        {
            int beginDelimiterCount = beginDelimiter.ReadableBytes;
            int endDelimiterCount = endDelimiter.ReadableBytes;
            if (input.ReadableBytes <= beginDelimiterCount + endDelimiterCount)
            {
                return;
            }
            var beginIdx = input.IndexOf(0, input.ReadableBytes, beginDelimiter.GetByte(0));
            if (beginIdx < 0)
            {
                return;
            }

            var endIdx = input.IndexOf(beginIdx, input.ReadableBytes, endDelimiter.GetByte(0));
            if (endIdx >= beginIdx)
            {
                // 拷贝真正数据
                int packetLength = endIdx - beginIdx - 1;
                if (packetLength == 0)
                {
                    input.SkipBytes(endIdx + 1);
                }
                else if (packetLength > this.maxFrameLength)
                {
                    input.SkipBytes(packetLength + beginDelimiterCount + endDelimiterCount);
                }
                else
                {
                    if (this.stripDelimiter)
                    {
                        // 不带分隔符
                        input.SkipBytes(beginDelimiterCount);
                        var dest = input.ReadRetainedSlice(packetLength);
                        input.SkipBytes(endDelimiterCount);
                        output.Add(dest);
                    }
                    else
                    {
                        // 带分隔符
                        var dest = input.ReadRetainedSlice(packetLength + beginDelimiterCount + endDelimiterCount);
                        output.Add(dest);
                    }
                }
            }
            else
            {
                // beginIdx其实不可能为0
                input.SkipBytes(beginIdx);
            }

        }
    }
}
