﻿/*
 * ==============================================================================
 *
 * Filename: NettyHttpServerInitializer
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 15:04:29
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Codecs.Http;
using DotNetty.Transport.Channels;

namespace Core
{
    public class NettyHttpServerInitializer : ChannelInitializer<IChannel>
    {
        private INetServerHandler serverHandler;

        public NettyHttpServerInitializer(INetServerHandler serverHandler)
        {
            this.serverHandler = serverHandler;
        }

        protected override void InitChannel(IChannel channel)
        {
            IChannelPipeline pipeline = channel.Pipeline;
            pipeline.AddLast(new HttpServerCodec());
            // post请求会将头和体分两次发送,用这个可以聚合在一起
            pipeline.AddLast(new HttpObjectAggregator(int.MaxValue));
            pipeline.AddLast(new NettyHttpServerHandler(this.serverHandler));
        }
    }

}
