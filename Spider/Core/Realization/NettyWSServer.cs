﻿/*
 * ==============================================================================
 *
 * Filename: NettyWSServer
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 14:59:06
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using System.Net;

namespace Core
{
    /// <summary>
    /// websocket server
    /// </summary>
    public class NettyWSServer : INetServer
    {
        private IEventLoopGroup bossGroup;
        private IEventLoopGroup workerGroup;
        private NettyWSServerOption option;
        private INetServerHandler serverHandler;
        private IChannel channel;

        private NettyWSServer(int eventLoopCount)
        {
            // MultithreadEventLoopGroup(1) == SingleThreadEventLoop
            this.bossGroup = new MultithreadEventLoopGroup(1);
            this.workerGroup = new MultithreadEventLoopGroup(eventLoopCount);
            this.channel = null;
        }

        public NettyWSServer(int eventLoopCount, INetServerHandler serverHandler, NettyWSServerOption option)
            : this(eventLoopCount)
        {
            this.serverHandler = serverHandler;
            this.option = option;
        }

        public NettyWSServer(int eventLoopCount, INetServerHandler serverHandler)
            : this(eventLoopCount)
        {
            this.serverHandler = serverHandler;
            this.option = new NettyWSServerOption()
            {
                AllIdleTimeSeconds = 0,
                WriterIdleTimeSeconds = 0,
                ReaderIdleTimeSeconds = 120,
                MaxContentLength = 8192,
                WebsocketPath = "/ws"
            };
        }

        public void Start(int inetPort)
        {
            var bootstrap = new ServerBootstrap();
            bootstrap.Group(this.bossGroup, this.workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new NettyWSServerInitializer(this.serverHandler, this.option));
            var bootstrapChannel = bootstrap.BindAsync(inetPort);
            this.channel = bootstrapChannel.Result;
        }

        public void Start(string inetHost, int inetPort)
        {
            var bootstrap = new ServerBootstrap();
            bootstrap.Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new NettyWSServerInitializer(this.serverHandler, this.option));
            var bootstrapChannel = bootstrap.BindAsync(IPAddress.Parse(inetHost), inetPort);
            this.channel = bootstrapChannel.Result;
        }

        public void Stop()
        {
            this.channel?.CloseAsync();
            workerGroup.ShutdownGracefullyAsync();
            bossGroup.ShutdownGracefullyAsync();
        }
    }



}
