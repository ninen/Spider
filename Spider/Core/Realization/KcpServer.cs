﻿/*
 * ==============================================================================
 *
 * Filename: KcpServer
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/11 21:55:13
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core
{
    public class KcpServer : INetServer
    {
        UdpClient udp;
        private CancellationTokenSource cts;
        private CancellationToken ct;
        private uint sid = 0;
        private INetServerHandler serverHandler;
        private static object locker = new object();
        private Dictionary<uint, KcpCallback> kcpSessions;

        public KcpServer(INetServerHandler serverHandler)
        {
            cts = new CancellationTokenSource();
            ct = cts.Token;
            this.serverHandler = serverHandler;
            this.kcpSessions = new Dictionary<uint, KcpCallback>();
        }

        public void Start(IPAddress ip, int port)
        {
            udp = new UdpClient(new IPEndPoint(ip, port));
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                udp.Client.IOControl((IOControlCode)(-1744830452), new byte[] { 0, 0, 0, 0 }, null);
            }
            Task.Run(Update, ct);
            Task.Run(Recive, ct);
        }

        public void Start(string ip, int port) => this.Start(IPAddress.Parse(ip), port);
        public void Start(int port) => this.Start(IPAddress.Any, port);

        void Update()
        {
            while (true)
            {
                // 清理
                lock(locker)
                {
                    foreach (var item in this.kcpSessions)
                    {
                        var recvBuffer = item.Value.Recv();
                        if (recvBuffer != null)
                        {
                            // 接收消息
                            this.serverHandler.Read(new KcpHandlerContext(item.Value), UTF8Encoding.UTF8.GetString(recvBuffer));
                        }

                        // 更新
                        item.Value.Update();

                        // 清理
                        if (item.Value.GetHeart() <= 0)
                        {
                            this.serverHandler.DisConnect(new KcpHandlerContext(item.Value));
                            this.kcpSessions.Remove(item.Key);
                        }
                    }
                }
            }
        }

        async void Recive()
        {
            while (true)
            {
                if (ct.IsCancellationRequested)
                {
                    break;
                }

                // 接收消息
                UdpReceiveResult result = await udp.ReceiveAsync();
                uint sid = BitConverter.ToUInt32(result.Buffer, 0);
                if (sid == 0)
                {
                    KcpCallback kcpSession = new KcpCallback(this.GenerateSID(), result.RemoteEndPoint, this);
                    lock(locker)
                    {
                        this.kcpSessions.Add(kcpSession.GetConv(), kcpSession);
                    }
                    this.serverHandler.Connect(new KcpHandlerContext(kcpSession));

                    // 上线
                    byte[] convBytes = new byte[8];
                    Array.Copy(BitConverter.GetBytes(kcpSession.GetConv()), 0, convBytes, 4, 4);
                    this.UdpSend(convBytes, result.RemoteEndPoint);
                }
                else
                {
                    if (this.kcpSessions.ContainsKey(sid))
                    {
                        var kcpSession = this.kcpSessions[sid];
                        kcpSession.Push(result.Buffer);
                    }
                }
            }
        }

        /// <summary>
        /// udp消息发送
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="remotePoint"></param>
        public void UdpSend(byte[] bytes, IPEndPoint remotePoint)
        {
            if (udp != null)
            {
                udp.SendAsync(bytes, bytes.Length, remotePoint);
            }
        }

        private uint GenerateSID()
        {
            if (sid >= uint.MaxValue)
            {
                sid = 1;
            }
            else
            {
                ++sid;
            }
            return sid;
        }

        public void Stop()
        {
            if (udp != null)
            {
                udp.Close();
                udp = null;
                cts.Cancel();
            }
        }

    }
}
