﻿/*
 * ==============================================================================
 *
 * Filename: NettyHttpServer
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 14:59:06
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using System.Net;

namespace Core
{
    /// <summary>
    /// http server
    /// </summary>
    public class NettyHttpServer : INetServer
    {
        private IEventLoopGroup bossGroup;
        private IEventLoopGroup workerGroup;
        private INetServerHandler nettyEvent;
        private IChannel channel;

        private NettyHttpServer(int eventLoopCount)
        {
            // MultithreadEventLoopGroup(1) == SingleThreadEventLoop
            this.bossGroup = new MultithreadEventLoopGroup(1);
            this.workerGroup = new MultithreadEventLoopGroup(eventLoopCount);
            this.channel = null;
        }

        public NettyHttpServer(int eventLoopCount, INetServerHandler nettyEvent)
            : this(eventLoopCount)
        {
            this.nettyEvent = nettyEvent;
        }

        public void Start(int inetPort)
        {
            var bootstrap = new ServerBootstrap();
            bootstrap.Group(this.bossGroup, this.workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new NettyHttpServerInitializer(this.nettyEvent));
            var bootstrapChannel = bootstrap.BindAsync(inetPort);
            this.channel = bootstrapChannel.Result;
        }

        public void Start(string inetHost, int inetPort)
        {
            var bootstrap = new ServerBootstrap();
            bootstrap.Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new NettyHttpServerInitializer(this.nettyEvent));
            var bootstrapChannel = bootstrap.BindAsync(IPAddress.Parse(inetHost), inetPort);
            this.channel = bootstrapChannel.Result;
        }

        public void Stop()
        {
            this.channel?.CloseAsync();
            workerGroup.ShutdownGracefullyAsync();
            bossGroup.ShutdownGracefullyAsync();
        }
    }



}
