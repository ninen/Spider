﻿/*
 * ==============================================================================
 *
 * Filename: LocalCache
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/24 11:07:46
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public class LocalCache<KeyT, ValueT> : CacheProvider<KeyT, ValueT>
    {
        private static object rwlock = new object();
        private Dictionary<KeyT, ValueT> dataPool;
        public LocalCache()
        {
            this.dataPool = new Dictionary<KeyT, ValueT>();
        }

        public override ValueT GetValue(KeyT key)
        {
            ValueT value = default(ValueT);
            if (this.dataPool.ContainsKey(key))
            {
                lock (rwlock)
                {
                    if (this.dataPool.ContainsKey(key))
                    {
                        value = this.dataPool[key];
                    }
                }
            }
            return value;
        }

        public override KeyT GetKey(ValueT value)
        {
            KeyT key = default(KeyT);
            foreach (var item in this.dataPool)
            {
                if (item.Value.Equals(value))
                {
                    key = item.Key;
                    break;
                }
            }
            return key;
        }

        public override bool Add(KeyT key, ValueT value)
        {
            if (!this.dataPool.ContainsKey(key))
            {
                lock (rwlock)
                {
                    this.dataPool.Add(key, value);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void Update(KeyT key, ValueT value)
        {
            if (this.dataPool.ContainsKey(key))
            {
                lock (rwlock)
                {
                    this.dataPool[key] = value;
                }
            }
            else
            {
                lock (rwlock)
                {
                    this.dataPool.Add(key, value);
                }
            }
        }

        public override bool Delete(KeyT key)
        {
            if (this.dataPool.ContainsKey(key))
            {
                lock (rwlock)
                {
                    this.dataPool.Remove(key);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public override List<ValueT> GetValueList()
        {
            return this.dataPool.Values.ToList();
        }

        public override List<KeyT> GetKeyList()
        {
            return this.dataPool.Keys.ToList();
        }

        public override int Count()
        {
            return this.dataPool.Count();
        }

        public override bool ContainsKey(KeyT key)
        {
            return this.dataPool.ContainsKey(key);
        }

        public override void ForEach(Action<KeyT, ValueT> action)
        {
            foreach (var item in this.dataPool)
            {
                action?.Invoke(item.Key, item.Value);
            }
        }
    }
}
