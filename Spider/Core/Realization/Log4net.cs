﻿/*
 * ==============================================================================
 *
 * Filename: Log4net
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 20:10:42
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Log4net : LogProvider
    {
        //这里的 loginfo 和 log4net.config 里的名字要一样
        private ILog loginfo = null;
        //这里的 logerror 和 log4net.config 里的名字要一样
        private ILog logerror = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename">配置文件</param>
        /// <param name="loginfo"></param>
        /// <param name="logerror"></param>
        public Log4net(string filename, string loginfo, string logerror)
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo(filename));
            this.loginfo = log4net.LogManager.GetLogger(loginfo);
            this.logerror = log4net.LogManager.GetLogger(logerror);
        }

        public override void Log(string info)
        {
            if (loginfo != null && loginfo.IsInfoEnabled)
            {
                loginfo.Info(info);
            }
        }

        public override void Error(string info)
        {
            if (logerror != null && logerror.IsErrorEnabled)
            {
                logerror.Error(info);
            }
        }
    }
}
