﻿/*
 * ==============================================================================
 *
 * Filename: NettyTcpServer01Option
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 13:37:58
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class NettyTcpServer01Option
    {
        // 读超时时间
        public int ReaderIdleTimeSeconds { get; set; } 
        // 写超时时间
        public int WriterIdleTimeSeconds { get; set; }
        // 所有类型的超时时间
        public int AllIdleTimeSeconds { get; set; }

        /// <summary>
        /// 一个包最长是多长
        /// </summary>
        public int MaxFrameLength { get; set; }
        /// <summary>
        /// 长度域的偏移量
        /// </summary>
        public int LengthFieldOffset { get; set; }
        /// <summary>
        /// 长度域长度
        /// </summary>
        public int LengthFieldLength { get; set; }
        /// <summary>
        /// 基于可调整长度的拆包才会用到
        /// 长度域的数值表示的长度加上这个修正值表示的就是带header的包
        /// 没有header就不用了为0即可
        /// 长度 + header + 包体
        /// </summary>
        public int LengthAdjustment { get; set; }
        /// <summary>
        /// 获取完一个完整的数据包之后，忽略前面的n个字节
        /// </summary>
        public int InitialBytesToStrip { get; set; }
    }
}
