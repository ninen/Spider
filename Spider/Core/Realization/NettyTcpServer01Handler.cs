﻿/*
 * ==============================================================================
 *
 * Filename: NettyTcpServer01Handler
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 13:43:54
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Handlers.Timeout;
using DotNetty.Transport.Channels;
using System;

namespace Core
{
    public class NettyTcpServer01Handler : SimpleChannelInboundHandler<string>
    {
        private INetServerHandler serverHandler;
        public NettyTcpServer01Handler(INetServerHandler serverHandler)
        {
            this.serverHandler = serverHandler;
        }

        protected override void ChannelRead0(IChannelHandlerContext ctx, string msg)
        {
            this.serverHandler.Read(new NettyHandlerContext(NettyProtocolType.Tcp, ctx), msg);
        }

        public override void ChannelActive(IChannelHandlerContext ctx)
        {
            this.serverHandler.Connect(new NettyHandlerContext(NettyProtocolType.Tcp, ctx));
            base.ChannelActive(ctx);
        }

        public override void UserEventTriggered(IChannelHandlerContext ctx, object evt)
        {
            if (evt is IdleStateEvent)
            {
                ctx.Channel.CloseAsync();
            }
        }

        public override void ChannelInactive(IChannelHandlerContext ctx)
        {
            this.serverHandler.DisConnect(new NettyHandlerContext(NettyProtocolType.Tcp, ctx));
            base.ChannelInactive(ctx);
        }

        public override void ExceptionCaught(IChannelHandlerContext ctx, Exception exception)
        {
            this.serverHandler.Exception(new NettyHandlerContext(NettyProtocolType.Tcp, ctx), exception);
            ctx.CloseAsync();
        }
    }
}
