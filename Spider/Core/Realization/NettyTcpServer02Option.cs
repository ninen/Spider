﻿/*
 * ==============================================================================
 *
 * Filename: NettyTcpServer02Option
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 13:37:58
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class NettyTcpServer02Option
    {
        // 读超时时间
        public int ReaderIdleTimeSeconds { get; set; } 
        // 写超时时间
        public int WriterIdleTimeSeconds { get; set; }
        // 所有类型的超时时间
        public int AllIdleTimeSeconds { get; set; }

        /// <summary>
        /// 一个包最长是多长
        /// </summary>
        public int MaxFrameLength { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public byte[] BeginDelimiter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public byte[] EndDelimiter { get; set; }
    }
}
