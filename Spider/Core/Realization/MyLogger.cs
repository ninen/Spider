﻿/*
 * ==============================================================================
 *
 * Filename: MyLogger
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/29 13:55:04
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class MyLogger : LogProvider
    {
        public override void Error(string str)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " " + str);
        }

        public override void Log(string str)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " " + str);
        }
    }
}
