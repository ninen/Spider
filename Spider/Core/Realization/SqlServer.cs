﻿/*
 * ==============================================================================
 *
 * Filename: SqlServer
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/25 20:43:32
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class SqlServer : DBProvider
    {
        private SqlConnection conn;
        public SqlServer(string connStr)
        {
            this.conn = new SqlConnection(connStr);
        }

        public override void Close()
        {
            conn.Close();
        }

        public override int NoQuery(string sql, Dictionary<string, object> parameters)
        {
            SqlCommand cmd = new SqlCommand(sql, conn);
            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    cmd.Parameters.Add(new SqlParameter(item.Key, value: item.Value));
                }
            }
            return cmd.ExecuteNonQuery();
        }

        public override bool Open()
        {
            try
            {
                conn.Open();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public override object[] Query(string sql, Dictionary<string, object> parameters)
        {
            SqlCommand cmd = new SqlCommand(sql, conn);
            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    cmd.Parameters.Add(new SqlParameter(item.Key, value: item.Value));
                }
            }
            List<object> records = new List<object>();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Dictionary<string, object> record = new Dictionary<string, object>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string fieldName = reader.GetName(i);
                    object fieldValue = reader.GetValue(i);
                    record.Add(fieldName, fieldValue);
                }
                records.Add(record);
            }
            reader.Close();
            return records.ToArray();
        }
    }
}
