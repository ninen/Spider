﻿/*
 * ==============================================================================
 *
 * Filename: NettyProtocol
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 15:16:29
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// 基本传输协议
    /// </summary>
    public enum NettyProtocolType
    {
        Tcp,
        Udp,
        Websocket,
        Http
    }
}
