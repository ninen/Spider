﻿/*
 * ==============================================================================
 *
 * Filename: NettyHandlerContext
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 15:15:26
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Codecs.Http.WebSockets;
using DotNetty.Transport.Channels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class NettyHandlerContext : IHandlerContext
    {
        public IChannelHandlerContext Context { get; }
        public NettyProtocolType NettyProtocolType { get; }
        public NettyHandlerContext(NettyProtocolType nettyProtocolType, IChannelHandlerContext context)
        {
            this.NettyProtocolType = nettyProtocolType;
            this.Context = context;
        }

        public string RemoteAddress()
        {
            return this.Context.Channel.RemoteAddress.ToString();
        }

        public async void CloseAsync()
        {
            await this.Context.CloseAsync();
        }

        public void WriteAsync(object message)
        {
            if (message != null)
            {
                switch (NettyProtocolType)
                {
                    case NettyProtocolType.Tcp:
                        Context.WriteAsync(message);
                        break;
                    case NettyProtocolType.Udp:
                        break;
                    case NettyProtocolType.Websocket:
                        Context.WriteAsync(new TextWebSocketFrame(message.ToString()));
                        break;
                    case NettyProtocolType.Http:
                        Context.WriteAsync(message);
                        break;
                }
            }
            
        }

        public void WriteAndFlushAsync(object message)
        {
            if (message != null)
            {
                switch (NettyProtocolType)
                {
                    case NettyProtocolType.Tcp:
                        Context.WriteAndFlushAsync(message);
                        break;
                    case NettyProtocolType.Udp:
                        break;
                    case NettyProtocolType.Websocket:
                        Context.WriteAndFlushAsync(new TextWebSocketFrame(message.ToString()));
                        break;
                    case NettyProtocolType.Http:
                        Context.WriteAndFlushAsync(message);
                        break;
                }
            }
        }

    }
}
