﻿/*
 * ==============================================================================
 *
 * Filename: LuaConfiguration
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/20 23:54:57
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using NLua;
using System;
using System.Collections.Generic;

namespace Core
{
    internal class LuaConfiguration : IConfigureable
    {
        public AppConfig Configure(string filename)
        {
            AppConfig config = new AppConfig();
            try
            {
                LuaEngine luaEngine = new LuaEngine();
                var result = luaEngine.DoFile(filename);
                config.Root = luaEngine.GetString("Root");
                config.IP = luaEngine.GetString("IP");
                config.Port = luaEngine.GetInteger("Port");
                config.NetProtocolType = (NetProtocolType)luaEngine.GetInteger("NetProtocolType");
                config.SocketThreads = luaEngine.GetInteger("SocketThreads");
                config.WorkerThreads = luaEngine.GetInteger("WorkerThreads");
                config.Hotfix = luaEngine.GetString("Hotfix");
                config.LuaPath = luaEngine.GetString("LuaPath");
                config.LuaCPath = luaEngine.GetString("LuaCPath");
                config.LoadModules = new List<LoadService>();
                var loadServiceTable = luaEngine.GetTable("LoadServices");
                foreach (var key in loadServiceTable.Keys)
                {
                    LuaTable item = loadServiceTable[key] as LuaTable;
                    config.LoadModules.Add(new LoadService()
                    {
                        Alias = item["Alias"].ToString(),
                        DllFileName = item["DllFileName"].ToString(),
                        Args = item["Args"].ToString()
                    });
                }

                switch (config.NetProtocolType)
                {
                    case NetProtocolType.NettyWSServer:
                        InitWSOption(config, luaEngine);
                        break;
                    case NetProtocolType.NettyTcpServer01:
                        InitNettyTSLF(config, luaEngine);
                        break;
                    case NetProtocolType.NettyTcpServer02:
                        InitNettyTSSEP(config, luaEngine);
                        break;
                }

                luaEngine.Clear();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return config;
        }

        private void InitNettyTSSEP(AppConfig config, LuaEngine luaEngine)
        {
            config.TcpServer02Option = new NettyTcpServer02Option();
            LuaTable tsSepOption = luaEngine.GetTable("TSSEPOption");
            config.TcpServer02Option.ReaderIdleTimeSeconds = Convert.ToInt32(tsSepOption["ReaderIdleTimeSeconds"]);
            config.TcpServer02Option.WriterIdleTimeSeconds = Convert.ToInt32(tsSepOption["WriterIdleTimeSeconds"]);
            config.TcpServer02Option.AllIdleTimeSeconds = Convert.ToInt32(tsSepOption["AllIdleTimeSeconds"]);
            config.TcpServer02Option.MaxFrameLength = Convert.ToInt32(tsSepOption["MaxFrameLength"]);
            config.TcpServer02Option.BeginDelimiter = new byte[] {
                    Convert.ToByte(tsSepOption["BeginDelimiter"])
                };
            config.TcpServer02Option.EndDelimiter = new byte[] {
                    Convert.ToByte(tsSepOption["EndDelimiter"])
                };
        }

        private void InitNettyTSLF(AppConfig config, LuaEngine luaEngine)
        {
            config.TcpServer01Option = new NettyTcpServer01Option();
            LuaTable tslfOption = luaEngine.GetTable("TSLFOption");
            config.TcpServer01Option.ReaderIdleTimeSeconds = Convert.ToInt32(tslfOption["ReaderIdleTimeSeconds"]);
            config.TcpServer01Option.WriterIdleTimeSeconds = Convert.ToInt32(tslfOption["WriterIdleTimeSeconds"]);
            config.TcpServer01Option.AllIdleTimeSeconds = Convert.ToInt32(tslfOption["AllIdleTimeSeconds"]);
            config.TcpServer01Option.MaxFrameLength = Convert.ToInt32(tslfOption["MaxFrameLength"]);
            config.TcpServer01Option.LengthFieldOffset = Convert.ToInt32(tslfOption["LengthFieldOffset"]);
            config.TcpServer01Option.LengthFieldLength = Convert.ToInt32(tslfOption["LengthFieldLength"]);
            config.TcpServer01Option.LengthAdjustment = Convert.ToInt32(tslfOption["LengthAdjustment"]);
            config.TcpServer01Option.InitialBytesToStrip = Convert.ToInt32(tslfOption["InitialBytesToStrip"]);
        }

        private void InitWSOption(AppConfig config, LuaEngine luaEngine)
        {
            config.WSServerOption = new NettyWSServerOption();
            LuaTable wsOption = luaEngine.GetTable("WSOption");
            config.WSServerOption.AllIdleTimeSeconds = Convert.ToInt32(wsOption["AllIdleTimeSeconds"]);
            config.WSServerOption.WriterIdleTimeSeconds = Convert.ToInt32(wsOption["WriterIdleTimeSeconds"]);
            config.WSServerOption.ReaderIdleTimeSeconds = Convert.ToInt32(wsOption["ReaderIdleTimeSeconds"]);
            config.WSServerOption.MaxContentLength = Convert.ToInt32(wsOption["MaxContentLength"]);
            config.WSServerOption.WebsocketPath = wsOption["WebsocketPath"].ToString();
        }
    }
}
