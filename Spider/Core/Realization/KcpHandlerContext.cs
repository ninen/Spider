﻿/*
 * ==============================================================================
 *
 * Filename: KcpHandlerContext
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/5/12 10:26:35
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System.Text;

namespace Core
{
    public class KcpHandlerContext : IHandlerContext
    {
        private KcpCallback kcpCallback;

        public KcpHandlerContext(KcpCallback kcpCallback)
        {
            this.kcpCallback = kcpCallback;
        }

        public void CloseAsync()
        {

        }

        public string RemoteAddress()
        {
            return this.kcpCallback.remoteEndPoint.ToString();
        }

        public void WriteAndFlushAsync(object message)
        {
            var bytes = UTF8Encoding.UTF8.GetBytes(message.ToString());
            this.kcpCallback.Send(bytes);
        }

        public void WriteAsync(object message)
        {
            var bytes = UTF8Encoding.UTF8.GetBytes(message.ToString());
            this.kcpCallback.Send(bytes);
        }
    }
}
