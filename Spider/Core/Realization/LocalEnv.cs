﻿/*
 * ==============================================================================
 *
 * Filename: LocalEnv
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/22 23:23:12
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class LocalEnv : EnvProvider
    {
        private CacheProvider<string, string> cache;

        public LocalEnv()
        {
            this.cache = new LocalCache<string, string>();
        }

        public override string Query(string key)
        {
            string value = this.cache.GetValue(key);
            return value;
        }

        public override void Update(string key, string value)
        {
            this.cache.Update(key, value);
        }
    }
}
