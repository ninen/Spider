﻿/*
 * ==============================================================================
 *
 * Filename: KcpCallback
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/5/12 13:16:05
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Buffers;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Sockets.Kcp;

namespace Core
{
    public class KcpCallback : IKcpCallback
    {
        private KcpServer kcpServer;
        private Kcp kcp;
        private int heart;
        public IPEndPoint remoteEndPoint;
        private double updateTimeStamp;

        public KcpCallback(uint conv, IPEndPoint remoteEndPoint, KcpServer kcpServer)
        {
            this.remoteEndPoint = remoteEndPoint;
            this.kcpServer = kcpServer;
            this.updateTimeStamp = UtilMethod.GetTimeStamp();
            this.heart = 10;
            this.kcp = new Kcp(conv, this);
            this.kcp.NoDelay(1, 10, 2, 1);
            this.kcp.WndSize(64, 64);
            this.kcp.SetMtu(512);
        }

        public uint GetConv()
        {
            return this.kcp.conv;
        }

        public int GetHeart()
        {
            return this.heart;
        }

        /// <summary>
        /// 超过1分钟
        /// </summary>
        /// <returns></returns>
        public bool PassTime()
        {
            double now = UtilMethod.GetTimeStamp();
            if (now >= this.updateTimeStamp + 6000)
            {
                this.updateTimeStamp = now;
                return true;
            }
            else
            {
                return false;
            }
        }

        public int Update()
        {
            if (this.heart > 0)
            {
                this.kcp.Update(DateTime.UtcNow);
                if (this.PassTime())
                {
                    this.heart--;
                }
            }

            return this.heart;
        }

        public void Send(byte[] bytes)
        {
            this.heart = 10;
            this.kcp.Send(bytes.AsSpan());
        }

        public void Push(byte[] recvBuffer)
        {
            this.kcp.Input(recvBuffer.AsSpan());
        }

        public byte[] Recv()
        {
            int len = kcp.PeekSize();
            if (len > 0)
            {
                var buffer = new byte[len];
                if (kcp.Recv(buffer) >= 0)
                {
                    this.heart = 10;
                    return buffer;
                }
            }
            return null;
        }

        private byte[] Compress(byte[] input)
        {
            using (MemoryStream outMS = new MemoryStream())
            {
                using (GZipStream gzs = new GZipStream(outMS, CompressionMode.Compress, true))
                {
                    gzs.Write(input, 0, input.Length);
                    gzs.Close();
                    return outMS.ToArray();
                }
            }
        }

        public void Output(IMemoryOwner<byte> buffer, int avalidLength)
        {
            // 发送消息
            var bytes = buffer.Memory.Slice(0, avalidLength).ToArray();
            kcpServer.UdpSend(bytes, this.remoteEndPoint);
        }
    }
}
