﻿/*
 * ==============================================================================
 *
 * Filename: Mysql
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/22 14:30:46
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Mysql : DBProvider
    {
        MySqlConnection conn;
        public Mysql(string connetStr)
        {
            this.conn = new MySqlConnection(connetStr);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Open()
        {
            try
            {
                conn.Open();
                return true;
            }
            catch 
            {
                conn.Close();
            }
            return false;
        }

        public override int NoQuery(string sql, Dictionary<string, object> parameters)
        {
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    cmd.Parameters.Add(new MySqlParameter(item.Key, value: item.Value));
                }
            }
            return cmd.ExecuteNonQuery();
        }

        public override object[] Query(string sql, Dictionary<string, object> parameters)
        {
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    cmd.Parameters.Add(new MySqlParameter(item.Key, value: item.Value));
                }
            }
            List<object> result = new List<object>();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Dictionary<string, object> table = new Dictionary<string, object>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string fieldName = reader.GetName(i);
                    object fieldValue = reader.GetValue(i);
                    table.Add(fieldName, fieldValue);
                }
                result.Add(table);
            }
            reader.Close();
            return result.ToArray();
        }


        public override void Close()
        {
            conn.Close();
        }
    }
}
