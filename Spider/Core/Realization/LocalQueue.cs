﻿/*
 * ==============================================================================
 *
 * Filename: LocalQueue
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/7/7 9:40:00
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class LocalQueue<T> : QueueProvider<T>
    {
        private Queue<T> dataPool;
        private static object rwlock = new object();
        public LocalQueue()
        {
            this.dataPool = new Queue<T>();
        }

        public override int Count()
        {
            int count = 0;
            lock(rwlock)
            {
                count = this.dataPool.Count();
            }
            return count;
        }

        public override T Dequeue()
        {
            T v = default(T);
            lock(rwlock)
            {
                v = this.dataPool.Dequeue();
            }
            return v;
        }

        public override void Enqueue(T value)
        {
            lock (rwlock)
            {
                this.dataPool.Enqueue(value);
            }
        }
    }
}
