﻿/*
 * ==============================================================================
 *
 * Filename: NettyTcpServer02Initializer
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 13:33:50
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Handlers.Timeout;
using DotNetty.Transport.Channels;
using System.Text;

namespace Core
{
    public class NettyTcpServer02Initializer : ChannelInitializer<IChannel>
    {
        private INetServerHandler netServer;
        private NettyTcpServer02Option option;

        public NettyTcpServer02Initializer(INetServerHandler netServer, NettyTcpServer02Option option)
        {
            this.netServer = netServer;
            this.option = option;
        }

        protected override void InitChannel(IChannel channel)
        {
            IChannelPipeline pipeline = channel.Pipeline;
            IByteBuffer beginDelimiter = Unpooled.CopiedBuffer(this.option.BeginDelimiter);
            IByteBuffer endDelimiter = Unpooled.CopiedBuffer(this.option.EndDelimiter);
            pipeline.AddLast(new IdleStateHandler(this.option.ReaderIdleTimeSeconds, this.option.WriterIdleTimeSeconds, this.option.AllIdleTimeSeconds));
            pipeline.AddLast(new NettySepBaseFrameDecoder(this.option.MaxFrameLength, true, false, beginDelimiter, endDelimiter));
            pipeline.AddLast(new StringDecoder(Encoding.UTF8));
            pipeline.AddLast(new StringEncoder(Encoding.UTF8));
            pipeline.AddLast(new NettySepBaseFrameEncoder(this.option.MaxFrameLength, false, beginDelimiter, endDelimiter));
            pipeline.AddLast(new NettyTcpServer02Handler(this.netServer));
        }
    }
}
