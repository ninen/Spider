﻿/*
 * ==============================================================================
 *
 * Filename: NettyWSServerOption
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 15:00:52
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class NettyWSServerOption
    {
        // 读超时时间
        public int ReaderIdleTimeSeconds { get; set; }
        // 写超时时间
        public int WriterIdleTimeSeconds { get; set; }
        // 所有类型的超时时间
        public int AllIdleTimeSeconds { get; set; }

        /// <summary>
        /// 一个包的内容最长是多长
        /// </summary>
        public int MaxContentLength { get; set; }
        /// <summary>
        /// Websocket路径
        /// </summary>
        public string WebsocketPath { get; set; }
    }
}
