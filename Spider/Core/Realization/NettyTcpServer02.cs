﻿/*
 * ==============================================================================
 *
 * Filename: NettyTcpServer02
 * Description: 
 * tcp server by sep
 * Version: 1.0
 * Created: 2022/3/9 13:28:58
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using System.Net;

namespace Core
{
    /// <summary>
    /// tcp服务器,且以进行分包
    /// </summary>
    public class NettyTcpServer02 : INetServer
    {
        private IEventLoopGroup bossGroup;
        private IEventLoopGroup workerGroup;
        private NettyTcpServer02Option option;
        private INetServerHandler nettyEvent;
        private IChannel channel;

        private NettyTcpServer02(int eventLoopCount)
        {
            // MultithreadEventLoopGroup(1) == SingleThreadEventLoop
            this.bossGroup = new MultithreadEventLoopGroup(1);
            this.workerGroup = new MultithreadEventLoopGroup(eventLoopCount);
            this.channel = null;
        }

        public NettyTcpServer02(int eventLoopCount, INetServerHandler nettyEvent, NettyTcpServer02Option option)
            : this(eventLoopCount)
        {
            this.nettyEvent = nettyEvent;
            this.option = option;
        }

        public NettyTcpServer02(int eventLoopCount, INetServerHandler nettyEvent)
            : this(eventLoopCount)
        {
            this.nettyEvent = nettyEvent;
            this.option = new NettyTcpServer02Option
            {
                ReaderIdleTimeSeconds = 120,
                WriterIdleTimeSeconds = 120,
                AllIdleTimeSeconds = 0,
                MaxFrameLength = 4096,
                BeginDelimiter = new byte[] { 0x2 },
                EndDelimiter = new byte[] { 0x3 },
            };
        }

        public void Start(int inetPort)
        {
            var bootstrap = new ServerBootstrap();
            bootstrap.Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new NettyTcpServer02Initializer(this.nettyEvent, this.option));
            var bootstrapChannel = bootstrap.BindAsync(inetPort);
            this.channel = bootstrapChannel.Result;
        }

        public void Start(string inetHost, int inetPort)
        {
            var bootstrap = new ServerBootstrap();
            bootstrap.Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new NettyTcpServer02Initializer(this.nettyEvent, this.option));
            var ipAddress = IPAddress.Parse(inetHost);
            var bootstrapChannel = bootstrap.BindAsync(ipAddress, inetPort);
            this.channel = bootstrapChannel.Result;
        }

        public void Stop()
        {
            this.channel?.CloseAsync();
            workerGroup.ShutdownGracefullyAsync();
            bossGroup.ShutdownGracefullyAsync();
        }
    }
}
