﻿/*
 * ==============================================================================
 *
 * Filename: NettyTcpServer01Initializer
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 13:33:50
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Codecs;
using DotNetty.Handlers.Timeout;
using DotNetty.Transport.Channels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class NettyTcpServer01Initializer : ChannelInitializer<IChannel>
    {
        private INetServerHandler nettyEvent;
        private NettyTcpServer01Option option;

        public NettyTcpServer01Initializer(INetServerHandler nettyEvent, NettyTcpServer01Option option)
        {
            this.nettyEvent = nettyEvent;
            this.option = option;
        }

        protected override void InitChannel(IChannel channel)
        {
            IChannelPipeline pipeline = channel.Pipeline;

            pipeline.AddLast(new IdleStateHandler(this.option.ReaderIdleTimeSeconds, this.option.WriterIdleTimeSeconds, this.option.AllIdleTimeSeconds));
            pipeline.AddLast(new LengthFieldBasedFrameDecoder(this.option.MaxFrameLength, this.option.LengthFieldOffset, this.option.LengthFieldLength, this.option.LengthAdjustment, this.option.InitialBytesToStrip));
            pipeline.AddLast(new LengthFieldPrepender(this.option.LengthFieldLength));
            pipeline.AddLast(new StringDecoder(Encoding.UTF8));
            pipeline.AddLast(new StringEncoder(Encoding.UTF8));
            pipeline.AddLast(new NettyTcpServer01Handler(this.nettyEvent));
        }
    }
}
