﻿/*
 * ==============================================================================
 *
 * Filename: EmailKit
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/9 0:10:44
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using MailKit.Net.Smtp;
using MimeKit;
using System;

namespace Core
{
    public class EmailKit : EmailProvider
    {
        /// <summary>
        /// 使用Ssl
        /// </summary>
        public bool UseSsl { get; }

        /// <summary>
        /// 邮箱服务商地址
        /// </summary>
        public string Host { get; }

        /// <summary>
        /// 邮箱服务商地址
        /// </summary>
        public int Port { get; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string Username { get; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; }

        public EmailKit(bool useSsl, string host, int port, string username, string password)
        {
            this.UseSsl = useSsl;
            this.Host = host;
            this.Port = port;
            this.Username = username;
            this.Password = password;
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="parameter"></param>
        public override string Send(EmailKitParameter parameter)
        {
            string result = string.Empty;
            if (parameter != null)
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(parameter.FromName, parameter.FromAddress));
                message.To.Add(new MailboxAddress(parameter.ToName, parameter.ToAddress));
                message.Subject = parameter.Subject;
                message.Body = new TextPart("plain") { Text = parameter.Body };
                try
                {
                    using (var client = new SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect(this.Host, this.Port, this.UseSsl);
                        client.Authenticate(this.Username, this.Password);
                        result = client.Send(message);
                        client.Disconnect(true);
                    }
                }
                catch(Exception e)
                {
                    result = e.Message;
                }
            }

            return result;
        }

    }
}
