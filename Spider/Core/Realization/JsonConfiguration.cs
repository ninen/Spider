﻿/*
 * ==============================================================================
 *
 * Filename: JsonConfiguration
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/1 20:19:34
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System.IO;

namespace Core
{
    internal class JsonConfiguration : IConfigureable
    {
        public AppConfig Configure(string filename)
        {
            string content = File.ReadAllText(filename);
            AppConfig config = JsonHelper.Deserialize<AppConfig>(content);
            return config;
        }
    }
}
