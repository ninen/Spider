﻿/*
 * ==============================================================================
 *
 * Filename: NettySepBaseFrameEncoder
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/23 16:48:02
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using System.Text;

namespace Core
{
    public class NettySepBaseFrameEncoder : MessageToByteEncoder<string>
    {
        private int maxFrameLength;
        private bool failFast;
        private IByteBuffer beginDelimiter;
        private IByteBuffer endDelimiter;

        public NettySepBaseFrameEncoder(int maxFrameLength, bool failFast, IByteBuffer beginDelimiter, IByteBuffer endDelimiter)
        {
            this.maxFrameLength = maxFrameLength;
            this.failFast = failFast;
            this.beginDelimiter = beginDelimiter;
            this.endDelimiter = endDelimiter;
        }

        protected override void Encode(IChannelHandlerContext context, string message, IByteBuffer output)
        {
            int beginDelimiterCount = beginDelimiter.ReadableBytes;
            int endDelimiterCount = endDelimiter.ReadableBytes;
            if (message.Length + beginDelimiterCount + endDelimiterCount > this.maxFrameLength)
            {
                return;
            }
            output.WriteBytes(beginDelimiter.Array);
            output.WriteString(message, Encoding.UTF8);
            output.WriteBytes(endDelimiter.Array);
        }
    }
}
