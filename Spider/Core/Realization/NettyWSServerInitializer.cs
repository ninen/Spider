﻿/*
 * ==============================================================================
 *
 * Filename: NettyWSServerInitializer
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 15:04:29
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Codecs.Http;
using DotNetty.Codecs.Http.WebSockets;
using DotNetty.Handlers.Streams;
using DotNetty.Handlers.Timeout;
using DotNetty.Transport.Channels;

namespace Core
{
    public class NettyWSServerInitializer : ChannelInitializer<IChannel>
    {
        private INetServerHandler netServer;
        private NettyWSServerOption option;

        public NettyWSServerInitializer(INetServerHandler netServer, NettyWSServerOption option)
        {
            this.netServer = netServer;
            this.option = option;
        }

        protected override void InitChannel(IChannel channel)
        {
            IChannelPipeline pipeline = channel.Pipeline;
            pipeline.AddLast(new IdleStateHandler(this.option.ReaderIdleTimeSeconds, this.option.WriterIdleTimeSeconds, this.option.AllIdleTimeSeconds));
            pipeline.AddLast(new HttpServerCodec());
            pipeline.AddLast(new ChunkedWriteHandler<byte>());
            pipeline.AddLast(new HttpObjectAggregator(this.option.MaxContentLength));
            pipeline.AddLast(new WebSocketServerProtocolHandler(this.option.WebsocketPath));
            pipeline.AddLast(new NettyWSServerHandler(this.netServer));
        }
    }

}
