﻿/*
 * ==============================================================================
 *
 * Filename: NettyTcpServer02Handler
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 13:43:54
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Handlers.Timeout;
using DotNetty.Transport.Channels;
using System;

namespace Core
{
    public class NettyTcpServer02Handler : SimpleChannelInboundHandler<string>
    {
        private INetServerHandler netServer;
        public NettyTcpServer02Handler(INetServerHandler netServer)
        {
            this.netServer = netServer;
        }

        protected override void ChannelRead0(IChannelHandlerContext ctx, string msg)
        {
            this.netServer.Read(new NettyHandlerContext(NettyProtocolType.Tcp, ctx), msg);
        }

        public override void ChannelActive(IChannelHandlerContext ctx)
        {
            this.netServer.Connect(new NettyHandlerContext(NettyProtocolType.Tcp, ctx));
            base.ChannelActive(ctx);
        }

        public override void UserEventTriggered(IChannelHandlerContext ctx, object evt)
        {
            if (evt is IdleStateEvent)
            {
                ctx.Channel.CloseAsync();
            }
        }

        public override void ChannelInactive(IChannelHandlerContext ctx)
        {
            this.netServer.DisConnect(new NettyHandlerContext(NettyProtocolType.Tcp, ctx));
            base.ChannelInactive(ctx);
        }

        public override void ExceptionCaught(IChannelHandlerContext ctx, Exception exception)
        {
            this.netServer.Exception(new NettyHandlerContext(NettyProtocolType.Tcp, ctx), exception);
            ctx.CloseAsync();
        }
    }
}
