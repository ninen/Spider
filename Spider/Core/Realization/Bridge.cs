﻿/*
 * ==============================================================================
 *
 * Filename: Bridge
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/13 10:31:27
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;

namespace Core
{
    internal class Bridge : INetServerHandler
    {
        public AppContext Context { get; }

        public Bridge(AppContext context)
        {
            this.Context = context;
        }

        public void Connect(IHandlerContext ctx)
        {
            Context.NetManager.ForeachListener((handle) =>
            {
                var mailbox = Context.MailboxManager.QueryMailbox(handle);
                if (mailbox != null)
                {
                    mailbox.Deliver(UtilConstants.SystemHandle, MessageType.SOCKET, 0, Convert.ToInt32(MessageType.SOCKET_CONNECT), ctx.RemoteAddress(), string.Empty);
                }
            });
            Context.NetManager.AddHandlerContext(ctx);
        }

        public void DisConnect(IHandlerContext ctx)
        {
            Context.NetManager.ForeachListener((handle) =>
            {
                var mailbox = Context.MailboxManager.QueryMailbox(handle);
                if (mailbox != null)
                {
                    mailbox.Deliver(UtilConstants.SystemHandle, MessageType.SOCKET, 0, Convert.ToInt32(MessageType.SOCKET_CLOSE), ctx.RemoteAddress(), string.Empty);
                }
            });
            Context.NetManager.RemoveHandlerContext(ctx);
        }

        public void Exception(IHandlerContext ctx, Exception exception)
        {
            Context.NetManager.ForeachListener((handle) =>
            {
                var mailbox = Context.MailboxManager.QueryMailbox(handle);
                if (mailbox != null)
                {
                    mailbox.Deliver(UtilConstants.SystemHandle, MessageType.SOCKET, 0, Convert.ToInt32(MessageType.SOCKET_ERROR), ctx.RemoteAddress(), exception.Message);
                }
            });
            Context.NetManager.RemoveHandlerContext(ctx);
        }

        public void Read(IHandlerContext ctx, string msg)
        {
            if (Context.NetManager.ContainsHandlerContext(ctx))
            {
                Context.NetManager.ForeachListener((handle) =>
                {
                    var mailbox = Context.MailboxManager.QueryMailbox(handle);
                    if (mailbox != null)
                    {
                        mailbox.Deliver(UtilConstants.SystemHandle, MessageType.SOCKET, 0, Convert.ToInt32(MessageType.SOCKET_DATA), ctx.RemoteAddress(), msg);
                    }
                });
            }
        }


    }
}
