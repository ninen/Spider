﻿/*
 * ==============================================================================
 *
 * Filename: NettyTcpServer01
 * Description: 
 * tcp server by LengthFieldBasedFrameDecoder
 * Version: 1.0
 * Created: 2022/3/9 13:28:58
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using System.Net;

namespace Core
{
    /// <summary>
    /// tcp服务器,且以LengthFieldBasedFrameDecoder进行分包
    /// </summary>
    public class NettyTcpServer01 : INetServer
    {
        private IEventLoopGroup bossGroup;
        private IEventLoopGroup workerGroup;
        private NettyTcpServer01Option option;
        private INetServerHandler nettyEvent;
        private IChannel channel;

        private NettyTcpServer01(int eventLoopCount)
        {
            // MultithreadEventLoopGroup(1) == SingleThreadEventLoop
            this.bossGroup = new MultithreadEventLoopGroup(1);
            this.workerGroup = new MultithreadEventLoopGroup(eventLoopCount);
            this.channel = null;
        }

        public NettyTcpServer01(int eventLoopCount, INetServerHandler nettyEvent, NettyTcpServer01Option option)
            : this(eventLoopCount)
        {
            this.nettyEvent = nettyEvent;
            this.option = option;
        }

        public NettyTcpServer01(int eventLoopCount, INetServerHandler nettyEvent)
            : this(eventLoopCount)
        {
            this.nettyEvent = nettyEvent;
            this.option = new NettyTcpServer01Option
            {
                ReaderIdleTimeSeconds = 120,
                WriterIdleTimeSeconds = 120,
                AllIdleTimeSeconds = 0,
                MaxFrameLength = 4096,
                LengthFieldOffset = 0,
                LengthFieldLength = 4,
                LengthAdjustment = 0,
                InitialBytesToStrip = 4
            };
        }

        public void Start(int inetPort)
        {

            var bootstrap = new ServerBootstrap();
            bootstrap.Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new NettyTcpServer01Initializer(this.nettyEvent, this.option));
            var bootstrapChannel = bootstrap.BindAsync(inetPort);
            this.channel = bootstrapChannel.Result;
        }

        public void Start(string inetHost, int inetPort)
        {
            var bootstrap = new ServerBootstrap();
            bootstrap.Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new NettyTcpServer01Initializer(this.nettyEvent, this.option));
            var ipAddress = IPAddress.Parse(inetHost);
            var bootstrapChannel = bootstrap.BindAsync(ipAddress, inetPort);
            this.channel = bootstrapChannel.Result;
        }

        public void Stop()
        {
            this.channel?.CloseAsync();
            workerGroup.ShutdownGracefullyAsync();
            bossGroup.ShutdownGracefullyAsync();
        }
    }
}
