﻿/*
 * ==============================================================================
 *
 * Filename: NettyHttpServerHandler
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 15:06:24
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Codecs.Http;
using DotNetty.Transport.Channels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    class NettyHttpServerHandler : SimpleChannelInboundHandler<IHttpObject>
    {
        private INetServerHandler serverHandler;
        public NettyHttpServerHandler(INetServerHandler serverHandler)
        {
            this.serverHandler = serverHandler;
        }

        protected override void ChannelRead0(IChannelHandlerContext ctx, IHttpObject msg)
        {
            // HttpObjectAggregator
            if (msg is IFullHttpRequest)
            {
                var request = msg as IFullHttpRequest;
                HttpRequest httpRequest = new HttpRequest();
                httpRequest.Uri = request.Uri;
                httpRequest.Method = request.Method.Name;
                httpRequest.ProtocolName = request.ProtocolVersion.ProtocolName;
                httpRequest.Headers = new Dictionary<string, string>();
                var headers = request.Headers.Entries();
                if (headers.Count > 0)
                {
                    foreach (var header in headers)
                    {
                        httpRequest.Headers.Add(header.Key.ToString(), header.Value.ToString());
                    }
                }
                httpRequest.Parameters = request.Content.ReadString(request.Content.ReadableBytes, Encoding.UTF8);
                this.serverHandler.Read(new NettyHandlerContext(NettyProtocolType.Http, ctx), JsonHelper.Serialize(httpRequest));
            }
        }
    }
}
