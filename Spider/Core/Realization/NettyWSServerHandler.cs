﻿/*
 * ==============================================================================
 *
 * Filename: NettyWSServerHandler
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 15:06:24
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using DotNetty.Codecs.Http.WebSockets;
using DotNetty.Handlers.Timeout;
using DotNetty.Transport.Channels;
using System;

namespace Core
{
    class NettyWSServerHandler : SimpleChannelInboundHandler<TextWebSocketFrame>
    {
        private INetServerHandler netServer;
        public NettyWSServerHandler(INetServerHandler netServer)
        {
            this.netServer = netServer;
        }

        protected override void ChannelRead0(IChannelHandlerContext ctx, TextWebSocketFrame msg)
        {
            this.netServer.Read(new NettyHandlerContext(NettyProtocolType.Websocket, ctx), msg.Text());
        }

        public override void ChannelActive(IChannelHandlerContext ctx)
        {
            this.netServer.Connect(new NettyHandlerContext(NettyProtocolType.Websocket, ctx));
            base.ChannelActive(ctx);
        }

        public override void UserEventTriggered(IChannelHandlerContext ctx, object evt)
        {
            if (evt is IdleStateEvent)
            {
                ctx.Channel.CloseAsync();
            }
        }

        public override void ChannelInactive(IChannelHandlerContext ctx)
        {
            this.netServer.DisConnect(new NettyHandlerContext(NettyProtocolType.Websocket, ctx));
            base.ChannelInactive(ctx);
        }

        public override void ExceptionCaught(IChannelHandlerContext ctx, Exception exception)
        {
            this.netServer.Exception(new NettyHandlerContext(NettyProtocolType.Websocket, ctx), exception);
            ctx.CloseAsync();
        }

    }
}
