﻿/*
 * ==============================================================================
 *
 * Filename: LuaFile
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/25 13:18:59
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;
using System;
using System.IO;

namespace LuaService
{
    public class LuaFile
    {
        /// <summary>
        /// Append File
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        [LuaAPI]
        public void AppendB64(string filename, string str)
        {
            byte[] data = Convert.FromBase64String(str);
            using (FileStream fs = File.OpenWrite(filename))
            {
                fs.Position = fs.Length;
                fs.Write(data, 0, data.Length);
                fs.Close();
            }
        }
    }
}
