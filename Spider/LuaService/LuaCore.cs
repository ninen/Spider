﻿/*
 * ==============================================================================
 *
 * Filename: LuaCore
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/11 22:19:42
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/


using Core;
using NLua;

namespace LuaService
{
    public class LuaCore
    {
        public ServiceInstance Instance { get; }
        public LuaCore(ServiceInstance instance)
        {
            this.Instance = instance;
        }

        /// <summary>
        /// 版本
        /// </summary>
        /// <returns></returns>
        [LuaAPI]
        public string Version()
        {
            return "v1.0.0";
        }

        /// <summary>
        /// 设置回调函数
        /// </summary>
        /// <param name="cb"></param>
        [LuaAPI]
        public void Callback(LuaFunction cb)
        {
            if (cb != null)
            {
                this.Instance._cb = cb;
            }
        }

        /// <summary>
        /// 执行命令方法
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="paramters"></param>
        /// <returns></returns>
        [LuaAPI]
        public string Command(string cmd, params object[] paramters)
        {
            string result = this.Instance.Cmd.Command(cmd, paramters);
            return result;
        }

        /// <summary>
        /// 执行命令方法
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        [LuaAPI]
        public int IntCommand(string cmd, params object[] paramters)
        {
            string result = this.Instance.Cmd.Command(cmd, paramters);
            int value = 0;
            int.TryParse(result, out value);
            return value;
        }

        /// <summary>
        /// 生成session
        /// </summary>
        /// <returns></returns>
        [LuaAPI]
        public int GenID()
        {
            int session = this.Instance.Cmd.Send(this.Instance.Cmd.Handle(), MessageType.TAG_ALLOCSESSION, string.Empty);
            return session;
        }

        /// <summary>
        /// 调用目标服务
        /// </summary>
        /// <param name="dest">目标</param>
        /// <param name="type">类型</param>
        /// <param name="parameters">数据</param>
        [LuaAPI]
        public void Redirect(int dest, int type, params object[] parameters)
        {
            this.Instance.Cmd.Send(dest, (MessageType)type, parameters);
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="dest">目标类型</param>
        /// <param name="type"></param>
        /// <param name="parameters">数据</param>
        /// <returns></returns>
        [LuaAPI]
        public int Send(int dest, int type, params object[] parameters)
        {
            int session = this.Instance.Cmd.Send(dest, (MessageType)type, parameters);
            return session;
        }

        /// <summary>
        /// 打印日志
        /// </summary>
        /// <param name="msg"></param>
        [LuaAPI]
        public void Log(params string[] parameters)
        {
            this.Instance.Cmd.Log(parameters);
        }

        /// <summary>
        /// 打印错误
        /// </summary>
        /// <param name="msg"></param>
        [LuaAPI]
        public void Error(params string[] parameters)
        {
            this.Instance.Cmd.Error(parameters);
        }

    }
}
