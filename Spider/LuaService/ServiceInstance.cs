﻿/*
 * ==============================================================================
 *
 * Filename: ServiceInstance
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/28 13:14:18
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/
using Core;
using NLua;
using System;
using System.Collections.Generic;

namespace LuaService
{
    public class ServiceInstance
    {
        [Hotfix(true)]
        public LuaEngine LuaEngine { get; set; }

        [Hotfix(true)]
        public LuaFunction _cb { get; set; }

        public object DoResult { get; set; }
       
        public ICommandable Cmd { get; set; }

        public void Init(ICommandable cmd, string param)
        {
            this.Cmd = cmd;
            this.LuaEngine = new LuaEngine();
            try
            {
                LuaTable package = this.LuaEngine.GetTable("package");
                string luaPath = this.Cmd.Command("GETSTRING", "LuaPath");
                string luaCPath = this.Cmd.Command("GETSTRING", "LuaCPath");
                string root = this.Cmd.Command("GETSTRING", "Root");
                package["path"] += luaPath;
                package["cpath"] += luaCPath;
                this.LuaEngine.RequireF("core", new LuaCore(this));
                this.LuaEngine.RequireF("socket", new LuaSocket(this));
                this.LuaEngine.RequireF("crypt", new LuaCrypt());
                this.LuaEngine.RequireF("file", new LuaFile());
                this.LuaEngine.RequireF("mysql", new LuaMysql());
                this.LuaEngine.RequireF("email", new LuaEmail());
                var filename = string.Format("{0}/{1}", root, param);
                this.DoResult = this.LuaEngine.DoFile(filename);
            }
            catch(Exception ex)
            {
                this.Cmd.Error(ex.Source, ex.Message);
            }
        }

        public void HotfixInit(ICommandable cmd, string param)
        {
            try
            {
                this.Cmd = cmd;
                if (!string.IsNullOrEmpty(param) && param.Contains(".lua"))
                {
                    string root = this.Cmd.Command("GETSTRING", "Root");
                    var filename = string.Format("{0}/{1}", root, param);
                    this.DoResult = this.LuaEngine.DoFile(filename);
                }
                else
                {
                    this.Cmd.Log("只是C#层面热更");
                }
            }
            catch (Exception ex)
            {
                this.Cmd.Error(ex.Source, ex.Message);
            }
        }

        public void Callback(object ud, MessageType type, int session, int source, object[] args)
        {
            if (this._cb != null)
            {
                // 转换成在lua中为...方式
                List<object> converter = new List<object>();
                converter.Add(Convert.ToInt32(type));
                converter.Add(session);
                converter.Add(source);
                if (args.Length > 0)
                {
                    converter.AddRange(args);
                }
                this._cb.Call(converter.ToArray());
            }
        }
    }
}
