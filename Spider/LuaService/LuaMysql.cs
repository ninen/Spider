﻿/*
 * ==============================================================================
 *
 * Filename: LuaMysql
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/22 14:22:39
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;
using NLua;
using System.Collections.Generic;

namespace LuaService
{
    /// <summary>
    /// 
    /// </summary>
    public class LuaMysql
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        [LuaAPI]
        public DBProvider Create(string host, int port, string user, string password, string db)
        {
            string connetStr = string.Format("server={0};port={1};user={2};password={3}; database={4};", host, port, user, password, db);
            DBProvider dbProvider = new Mysql(connetStr);
            return dbProvider;
        }

        [LuaAPI]
        public bool Open(Mysql mysql)
        {
            return mysql.Open();
        }

        [LuaAPI]
        public int NoQuery(DBProvider dbProvider, string sql, LuaTable parameters)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (var item in parameters.Keys)
            {
                dict.Add(item.ToString(), parameters[item]);
            }
            return dbProvider.NoQuery(sql, dict);
        }

        [LuaAPI]
        public object[] Query(DBProvider dbProvider, string sql, LuaTable parameters)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (var item in parameters.Keys)
            {
                dict.Add(item.ToString(), parameters[item]);
            }
            return dbProvider.Query(sql, dict);
        }

        [LuaAPI]
        public string QueryJson(DBProvider dbProvider, string sql, LuaTable parameters)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (var item in parameters.Keys)
            {
                dict.Add(item.ToString(), parameters[item]);
            }
            var result = dbProvider.Query(sql, dict);
            return JsonHelper.Serialize(result);
        }

        [LuaAPI]
        public void Close(Mysql mysql)
        {
            mysql.Close();
        }

    }
}
