﻿/*
 * ==============================================================================
 *
 * Filename: Entry
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/26 21:25:14
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;
using System;

namespace LuaService
{
    public class Entry : IEntryable
    {
        public object Create()
        {
            ServiceInstance instance = new ServiceInstance();
            return instance;
        }

        public Action<object, MessageType, int, int, object[]> Init(ICommandable cmd, bool isHotfix, object ud, string param)
        {
            ServiceInstance instance = ud as ServiceInstance;
            if (isHotfix)
            {
                cmd.Log("hotfix init");
                instance.HotfixInit(cmd, param);
            }
            else
            {
                instance.Init(cmd, param);
            }

            return Callback;
        }

        void Callback(object ud, MessageType type, int session, int source, params object[] args)
        {
            ServiceInstance instance = ud as ServiceInstance;
            instance.Callback(ud, type, session, source, args);
        }

        public void Release(bool isHotfix, object ud)
        {
            ServiceInstance instance = ud as ServiceInstance;
            if (!isHotfix)
            {
                instance.Cmd.Error("Release");
                instance.LuaEngine.Clear();
            }
        }

        public void Signal(object ud)
        {
            ServiceInstance instance = ud as ServiceInstance;
        }

    }
}
