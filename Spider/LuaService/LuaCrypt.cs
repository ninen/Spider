﻿/*
 * ==============================================================================
 *
 * Filename: LuaCrypt
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/22 0:14:06
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;

namespace LuaService
{
    public class LuaCrypt
    {
        /// <summary>
        /// md5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        [LuaAPI]
        public string MD5(string str)
        {
            return Crypt.GetMD5(str);
        }

        /// <summary>
        /// base64 encode
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        [LuaAPI]
        public string B64de(string str)
        {
            return Crypt.Base64Decode(str);
        }

        /// <summary>
        /// base64 encode
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        [LuaAPI]
        public string B64en(string str)
        {
            return Crypt.Base64Encode(str);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        [LuaAPI]
        public string AesEncrypt(string str, string key, string iv)
        {
            return Crypt.AesEncrypt(str, key, iv);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        [LuaAPI]
        public string AesDecrypt(string str, string key, string iv)
        {
            string result = Crypt.AesDecrypt(str, key, iv);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        [LuaAPI]
        public string DesEncrypt(string str, string key, string iv)
        {
            string result = Crypt.DesEncrypt(str, key, iv);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        [LuaAPI]
        public string DesDecrypt(string str, string key, string iv)
        {
            return Crypt.DesDecrypt(str, key, iv);
        }

    }
}
