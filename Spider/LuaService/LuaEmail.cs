﻿/*
 * ==============================================================================
 *
 * Filename: LuaEmail
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/6/9 0:18:00
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;

namespace LuaService
{
    public class LuaEmail
    {
        [LuaAPI]
        public EmailKit Create(bool useSsl, string host, int port, string username, string password)
        {
            EmailKit mailKit = new EmailKit(useSsl, host, port, username, password);
            return mailKit;
        }

        [LuaAPI]
        public string Send(EmailKit mailKit, 
            string fromAddress, string fromName,   
            string toAddress, string toName, 
            string subject, string body)
        {
            string result = mailKit.Send(new EmailKitParameter()
            {
                FromAddress = fromAddress,
                FromName = fromName,
                ToAddress = toAddress,
                ToName = toName,
                Subject = subject,
                Body = body
            });
            return result;
        }

    }
}
