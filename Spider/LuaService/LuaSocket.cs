﻿/*
 * ==============================================================================
 *
 * Filename: LuaSocket
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/20 23:17:37
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;

namespace LuaService
{
    public class LuaSocket
    {
        public ServiceInstance Instance { get; }
        public LuaSocket(ServiceInstance instance)
        {
            this.Instance = instance;
        }

        [LuaAPI]
        public void Listen()
        {
            this.Instance.Cmd.Listen();
        }

        [LuaAPI]
        public void Unlisten()
        {
            this.Instance.Cmd.Unlisten();
        }

        [LuaAPI]
        public void Send(string remoteAddress, object msg)
        {
            this.Instance.Cmd.NetSend(remoteAddress, msg);
        }
    }
}
