﻿using Core;
using System;

namespace JSTest
{
    class CC
    {
        public void Test2(int a, int b)
        {
            Console.WriteLine("test=============" + (a + b));
        }

        public static void Test()
        {
            Console.WriteLine("test=============");
        }
    }

    class Program
    {
        static void TestJavaScript()
        {
            JavaScriptEngine javaScript = new JavaScriptEngine();
            javaScript.RegisterMethod("console", new
            {
                log = new Action<object>(p => Console.WriteLine(p)),
                log4 = new Action<object>(p =>
                {
                    Console.WriteLine(p);
                })
            });

            javaScript.RegisterMethod("system", new
            {
                loadfile = new Action<string>( filename => javaScript.ExecuteFile(filename))
            });

            //CC cc = new CC();
            var type = typeof(Program).GetType();
            javaScript.RegisterFunction("cc", type);

            CC cc = new CC();
            javaScript.RegisterMethod("cc", cc);
            javaScript.RegisterFunction("tt", cc.GetType());

            javaScript.ExecuteFile(AppDomain.CurrentDomain.BaseDirectory + "test.js");
            javaScript.Close();
        }

        static void Main(string[] args)
        {
            TestJavaScript();
            Console.ReadKey();
        }
    }
}
