﻿/*
 * ==============================================================================
 *
 * Filename: HttpServerHandler
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/13 10:31:27
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;
using System;

namespace HttpServerService
{
    public class HttpServerHandler : INetServerHandler
    {
        public ICommandable Cmd { get; }
        public HttpServerHandler(ICommandable cmd)
        {
            this.Cmd = cmd;
        }

        public void Connect(IHandlerContext ctx)
        {
        }

        public void DisConnect(IHandlerContext ctx)
        {
        }

        public void Exception(IHandlerContext ctx, Exception exception)
        {
        }

        public void Read(IHandlerContext ctx, string msg)
        {
            this.Cmd.Send(Cmd.Handle(), MessageType.HTTP, ctx, msg);
        }
    }
}
