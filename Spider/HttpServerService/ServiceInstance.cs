﻿/*
 * ==============================================================================
 *
 * Filename: ServiceInstance
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/25 19:38:19
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/


using Core;

namespace HttpServerService
{
    public class ServiceInstance
    {
        public ICommandable Cmd { get; set; }

        [Hotfix(true)]
        public NettyHttpServer NettyHttpServer { get; set; }

        public ServiceInstance()
        {
            this.Cmd = null;
            this.NettyHttpServer = null;
        }

        public void Init(ICommandable cmd, int port)
        {
            this.Cmd = cmd;
            cmd.Log("http server bind port: ", port);
            this.NettyHttpServer = new NettyHttpServer(1, new HttpServerHandler(this.Cmd));
            this.NettyHttpServer.Start(port);
        }

        public void Release()
        {
            this.NettyHttpServer.Stop();
        }

    }
}
