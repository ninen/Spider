﻿/*
 * ==============================================================================
 *
 * Filename: Entry
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 19:00:10
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;
using DotNetty.Buffers;
using DotNetty.Codecs.Http;
using Newtonsoft.Json;
using System;
using System.Text;

namespace HttpServerService
{
    public class Entry : IEntryable
    {
        public object Create()
        {
            ServiceInstance instance = new ServiceInstance();
            return instance;
        }

        public Action<object, MessageType, int, int, object[]> Init(ICommandable cmd, bool isHotfix, object ud, string args)
        {
            ServiceInstance instance = ud as ServiceInstance;
            int port = Convert.ToInt32(args);
            instance.Init(cmd, port);
            return Callback;
        }

        void Callback(object ud, MessageType type, int session, int source, object[] parameters)
        {
            ServiceInstance instance = ud as ServiceInstance;
            var ctx = parameters[0] as NettyHandlerContext;
            string msg = parameters[1] as string;
            var request = JsonConvert.DeserializeObject<HttpRequest>(msg);
            if (request.Uri.Contains("/services"))
            {
                var services = instance.Cmd.Services();
                IByteBuffer byteBuffer = Unpooled.CopiedBuffer(services, UTF8Encoding.UTF8);
                var response = new DefaultFullHttpResponse(HttpVersion.Http11, HttpResponseStatus.OK, byteBuffer);
                response.Headers.Set(HttpHeaderNames.ContentType, "text/pain");
                response.Headers.Set(HttpHeaderNames.ContentLength, byteBuffer.ReadableBytes);
                ctx.WriteAndFlushAsync(response);
            }
            else
            {
                IByteBuffer byteBuffer = Unpooled.Empty;
                var response = new DefaultFullHttpResponse(HttpVersion.Http11, HttpResponseStatus.NotFound, byteBuffer);
                response.Headers.Set(HttpHeaderNames.ContentType, "text/pain");
                response.Headers.Set(HttpHeaderNames.ContentLength, byteBuffer.ReadableBytes);
                ctx.WriteAndFlushAsync(response);
            }
        }

        public void Release(bool isHotfix, object ud)
        {
            ServiceInstance instance = ud as ServiceInstance;
            if (isHotfix)
            {
                instance.Release();
            }
        }

        public void Signal(object ud)
        {
        }

    }
}
