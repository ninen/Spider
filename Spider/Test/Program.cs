﻿using Core;
using System;

namespace Test
{
    public class NettyEvent : INetServerHandler
    {
        public void Connect(IHandlerContext ctx)
        {
            ctx.WriteAndFlushAsync("hello world");
            Console.WriteLine("Connect :" + ctx.RemoteAddress());
        }

        public void DisConnect(IHandlerContext ctx)
        {
            Console.WriteLine("DisConnect :" + ctx.RemoteAddress());
        }

        public void Exception(IHandlerContext ctx, Exception exception)
        {
            Console.WriteLine("Exception :" + ctx.RemoteAddress());
        }

        public void Read(IHandlerContext ctx, string msg)
        {
            Console.WriteLine("Read :" + msg);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //INetty netty = new NettyTSLF(1, new NettyEvent());
            //INetty netty = new NettyWS(1, new NettyEvent());
            //netty.Start("172.26.111.35", 30020);
            //ISocketServer netty = new NettyTSSEP(1, new NettyEvent());
            //netty.Start(30020);

            //LuaEngine luaEngine = new LuaEngine();
            //LuaSpider luaSpider = new LuaSpider();
            //foreach (MethodInfo method in luaSpider.GetType().GetMethods())
            //{
            //    luaEngine.RegisterMethod(method.Name.ToLower(), luaSpider, method);
            //}
            //luaEngine.SetVariable("api", luaSpider);
            //var result = luaEngine.DoFile("test.lua");
            //var func = result[0] as LuaFunction;
            //var t = func.Call(1, 2);
            //Console.WriteLine(t[0]);

            //string str = "/9j/4AAQSkZJRgABAQEAeAB4AAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCALQBQADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSEk=";

            //var bs = Convert.FromBase64String(str);
            //using (FileStream fWriter = new FileStream("txt.txt", FileMode.Append, FileAccess.Write))
            //{
            //    fWriter.Write(bs, 0, bs.Length);
            //}

            //EmailKit mailKit = new EmailKit(true, "220.181.15.161", 465, "18870214696@163.com", "OEFZYILAZYAGUJYK");
            //string result = mailKit.Send(new EmailKitParameter()
            //{
            //    Body = "11111",
            //    FromAddress = "18870214696@163.com",
            //    FromName = "18870214696",
            //    Subject = "11",
            //    ToAddress = "228407354@qq.com",
            //    ToName = "228407354"
            //});

            //Console.WriteLine("result= " + result);
            Console.WriteLine("  12");

            Console.ReadKey();
        }
    }
}
