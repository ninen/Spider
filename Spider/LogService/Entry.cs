﻿/*
 * ==============================================================================
 *
 * Filename: Entry
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/9 19:00:10
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;
using System;

namespace LogService
{
    public class Entry : IEntryable
    {
        public object Create()
        {
            ServiceInstance instance = new ServiceInstance();
            return instance;
        }

        public Action<object, MessageType, int, int, object[]> Init(ICommandable cmd, bool isHotfix, object ud, string param)
        {
            ServiceInstance instance = ud as ServiceInstance;
            instance.Init(param);
            return Callback;
        }

        void Callback(object ud, MessageType type, int session, int source, object[] parameters)
        {
            if (parameters != null)
            {
                ServiceInstance instance = ud as ServiceInstance;
                string message = string.Format("{0} {1}", UtilMethod.ToAddr(source), string.Join(',', parameters));
                if (type == MessageType.TEXT)
                {
                    instance.Log(message);
                }
                else if (type == MessageType.ERROR)
                {
                    instance.Error(message);
                }
                else if (type == MessageType.SOCKET)
                {
                    instance.Log(message);
                }
                else
                {
                    instance.Error(string.Format("{0} not found type={1} message={2}", UtilMethod.ToAddr(source), type, message));
                }
            }
        }

        public void Release(bool isHotfix, object ud)
        {
            ServiceInstance instance = ud as ServiceInstance;
            instance.Log("Release: isHotfix= " + isHotfix);
        }

        public void Signal(object ud)
        {
            ServiceInstance instance = ud as ServiceInstance;
        }

    }
}
