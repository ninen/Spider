﻿/*
 * ==============================================================================
 *
 * Filename: ServiceInstance
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/3/11 19:54:12
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;

namespace LogService
{
    public class ServiceInstance
    {
        public Log4net log4net { get; set; }

        public ServiceInstance()
        {
        }

        public void Init(string filename)
        {
            this.log4net = new Log4net(filename, "loginfo", "logerror");
        }

        public void Log(string message)
        {
            this.log4net.Log(message);
        }

        public void Error(string message)
        {
            this.log4net.Error(message);
        }
    }
}
