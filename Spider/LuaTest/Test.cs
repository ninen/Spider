﻿/*
 * ==============================================================================
 *
 * Filename: Test
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/11 22:03:46
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuaTest
{
    public class Test
    {
        public string Name { get; }
        public Test()
        {
            this.Name = "xiaoming";
        }

        public string GetName()
        {
            return this.Name;
        }
    }
}
