﻿/*
 * ==============================================================================
 *
 * Filename: Lazynet
 * Description: 
 *
 * Version: 1.0
 * Created: 2022/4/11 22:29:12
 *
 * Author: Your name
 * Company: Your company name
 *
 * ==============================================================================
*/

using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuaTest
{
    public class Lazynet
    {
        public Action<object, int, int, int, string> _cb;

        /// <summary>
        /// 版本
        /// </summary>
        /// <returns></returns>
        [LuaAPI]
        public string Version()
        {
            return "v1.0.0";
        }

        /// <summary>
        /// 设置回调函数
        /// </summary>
        /// <param name="cb"></param>
        [LuaAPI]
        public void Callback(Action<object, int, int, int, string> cb)
        {
            this._cb = cb;
        }

        /// <summary>
        /// 执行命令方法
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        [LuaAPI]
        public string Command(string cmd, string param)
        {
            string result = string.Empty;

            return result;
        }

        
        /// <summary>
        /// 执行命令方法
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        [LuaAPI]
        public int IntCommand(string cmd, int param)
        {
            int result = 0;

            return result;
        }

        /// <summary>
        /// 生成session
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        [LuaAPI]
        public int GenerateId(string cmd, int param)
        {
            int session = 0;

            return session;
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="dest">目标类型</param>
        /// <param name="type"></param>
        /// <param name="session">sesion</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        [LuaAPI]
        public int Send(int dest, int type, int session, string data)
        {
            return 0;
        }
    }
}
