﻿using Core;
using System;

namespace LuaTest
{
    class Program
    {
        static void Main(string[] args)
        {
            LuaEngine luaEngine = new LuaEngine();
            luaEngine["name"] = "liwei";
            var result = luaEngine.GetString("name");

            Test test = new Test();
            luaEngine.RegisterMethod("getname", test, test.GetType().GetMethod("GetName"));

            Lazynet lazynet = new Lazynet();
            luaEngine.RequireF("api", lazynet);

            luaEngine.DoFile("test.lua");
            luaEngine.DoFile("test2.lua");

            //lazynet._cb(lazynet, 0, 1, 4, "callback");

            Console.ReadKey();
        }
    }
}
