# <img src="spider.ico" alt="b63a35b6b7ef211330a986c6e1419243"/>Spider

#### 介绍

actor模型的网络并发框架.

#### 软件架构



#### 安装教程

1.  在VS打开项目工程并且编译对应平台的版本
2.  找到Spider相关生成直接运行即可

#### 使用说明

1.  配置好根目录下config.lua文件中的配置
2.  Spider.exe(windows)/dotnet Spider.dll(linux) root_dir 运行
3.  看到打印相关信息表示运行成功

#### 参与贡献

1.  liwei


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
